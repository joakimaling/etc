unit Utilities;

interface

{ Colours }
procedure ColourStart(Background, Foreground: Byte);
procedure ColourStop;

{ Console }
procedure ConsoleClear;
function ConsoleColumns: Word;
function ConsoleRows: Word;

{ Cursor }
procedure CursorHide;
procedure CursorMove(X, Y: Word);
procedure CursorReset;
procedure CursorShow;

implementation

uses
	Unix;

var
	Pipe: Text;

{ Printing routines following this will print data using given colours. }
procedure ColourStart(Background, Foreground: Byte);
begin
	Write(#27, '[48;5;', Background, 'm', #27, '[38;5;', Foreground, 'm');
end;

{Printing routines following this will print data in default colours. }
procedure ColourStop;
begin
	Write(#27, '[33;5;0m');
end;

{ Clears the entire console. }
procedure ConsoleClear;
begin
	Write(#27, '[2J');
end;

{ Returns the number of columns. }
function ConsoleColumns: Word;
begin
	POpen(Pipe, 'tput cols', 'R');
	Read(Pipe, ConsoleColumns);
	PClose(Pipe);
end;

{ Returns the number of rows }
function ConsoleRows: Word;
begin
	POpen(Pipe, 'tput lines', 'R');
	Read(Pipe, ConsoleRows);
	PClose(Pipe);
end;

{ Turns the cursor off. }
procedure CursorHide;
begin
	Write(#27, '[?25l');
end;

{ Movers the console cursor to given point. Old: Write(#27, '[Y;Xf'); }
procedure CursorMove(X, Y: Word);
var
	Columns, Rows: String;
begin
	Str(X, Columns);
	Str(Y, Rows);
	POpen(Pipe, 'tput cup ' + Columns + ' ' + Rows, 'W');
	PClose(Pipe);
end;

{ Moves the console cursor to the 0,0 point. }
procedure CursorReset;
begin
	CursorMove(0, 0);
end;

{ Turns the cursor on. }
procedure CursorShow;
begin
	Write(#27, '[?25h');
end;

end.
