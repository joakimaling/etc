unit ConsoleBuffer;

{$H+}

interface

uses
	Utilities;

type
	Pixel = record
		Palette: Word;
		Glyph: String;
	end;

var
	BufferGlyph: String = #32; { Glyph used for clearing the buffer }
	BufferPalette: Word = $10ff; { Palette used for clearing the buffer }

procedure BufferDraw;
procedure BufferErase;

function BufferGetHeight: Word;
function BufferGetWidth: Word;

function BufferGetGlyph(X, Y: Word): String;
function BufferGetPalette(X, Y: Word): Word;
function BufferGetPixel(X, Y: Word): Pixel;
procedure BufferSetGlyph(X, Y: Word; Glyph: String);
procedure BufferSetPalette(X, Y, Palette: Word);
procedure BufferSetPixel(X, Y: Word; Glyph: String; Palette: Word);

procedure BufferStart;
procedure BufferStop;

{ Primitives }
procedure BufferDrawArea(X0, Y0, X1, Y1: Word; Glyph: String; Palette: Word);
procedure BufferDrawLine(X0, Y0, X1, Y1: Word; Glyph: String; Palette: Word);

{ Text }
procedure BufferDrawText(X, Y: Word; Glyphs: String; Palette: Word);
procedure BufferDrawTextCentre(Row: Word; Glyphs: String; Palette: Word);
procedure BufferDrawTextLeft(Row: Word; Glyphs: String; Palette: Word);
procedure BufferDrawTextRight(Row: Word; Glyphs: String; Palette: Word);
procedure BufferDrawTextWrap(X0, Y0, X1, Y1: Word; Glyphs: String; Palette: Word);

{ Helpers }
procedure BufferBoundaryReset;
procedure BufferBoundarySet(X0, Y0, X1, Y1: Word);

{ String helpers }
function UTFLength(const S: String): Word;

implementation

type
	PBuffer = ^TBuffer;
	TBuffer = record
		Height, Width: Word;
		Grid: Array of Pixel;
	end;

var
	BottomX, BottomY, TopX, TopY: Word;	{ Boundaries used for text wrapping }
	Buffer: PBuffer = nil;

{ Sets given point back to the buffer's width/height if the point exceeds it. }
procedure Clip(var X, Y: Word);
begin
  if X >= Buffer^.Width then X := Buffer^.Width - 1;
  if Y >= Buffer^.Height then Y := Buffer^.Height - 1;
end;

{ Sets given point to the start of the next line in a box with given boundaries.
  Used for wrapping lines of text. }
procedure Wrap(var X, Y: Word);
begin
  if X < BottomX then Inc(X) else begin X := TopX; Inc(Y) end;
  if Y > BottomY then Y := TopY;
end;

{ Creates the buffer of given size. }
procedure BufferCreate(Width, Height: Word);
begin
	GetMem(Buffer, SizeOf(TBuffer));
	if Buffer <> nil then begin
		Buffer^.Height := Height;
		Buffer^.Width := Width;
		SetLength(Buffer^.Grid, Height * Width);
		BufferErase;
	end;
end;

{ Destroys the buffer. }
procedure BufferDestroy;
begin
	FreeMem(Buffer, SizeOf(TBuffer));
end;

{ Draws the entire buffer to the console. }
procedure BufferDraw;
var
	Key: Word;
begin
	CursorReset;
	for Key := 0 to High(Buffer^.Grid) do begin
		with Buffer^.Grid[Key] do begin
			ColourStart(Hi(Palette), Lo(Palette));
			Write(Glyph);
			if Key mod Buffer^.Width = Buffer^.Width - 1 then WriteLn;
		end;
	end;
	ColourStop;
	BufferErase;
end;

{ Clears the buffer by filling it with spaces. }
procedure BufferErase;
begin
	BufferDrawArea(0, 0, Buffer^.Width - 1, Buffer^.Height - 1, BufferGlyph,
		BufferPalette);
end;

{ Returns the height of the buffer. }
function BufferGetHeight: Word;
begin
	BufferGetHeight := Buffer^.Height;
end;

{ Returns the width of the buffer. }
function BufferGetWidth: Word;
begin
	BufferGetWidth := Buffer^.Width;
end;

{ Returns the glyph at given point in the buffer. }
function BufferGetGlyph(X, Y: Word): String;
begin
	BufferGetGlyph := Buffer^.Grid[X + Y * Buffer^.Width].Glyph;
end;

{ Returns the palette at given point in the buffer. }
function BufferGetPalette(X, Y: Word): Word;
begin
	BufferGetPalette := Buffer^.Grid[X + Y * Buffer^.Width].Palette;
end;

{ Returns the pixel (glyph & palette) at given point in the buffer. }
function BufferGetPixel(X, Y: Word): Pixel;
begin
	BufferGetPixel := Buffer^.Grid[X + Y * Buffer^.Width];
end;

{ Inserts given glyph at given point in the buffer. }
procedure BufferSetGlyph(X, Y: Word; Glyph: String);
begin
	Buffer^.Grid[X + Y * Buffer^.Width].Glyph := Glyph;
end;

{ Inserts given palette at given point in the buffer. }
procedure BufferSetPalette(X, Y, Palette: Word);
begin
	Buffer^.Grid[X + Y * Buffer^.Width].Palette := Palette;
end;

{ Inserts given pixel (glyph & palette) at given point in the buffer. }
procedure BufferSetPixel(X, Y: Word; Glyph: String; Palette: Word);
begin
	BufferSetGlyph(X, Y, Glyph);
	BufferSetPalette(X, Y, Palette);
end;

{ Creates the buffer of console width & height, & hides the cursor. }
procedure BufferStart;
begin
	if Buffer = nil then begin
		BufferCreate(ConsoleColumns, ConsoleRows - 1);
		BufferBoundaryReset;
		CursorHide;
	end;
end;

{ Destroys the buffer & shows the cursor. }
procedure BufferStop;
begin
	if Buffer <> nil then begin
		BufferDestroy;
		CursorShow;
	end;
end;

{ Fills given area of the buffer with given glyph. }
procedure BufferDrawArea(X0, Y0, X1, Y1: Word; Glyph: String; Palette: Word);
var
	X, Y: Word;
begin
	for Y := Y0 to Y1 do begin
		for X := X0 to X1 do begin
			BufferSetPixel(X, Y, Glyph, Palette);
		end;
	end;
end;

{ Draws a line of given glyph to the buffer between given points. }
procedure BufferDrawLine2(X0, Y0, X1, Y1: Word; Glyph: String; Palette: Word);
var
	DX, DY, X, XLength, Y, YLength: Integer;
	XSlope, YSlope: Real;
begin
	XLength := Abs(X0 - X1);
	if (X0 - X1) < 0 then DX := -1;
	if (X0 - X1) = 0 then DX := 0;
	if (X0 - X1) > 0 then DX := 1;
	YLength := Abs(Y0 - Y1);
	if (Y0 - Y1) < 0 then DY := -1;
	if (Y0 - Y1) = 0 then DY := 0;
	if (Y0 - Y1) > 0 then DY := 1;
	if DY = 0 then begin
		if DX < 0 then
			for X := X0 to X1 do
				BufferSetPixel(X, Y0, Glyph, Palette);
		if DX > 0 then
			for X := X1 to X0 do
				BufferSetPixel(X, Y0, Glyph, Palette);
		Exit;
	end;
	if DX = 0 then begin
		if DY < 0 then
			for Y := Y0 to Y1 do
				BufferSetPixel(X0, Y, Glyph, Palette);
		if DY > 0 then
			for Y := Y1 to Y0 do
				BufferSetPixel(X0, Y, Glyph, Palette);
		Exit;
	end;
	XSlope := XLength div YLength;
	YSlope := YLength div XLength;
	if (YSlope / XSlope < 1) and (YSlope / XSlope > -1) then begin
		if DX < 0 then
			for X := X0 to X1 do
				BufferSetPixel(X, Round(YSlope * X), Glyph, Palette);
		if DX > 0 then
			for X := X1 to X0 do
				BufferSetPixel(X, Round(YSlope * X), Glyph, Palette);
	end	else begin
		if DY < 0 then
			for Y := Y0 to Y1 do
				BufferSetPixel(Round(XSlope * Y), Y, Glyph, Palette);
		if DY > 0 then
			for Y := Y1 to Y0 do
				BufferSetPixel(Round(XSlope * Y), Y, Glyph, Palette);
	end;
end;

{ Draws a line of given glyph to the buffer between given points. }
procedure BufferDrawLine(X0, Y0, X1, Y1: Word; Glyph: String; Palette: Word);
var
	A, B, D, DiagInc, DXDiag, DXNondiag, DYDiag, DYNondiag, I, NondiagInc, Swp, X, Y: LongInt;
begin
	X := X0;
	Y := Y0;

	A := X1 - X0;
	B := Y1 - Y0;

	if A < 0 then begin
		A := -A;
		DXDiag := -1;
	end else begin
		DXDiag := 1;
	end;

	if B < 0 then begin
		B := -B;
		DYDiag := -1;
	end else begin
		DYDiag := 1;
	end;

	if A < B then begin
		Swp := A;
		A := B;
		B := Swp;
		DXNondiag := 0;
		DYNondiag := DYDiag;
	end else begin
		DXNondiag := DXDiag;
		DYNondiag := 0;
	end;
	D := 2 * B - A;
	NondiagInc := 2 * B;
	DiagInc := 2 * (B - A);
	for I := 0 to A do begin
		BufferSetPixel(X, Y, Glyph, Palette);

		if D < 0 then begin
			Inc(X, DXNondiag);
			Inc(Y, DYNondiag);
			Inc(D, NondiagInc);
		end else begin
			Inc(X, DXDiag);
			Inc(Y, DYDiag);
			Inc(D, DiagInc);
		end;
	end;
end;

{ Draws given text to the buffer starting at given point. }
procedure BufferDrawText(X, Y: Word; Glyphs: String; Palette: Word);
var
	CPLen, Key: Byte;
	Glyph: String;
	P: PChar;
begin
	P := PChar(Glyphs);
	repeat
		CPLen := UTF8CodePointLen(P, 4, true);
		Glyph := '';
		for Key := 0 to CPLen - 1 do begin
			Glyph := Glyph + P[Key];
		end;
		BufferSetPixel(X, Y, Glyph, Palette);
		Inc(P, CPLen);
		Wrap(X, Y);
	until (CPLen = 0) or (P^ = #0);
end;

{ Draws given text to the buffer centred at given row. }
procedure BufferDrawTextCentre(Row: Word; Glyphs: String; Palette: Word);
begin
	BufferDrawText((Buffer^.Width - UTFLength(Glyphs)) div 2, Row, Glyphs, Palette);
end;

{ Draws given text to the buffer left-aligned at given row. }
procedure BufferDrawTextLeft(Row: Word; Glyphs: String; Palette: Word);
begin
	BufferDrawText(0, Row, Glyphs, Palette);
end;

{ Draws given text to the buffer right-aligned at given row. }
procedure BufferDrawTextRight(Row: Word; Glyphs: String; Palette: Word);
begin
	BufferDrawText(Buffer^.Width - UTFLength(Glyphs), Row, Glyphs, Palette);
end;

{ Draws given text to the buffer within given boundary. }
procedure BufferDrawTextWrap(X0, Y0, X1, Y1: Word; Glyphs: String; Palette: Word);
begin
	BufferBoundarySet(X0, Y0, X1, Y1);
	BufferDrawText(X0, Y0, Glyphs, Palette);
	BufferBoundaryReset;
end;

{ Resets the boundaries to the dimensions of the buffer. }
procedure BufferBoundaryReset;
begin
	BufferBoundarySet(0, 0, Buffer^.Width - 1, Buffer^.Height - 1);
end;

{ Sets the boundary of the buffer to given dimension. }
procedure BufferBoundarySet(X0, Y0, X1, Y1: Word);
begin
	BottomX := X1;
	BottomY := Y1;
	TopX := X0;
	TopY := Y0;
end;

{ Counts the number of "visible" characters in an UTF-8 string. }
function UTFLength(const S: String): Word;
var
	CPLen: Byte;
	P: PChar;
begin
	UTFLength := 0;
	P := PChar(S);
	repeat
		CPLen := UTF8CodePointLen(P, 4, true);
		Inc(UTFLength);
		Inc(P, CPLen);
	until (CPLen = 0) or (P^ = #0);
end;

end.
