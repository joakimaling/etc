unit ConsoleProgress;

interface

uses
	ConsoleBuffer,
	ConsoleFrame,
	ConsolePalette;

type
	PProgress = ^TProgress;
	TProgress = record
		Disabled, Focused: Boolean;
		Frame: PFrame;
		Palette: PStatePalette;
		Value: Byte;
		Width, X, Y: Word;
	end;

function ProgressCreate(X, Y, Width: Word): PProgress;
procedure ProgressDestroy(Progress: PProgress);
procedure ProgressDisable(Progress: PProgress);
procedure ProgressDraw(Progress: PProgress);
procedure ProgressEnable(Progress: PProgress);
procedure ProgressUpdate(Progress: PProgress; Value: Byte);

implementation

{ Creates a new progress bar with given data. }
function ProgressCreate(X, Y, Width: Word): PProgress;
begin
	GetMem(ProgressCreate, SizeOf(TProgress));
	if ProgressCreate <> nil then begin
		ProgressCreate^.Disabled := false;
		ProgressCreate^.Focused := false;
		ProgressCreate^.Palette := StatePalette;
		ProgressCreate^.Value := 0;
		ProgressCreate^.Width := Width;
		ProgressCreate^.X := X;
		ProgressCreate^.Y := Y;
	end;
end;

{ Destroys given progress bar. }
procedure ProgressDestroy(Progress: PProgress);
begin
	FreeMem(Progress, SizeOf(TProgress));
end;

{ Disables given progress bar. }
procedure ProgressDisable(Progress: PProgress);
begin
	Progress^.Disabled := true;
end;

{ Draws given progress bar to its frame. }
procedure ProgressDraw(Progress: PProgress);
var
	Done, Hue: Word;
begin
	with Progress^ do begin
		Hue := Palette^.Enabled;
		if Disabled then Hue := Palette^.Disabled;
		if Focused then Hue := Palette^.Focused;

		Done := (Value * Width) div 100;
		if Done > 0 then BufferDrawLine(X, Y, X + Done - 1, Y, '█', Hue);
		BufferDrawLine(X + Done, Y, X + Width, Y, #32, Hue);
	end;
end;

{ Enables given progress bar. }
procedure ProgressEnable(Progress: PProgress);
begin
	Progress^.Disabled := false;
end;

{ Updates the value of the progress bar. }
procedure ProgressUpdate(Progress: PProgress; Value: Byte);
begin
	Progress^.Value := Value;
end;

end.
