unit ConsoleMenu;

interface

uses
	ConsoleBuffer,
	ConsolePalette,
	StringUtilities;

type
	TPosition = (Top, Bottom);

	PMenuItem = ^TMenuItem;
	TMenuItem = record
		Action: TProcedure;
		Children: Array of PMenuItem;
		Disabled, Focused: Boolean;
		Palette: PStatePalette;
		Parent: PMenuItem;
		Title: UTFString;
	end;

	PMenu = ^TMenu;
	TMenu = record
		MenuItems: Array of PMenuItem;
		Palette: PStatePalette;
		Position: TPosition;
	end;

function MenuItemCreate(Title: UTFString): PMenuItem;
procedure MenuItemDestroy(MenuItem: PMenuItem);
procedure MenuItemDisable(MenuItem: PMenuItem);
procedure MenuItemDraw(MenuItem: PMenuItem);
procedure MenuItemEnable(MenuItem: PMenuItem);
procedure MenuItemLink(MenuItem, Child: PMenuItem);
procedure MenuItemSelect(MenuItem: PMenuItem);

function MenuCreate(Position: TPosition): PMenu;
procedure MenuDestroy(Menu: PMenu);
procedure MenuDraw(Menu: PMenu);
procedure MenuLink(Menu: PMenu; MenuItem: PMenuItem);

implementation

{ Creates a new menu item with given data. }
function MenuItemCreate(Title: UTFString): PMenuItem;
begin
	GetMem(MenuItemCreate, SizeOf(TMenuItem));
	if MenuItemCreate <> nil then begin
		MenuItemCreate^.Action := nil;
		MenuItemCreate^.Disabled := false;
		MenuItemCreate^.Focused := false;
		MenuItemCreate^.Parent := nil;
		MenuItemCreate^.Title := Title;
		SetLength(MenuItemCreate^.Children, 0);
	end;
end;

{ Destroys given menu item. }
procedure MenuItemDestroy(MenuItem: PMenuItem);
var
	Key: Byte;
begin
	if Length(MenuItem^.Children) > 0 then begin
		for Key := 0 to High(MenuItem^.Children) do begin
			MenuItemDestroy(MenuItem^.Children[Key]);
		end;
	end;

	FreeMem(MenuItem, SizeOf(TMenuItem));
end;

{ Disables given menu item. }
procedure MenuItemDisable(MenuItem: PMenuItem);
begin
	MenuItem^.Disabled := true;
end;

{ Draws given menu item to the buffer. }
procedure MenuItemDraw(MenuItem: PMenuItem);
var
	Hue: Word;
begin
	with MenuItem^ do begin
		Hue := Palette^.Enabled;
		if Disabled then Hue := Palette^.Disabled;
		if Focused then Hue := Palette^.Focused;

		BufferDrawText(0, 0, Title, Hue);
		if MenuItem^.Children <> nil then begin
			BufferSetPixel(UTFLength(Title) + 1, 0, '>', Hue);
		end;
	end;
end;

{ Enables given menu item. }
procedure MenuItemEnable(MenuItem: PMenuItem);
begin
	MenuItem^.Disabled := false;
end;

{ Returns true if this menu is located at the top. }
function MenuItemIsTop(MenuItem: PMenuItem): Boolean;
begin
	MenuItemIsTop := MenuItem^.Parent = nil;
end;

{ Appends a sub menu to the list of sub menus. }
procedure MenuItemLink(MenuItem, Child: PMenuItem);
begin
	SetLength(MenuItem^.Children, 1 + Length(MenuItem^.Children));
	MenuItem^.Children[Length(MenuItem^.Children) - 1] := Child;
	Child^.Parent := MenuItem;
end;

{ Draws the sub menu if it has chidren, or runs its procedure. }
procedure MenuItemSelect(MenuItem: PMenuItem);
var
	Key: Byte;
begin
	if MenuItem^.Children <> nil then begin
		for Key := 0 to High(MenuItem^.Children) do begin
			MenuItemDraw(MenuItem^.Children[Key]);
		end;
	end else begin
		MenuItem^.Action;
	end;
end;

{ Creates a new menu with given data.}
function MenuCreate(Position: TPosition): PMenu;
begin
	GetMem(MenuCreate, SizeOf(TMenu));
	if MenuCreate <> nil then begin
		MenuCreate^.Palette := StatePalette;
		MenuCreate^.Position := Position;
		SetLength(MenuCreate^.MenuItems, 0);
	end;
end;

{ Destroys given menu. }
procedure MenuDestroy(Menu: PMenu);
begin
	FreeMem(Menu, SizeOf(TMenu));
end;

{ Draws given menu to the buffer. }
procedure MenuDraw(Menu: PMenu);
var
	Key: Byte;
	X, Y: Word;
begin
	with Buffer^, Menu^ do begin
		X := 0;
		Y := Ord(Position) * (Height - 1);
		BufferDrawLine(0, Y, Width - 1, Y, #32, Palette^.Enabled);
		if Length(MenuItems) > 0 then begin
			for Key := 0 to High(MenuItems) do begin
				BufferDrawText(X, Y, MenuItems[Key]^.Title, Palette^.Enabled);
				Inc(X, UTFLength(MenuItems[Key]^.Title));
			end;
		end;
	end;
end;

{ Appends a menu item to the list of menu items. }
procedure MenuLink(Menu: PMenu; MenuItem: PMenuItem);
begin
	SetLength(Menu^.MenuItems, 1 + Length(Menu^.MenuItems));
	Menu^.MenuItems[High(Menu^.MenuItems)] := MenuItem;
end;

end.
