unit ConsoleFrame;

interface

uses
	ConsoleBuffer,
	ConsolePalette;

type
	PFrame = ^TFrame;
	TFrame = record
		Components: Array of Pointer;
		Disabled, Focused: Boolean;
		Height, Width: Word;
		Palette: PStatePalette;
		Title: String;
		X, Y: Word;
	end;

function FrameCreate(Title: String; X, Y, Width, Height: Word): PFrame;
procedure FrameDestroy(Frame: PFrame);
procedure FrameDisable(Frame: PFrame);
procedure FrameDraw(Frame: PFrame);
procedure FrameEnable(Frame: PFrame);
procedure FrameMove(Frame: PFrame; X, Y: Word);
procedure FrameRename(Frame: PFrame; Title: String);
procedure FrameResize(Frame: PFrame; Width, Height: Word);

procedure FrameAttach(Frame: PFrame; Component: Pointer);
procedure FrameDetach(Frame: PFrame; Component: Pointer);

implementation

{ Creates a new frame with given data. }
function FrameCreate(Title: String; X, Y, Width, Height: Word): PFrame;
begin
	GetMem(FrameCreate, SizeOf(TFrame));
	if FrameCreate <> nil then begin
		//SetLength(FrameCreate^.Components, 0);
		FrameCreate^.Disabled := false;
		FrameCreate^.Focused := false;
		FrameCreate^.Height := Height;
		FrameCreate^.Palette := StatePalette;
		FrameCreate^.Title := Title;
		FrameCreate^.Width := Width;
		FrameCreate^.X := X;
		FrameCreate^.Y := Y;
	end;
end;

{ Destroys given frame. }
procedure FrameDestroy(Frame: PFrame);
begin
	FreeMem(Frame, SizeOf(TFrame));
end;

{ Disables given frame. }
procedure FrameDisable(Frame: PFrame);
begin
	Frame^.Disabled := true;
end;

{ Draws given frame to the buffer. }
procedure FrameDraw(Frame: PFrame);
var
	Hue: Word;
begin
	with Frame^ do begin
		Hue := Palette^.Enabled;
		if Disabled then Hue := Palette^.Disabled;
		if Focused then Hue := Palette^.Focused;

		BufferDrawArea(X + 1, Y + 1, X + Width - 2, Y + Height - 2, #32, Hue);
		BufferDrawLine(X + 1, Y, X + Width - 2, Y, '─', Hue);
		BufferDrawText(X + (Width - (UTFLength(Title) + 2)) div 2, Y, ' ' + Title + ' ', Hue);
		BufferDrawLine(X, Y + 1, X, Y + Height - 2, '│', Hue);
		BufferDrawLine(X + Width - 1, Y + 1, X + Width - 1, Y + Height - 2, '│', Hue);
		BufferDrawLine(X + 1, Y + Height - 1, X + Width - 2, Y + Height - 1, '─', Hue);
		BufferSetPixel(X + Width - 1, Y + Height - 1, '┘', Hue);
		BufferSetPixel(X, Y + Height - 1, '└', Hue);
		BufferSetPixel(X + Width - 1, Y, '┐', Hue);
		BufferSetPixel(X, Y, '┌', Hue);

		for Hue := 0 to Width - 1 do begin
			BufferSetPalette(X + 1 + Hue, Y + Height, $ebf0);
		end;

		for Hue := 0 to Height - 1 do begin
			BufferSetPalette(X + Width, Y + 1 + Hue, $ebf0);
		end;
	end;
end;

{ Enables given frame. }
procedure FrameEnable(Frame: PFrame);
begin
	Frame^.Disabled := false;
end;

{ Updates the position of given frame. }
procedure FrameMove(Frame: PFrame; X, Y: Word);
begin
	Frame^.X := X;
	Frame^.Y := Y;
end;

{ Updates the title of given frame. }
procedure FrameRename(Frame: PFrame; Title: String);
begin
	Frame^.Title := Title;
end;

{ Updates the size of given frame. }
procedure FrameResize(Frame: PFrame; Width, Height: Word);
begin
	Frame^.Height := Height;
	Frame^.Width := Width;
end;

{ Adds given component to given frame. }
procedure FrameAttach(Frame: PFrame; Component: Pointer);
begin
	SetLength(
	Frame^.Components,
	1 +
	Length(
	Frame^.Components
	));
	Frame^.Components[High(Frame^.Components)] := Component;
end;

{ Removes a given component from given frame. }
procedure FrameDetach(Frame: PFrame; Component: Pointer);
begin

end;

end.
