program Test;

uses
	ConsoleBuffer,
	//ConsoleFrame,
	//ConsoleMenu,
	//ConsoleProgress,
	Keyboard;

var
	//Frame: PFrame;
	//Menu: PMenu;
	//Item0, Item1, Item2, Item3, Item4, Item5, Item6, Item7, Item8: PMenuItem;

	//Progress: PProgress;
	KeyEvent: TKeyEvent;
	Quit: Boolean = false;

begin
	InitKeyboard;
	BufferGlyph := '~';
	BufferPalette := $5816;
	BufferStart;
	//Frame := FrameCreate('Menu test', 4, 7, 30, 5);
	//Menu := MenuCreate(Top);
	//Item0 := MenuItemCreate(' Item #1 ');
	//Item1 := MenuItemCreate(' Item #2 ');
	//Item2 := MenuItemCreate(' Item #3 ');

	//MenuLink(Menu, Item0);
//	MenuLink(Menu, Item1);
//	MenuLink(Menu, Item2);
//	MenuItemLink(Item2, Item3);
//	MenuItemLink(Item2, Item4);
//	MenuItemLink(Item2, Item5);
//	MenuItemLink(Item4, Item6);
//	MenuItemLink(Item4, Item7);
//	MenuItemLink(Item4, Item8);

	repeat
		//FrameDraw(Frame);
		//MenuAddMenu(Menu);
//		MenuDraw(Menu);
		BufferDraw;

		KeyEvent := TranslateKeyEvent(GetKeyEvent);

		case GetKeyEventChar(KeyEvent) of
			//'t': FrameResize(Frame, Frame^.Width, Frame^.Height - 1);
		//	'g': FrameResize(Frame, Frame^.Width, Frame^.Height + 1);
		//	'f': FrameResize(Frame, Frame^.Width - 1, Frame^.Height);
		//	'h': FrameResize(Frame, Frame^.Width + 1, Frame^.Height);
		//	'w': FrameMove(Frame, Frame^.X, Frame^.Y - 1);
		//	's': FrameMove(Frame, Frame^.X, Frame^.Y + 1);
		//	'a': FrameMove(Frame, Frame^.X - 1, Frame^.Y);
		//	'd': FrameMove(Frame, Frame^.X + 1, Frame^.Y);
	//		'o': FrameDisable(Frame);
		//	'c': FrameEnable(Frame);
//			'z': MenuItemSelect();
			'q': Quit := true;
		end;
	until Quit;

//	MenuItemDestroy(Item0);
//	MenuItemDestroy(Item1);
//	MenuItemDestroy(Item2);
//	MenuItemDestroy(Item3);
//	MenuItemDestroy(Item4);
//	MenuItemDestroy(Item5);
//	MenuItemDestroy(Item6);
//	MenuItemDestroy(Item7);
//	MenuItemDestroy(Item8);
//	MenuDestroy(Menu);
	//FrameDestroy(Frame);
	BufferStop;
	DoneKeyboard;
end.
