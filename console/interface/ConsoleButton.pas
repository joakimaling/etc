unit ConsoleButton;

interface

uses
	ConsoleFrame,
	ConsolePalette,
	StringUtilities;

type
	PButton = ^TButton;
	TButton = record
		Action: TProcedure;
		Disabled, Focused: Boolean;
		Frame: PFrame;
		Palette: PStatePalette;
		Title: UTF8String;
		X, Y: Word;
	end;

function ButtonCreate(Frame: PFrame; Title: UTFString; X, Y: Word; Action: TProcedure): PButton;
procedure ButtonDestroy(Button: PButton);
procedure ButtonDisable(Button: PButton);
procedure ButtonDraw(Button: PButton);
procedure ButtonEnable(Button: PButton);

implementation

{ Creates a new button with given data. }
function ButtonCreate(Frame: PFrame; Title: UTFString; X, Y, Palette: Word;
	Action: TProcedure): PButton;
begin
	GetMem(ButtonCreate, SizeOf(TButton));
	if ButtonCreate <> nil then begin
		ButtonCreate^.Action := Action;
		ButtonCreate^.Disabled := false;
		ButtonCreate^.Focused := false;
		ButtonCreate^.Frame := Frame;
		ButtonCreate^.Palette := StatePalette;
		ButtonCreate^.Title := Title;
		ButtonCreate^.X := X;
		ButtonCreate^.Y := Y;
	end;
end;

{ Destroys given button. }
procedure ButtonDestroy(Button: PButton);
begin
	FreeMem(Button, SizeOf(TButton));
end;

{ Disables given button. }
procedure ButtonDisable(Button: PButton);
begin
	Button^.Disabled := true;
end;

{ Draws given button to its frame. }
procedure ButtonDraw(Button: PButton);
var
	Hue: Word;
begin
	with Button^ do begin
		Hue := Palette^.Enabled;
		if Disabled then Hue := Palette^.Disabled;
		if Focused then Hue := Palette^.Focused;

		BufferDrawText(X, Y, ' ' + Title + ' ', Hue);
	end;
end;

{ Enables given button. }
procedure ButtonEnable(Button: PButton);
begin
	Button^.Disabled := false;
end;

end.
