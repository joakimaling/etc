unit ConsoleTextArea;

interface

uses
	ConsoleBuffer,
	ConsolePalette;

type
	PTextArea = ^TTextArea;
	TTextArea = record
		Content: String;
		Disabled, Focused: Boolean;
		Palette: PStatePalette;
		Width, Height: Word;
		X, Y: Word;
	end;

function TextAreaCreate(X, Y, Width, Height: Word): PTextArea;
procedure TextAreaDestroy(TextArea: PTextArea);
procedure TextAreaDisable(TextArea: PTextArea);
procedure TextAreaDraw(TextArea: PTextArea);
procedure TextAreaEnable(TextArea: PTextArea);
procedure TextAreaErase(TextArea: PTextArea);
procedure TextAreaMove(TextArea: PTextArea; X, Y: Word);
procedure TextAreaResize(TextArea: PTextArea; Width, Height: Word);
procedure TextAreaWrite(TextArea: PTextArea; Content: String);

implementation

{ Creates a new text area with given data. }
function TextAreaCreate(X, Y, Width, Height: Word): PTextArea;
begin
	GetMem(TextAreaCreate, SizeOf(TTextArea));
	if TextAreaCreate <> nil then begin
		TextAreaCreate^.Content := '';
		TextAreaCreate^.Height := Height;
		TextAreaCreate^.Palette := StatePalette;
		TextAreaCreate^.Width := Width;
		TextAreaCreate^.X := X;
		TextAreaCreate^.Y := Y;
	end;
end;

{ Destroys given text area. }
procedure TextAreaDestroy(TextArea: PTextArea);
begin
	FreeMem(TextArea, SizeOf(TTextArea));
end;

{ Disables given text area. }
procedure TextAreaDisable(TextArea: PTextArea);
begin
	TextArea^.Disabled := true;
end;

{ Draws given text area to the buffer. }
procedure TextAreaDraw(TextArea: PTextArea);
var
	Hue: Word;
begin
	with TextArea^ do begin
		Hue := Palette^.Enabled;
		if Disabled then Hue := Palette^.Disabled;
		if Focused then Hue := Palette^.Focused;

		BufferDrawTextWrap(X, Y, X + Width - 1, Y + Height - 1, Content, Hue);
	end;
end;

{ Enables given text area. }
procedure TextAreaEnable(TextArea: PTextArea);
begin
	TextArea^.Disabled := false;
end;

{ Clears the content of given text area. }
procedure TextAreaErase(TextArea: PTextArea);
begin
	TextArea^.Content := '';
end;

{ Updates the position of given text area. }
procedure TextAreaMove(TextArea: PTextArea; X, Y: Word);
begin
	TextArea^.X := X;
	TextArea^.Y := Y;
end;

{ Updates the size of given text area. }
procedure TextAreaResize(TextArea: PTextArea; Width, Height: Word);
begin
	TextArea^.Height := Height;
	TextArea^.Width := Width;
end;

{ Appends given string to the content of given text area. }
procedure TextAreaWrite(TextArea: PTextArea; Content: String);
begin
	TextArea^.Content := TextArea^.Content + Content;
end;

end.
