unit ConsolePalette;

interface

type
	PFramePalette = ^TFramePalette;
	TFramePalette = record
		Minify, Quit: Word;
	end;

	PStatePalette = ^TStatePalette;
	TStatePalette = record
		Disabled, Enabled, Focused: Word;
	end;

function PaletteCreate(Disabled, Enabled, Focused: Word): PStatePalette;
procedure PaletteDestroy(Palette: PStatePalette);
function PaletteDuplicate: PStatePalette;
function PaletteInvert(Palette: Word): Word;
procedure PaletteSetBackground(var Palette: Word; Hue: Byte);
procedure PaletteSetForeground(var Palette: Word; Hue: Byte);

implementation

{ Creates a new state palette. }
function PaletteCreate(Disabled, Enabled, Focused: Word): PStatePalette;
begin
	GetMem(PaletteCreate, SizeOf(TStatePalette));
	if PaletteCreate <> nil then begin
		PaletteCreate^.Disabled := Disabled;
		PaletteCreate^.Enabled := Enabled;
		PaletteCreate^.Focused := Focused;
	end;
end;

{ Destroys given state palette. }
procedure PaletteDestroy(Palette: PStatePalette);
begin
	FreeMem(Palette, SizeOf(TStatePalette));
end;

{ Copies & returns the state palette as a new one. }
function PaletteDuplicate: PStatePalette;
begin
	PaletteDuplicate := PaletteCreate(
		StatePalette^.Disabled,
		StatePalette^.Enabled,
		StatePalette^.Focused
	);
end;

{ Swap the bytes, effectively inverting the colours. }
function PaletteInvert(Palette: Word): Word;
begin
	PaletteInvert := Swap(Palette);
end;

{ Replace the high byte with given, effectively changing the bacground colour. }
procedure PaletteSetBackground(var Palette: Word; Hue: Byte);
begin
	Palette := Word(Hue shl 8) + Lo(Palette);
end;

{ Replace the low byte with given, effectively changing the foreground colour. }
procedure PaletteSetForeground(var Palette: Word; Hue: Byte);
begin
	Palette := Palette and $ff00 + Hue;
end;

end.
