unit ConsoleEditBox;

interface

uses
	ConsolePalette,
	ConsoleUtilities,
	StringUtilities;

type
	PEditBox = ^TEditBox;
	TEditBox = record
		Active: Boolean;
		Content: UTFString;
		Height, Width: Word;
		Palette: PStatePalette;
		//Rules: PRules;
		X, Y: Word;
	end;

implementation



end.
