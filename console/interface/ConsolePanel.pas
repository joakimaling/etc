unit ConsolePanel;

interface

uses
	ConsoleBuffer,
	ConsolePalette;

type
	TPosition = (Bottom, Top);

	PPanel = ^TPanel;
	TPanel = record
		Components: Array of Pointer;
		Disabled: Boolean;
		Palette: PStatePalette;
		Position: TPosition;
	end;

function PanelCreate(Position: TPosition): PPanel;
procedure PanelDestroy(Panel: PPanel);
procedure PanelDisabale(Panel: PPanel);
procedure PanelDraw(Panel: PPanel);
procedure PanelEnable(Panel: PPanel);

procedure PanelAddComponent(Panel: PPanel; Component: Pointer);

implementation

{ Creates a new panel with given data. }
function PanelCreate(Position: TPosition): PPanel;
begin
	GetMem(PanelCreate, SizeOf(TPanel));
	if PanelCreate <> nil then begin
		PanelCreate^.Disabled := false;
		PanelCreate^.Palette := StatePalette;
		PanelCreate^.Position := Position;
	end;
end;

{ Destroys given panel. }
procedure PanelDestroy(Panel: PPanel);
begin
	FreeMem(Panel, SizeOf(TPanel));
end;

{ Disables given panel. }
procedure PanelDisabale(Panel: PPanel);
begin
	Panel^.Disabled := true;
end;

{ Draws given panel to the buffer. }
procedure PanelDraw(Panel: PPanel);
var
	Hue: Word;
begin
	with Panel^ do begin
		Hue := Palette^.Enabled;
		if Disabled then Hue := Palette^.Disabled;

		BufferDrawLine(0, 0, Buffer^.Width -1, 0, #32, Hue);
	end;
end;

{ Enables given panel. }
procedure PanelEnable(Panel: PPanel);
begin
	Panel^.Disabled := false;
end;

{ Adds given component to given panel. }
procedure PanelAddComponent(Panel: PPanel; Component: Pointer);
begin
	SetLength(Panel^.Components, 1 + Length(Panel^.Components));
	Panel^.Components[High(Panel^.Components)] := Component;
end;

end.
