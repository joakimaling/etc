unit GameEngine;

interface

uses
{$ifdef unix}
	CThreads,
	CMem,
{$endif}
	ConsoleBuffer;

var
	Frames: Cardinal = 0;
	FramesPerSecond: Byte = 60;

{ General routines }
procedure EngineCreate;
procedure EngineDestroy;
procedure EngineStart;
procedure EngineStop;

implementation

var
	IsRunning: Boolean = false;
	Thread: TThreadID;

{ Initialises the game engine & its state. }
procedure EngineCreate;
begin

end;

{ Destroys the game engine. }
procedure EngineDestroy;
begin

end;

{ Draws graphics of the game to the buffer & and the buffer to the console. }
procedure EngineDraw;
begin
	BufferDraw;
end;

{ Runs the main game loop. }
function EngineRun(P: Pointer): PtrInt;
var
	Key: Word = 0;
begin
	Run := 0;

	while IsRunning do begin
		WriteLn('Key: ', Key);
		Inc(Key);
		if Key >= 100 then Stop;
	//	HandleEvents;
		//Tick;
	//	Draw;
	end;
end;

{ Creates the thread & calls the run routine. }
procedure EngineStart;
begin
	if not IsRunning then begin
		IsRunning := true;
		Thread := BeginThread(@EngineRun, nil);
	end;
end;

{ Destroys the thread & exits the run routine. }
procedure EngineStop;
begin
	if IsRunning then begin
		IsRunning := false;
		CloseThread(Thread);
	end;
end;

{ Handles the game logic. }
procedure EngineTick;
begin

end;

end.
