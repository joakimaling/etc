unit GameHelper;

interface

uses
	ConsoleBuffer,
	StringUtilities,
	Keyboard;

procedure QuickFrame(X, Y, Width, Height: Byte; Title: UTFString);
function QuickMenu(Row: Word; Items: Array of UTFString): Byte;

implementation

var
	Palette: Word = $10ff;

{ Draws a simple frame using ASCII characters with given title. }
procedure QuickFrame(X, Y, Width, Height: Byte; Title: UTFString);
begin
	BufferDrawArea(X + 1, Y + 1, X + Width - 1, Y + Height - 1, #32, Palette);
	BufferDrawLine(X + 1, Y, X + Width - 1, Y, '-', Palette);
	BufferDrawText(X + (Width - (UTFLength(Title) + 2)) div 2, Y, ' ' + Title +
		' ', Palette);
	BufferDrawLine(X, Y + 1, X, Y + Height - 1, '|', Palette);
	BufferDrawLine(X + Width, Y + 1, X + Width, Y + Height - 1, '|', Palette);
	BufferDrawLine(X + 1, Y + Height, X + Width - 1, Y + Height, '-', Palette);
	BufferDrawGlyph(X, Y, '+', Palette);
	BufferDrawGlyph(X + Width, Y, '+', Palette);
	BufferDrawGlyph(X, Y + Height, '+', Palette);
	BufferDrawGlyph(X + Width, Y + Height, '+', Palette);
end;

{ Displays a simple menu centred starting at given row on the buffer. Use the
  Up/Down arrows to highlight a menu item & Enter to select it. }
function QuickMenu(Row: Word; Items: Array of UTFString): Byte;
var
	Hue: Word;
	KeyEvent: TKeyEvent;
	Key, Pick: Byte;
begin
	Pick := 0;
	repeat
		for Key := 0 to High(Items) do begin
			if Key = Pick then Hue := Swap(Palette) else Hue := Palette;
			BufferDrawTextCentred(Row + Key, Items[Key], Hue);
		end;
		KeyEvent := TranslateKeyEvent(GetKeyEvent);
		case GetKeyEventCode(KeyEvent) of
			65313: if Pick > 0 then Dec(Pick) else Pick := High(Items);
			65319: if Pick < High(Items) then Inc(Pick) else Pick := 0;
		end;
	until 7181 = GetKeyEventCode(KeyEvent);
	QuickMenu := Pick;
end;

end.
