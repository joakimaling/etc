{
  This unit serves as an extension to SysUtils by adding "missing" routines
  which are commonly used in various coding projects.
}
unit StringHelper;

interface

uses
	SysUtils;

const
	WhiteSpace = [#0..#32];

type
	TStringArray = Array of String;

function IsAlpha(Glyph: Char): Boolean;
function IsAlphaString(Glyphs: String): Boolean;
function Split(const S: String; const CSet: TSysCharSet): TStringArray;
function Split(const S: String): TStringArray;

implementation

var
	Index: Byte;

{}
function IsAlpha(Glyph: Char): Boolean;
begin
	IsAlpha := UpCase(Glyph) in ['A'..'Z'];
end;

{}
function IsAlphaString(Glyphs: String): Boolean;
begin
	IsAlphaString := true;
	for Index := 0 to High(Glyphs) do begin
		if not IsAlpha(Glyphs[Index]) then begin
			IsAlphaString := false;
			Break;
		end;
	end;
end;

{}
function Split(const S: String; const CSet: TSysCharSet): TStringArray;
var
	Part: String = '';
begin
	for Index := 0 to High(S) do begin
		if S[Index] in CSet then begin
			SetLength(Split, 1 + Length(Split));
			Split[High(Split)] := Part;
			Part := '';
		end else begin
			Part[Length(Part) - 1] := S[Index];
		end;
	end;
end;

{}
function Split(const S: String): TStringArray;
begin
	Split := Split(S, WhiteSpace);
end;

end.
