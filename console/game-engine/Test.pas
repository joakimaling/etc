program Test;

uses
	ConsoleBuffer,
	GameHelper,
	StringUtilities,
	Keyboard;

var
	MenuItems: Array[0..3] of UTFString;
	Reply: Byte;

begin
	InitKeyboard;
	MenuItems[0] := 'Item #1';
	MenuItems[1] := 'Item #2';
	MenuItems[2] := 'Item #3';
	MenuItems[3] := 'Quit';

	BufferDrawTextCentred(5, 'Testing this', $2418);

	QuickFrame(2, 2, 10, 4, 'Test');


	Reply := QuickMenu(10, MenuItems);

	WriteLn('Selected: ', Reply);
	BufferDraw;
	DoneKeyboard;
end.
