const
	DaysPerYear: Double = 365.2425;
type
	ValueRelationship = -1..1;
procedure EncodeDate(var Date: TDate; Day, Month, Year: SmallInt);
	var a, y, m: Word;
begin
	a := (14 - Month) div 12;
	y := Year + 4800 - a;
	m := Month + 12 * a - 3;
	Date := Day + (153 * m + 2) div 5 + 365 * y + y div 4 - y div 100 + y div 400 - 32045;
end;
{--------------------------------------------------------------------------------------------------}
procedure DecodeDate(Date: TDate; var Day, Month, Year: SmallInt);
	var j, g, dg, c, dc, b, db, a, da, y, m, d: LongInt;
begin
	//j := (Date + 0.5) + 32044; todo lÃ¤gg till dÃ¥ tid implementeras...
	j := Date + 32044; //todo tag bort
	g := j div 146097;
	dg := j mod 146097;
	c := (dg div 36524 + 1) * 3 div 4;
	dc := dg - c * 36524;
	b := dc div 1461;
	db := dc mod 1461;
	a := (db div 365 + 1) * 3 div 4;
	da := db - a * 365;
	y := g * 400 + c * 100 + b * 4 + a;
	m := (da * 5 + 308) div 153 - 2;
	d := da - (m + 4) * 153 div 5 + 122;
	Year := y - 4800 + (m + 2) div 12;
	Month := (m + 2) mod 12 + 1;
	Day := d + 1;
end;
{--------------------------------------------------------------------------------------------------}
function ValidDate(Day, Month, Year: SmallInt): Boolean;
begin
	if (Day < 1) or (Year < -4800) or ((Year = -4800) and (Month < 3)) then
		ValidDate := false
	else
		case Month of
			1, 3..12: ValidDate := Day <= 30 + (Month + Month div 8) mod 2;
			2: ValidDate := Day <= 28 + Ord(LeapYear(Year));
			else ValidDate := false;
		end;
end;
{--------------------------------------------------------------------------------------------------}
function WeekNumber(Date: TDate): SmallInt;
	var Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	WeekNumber := ((OrdinalDate[Month, LeapYear(Year)] + Day) - SmallInt(DayOfWeek(Date)) + 11) div 7;
end;
{--------------------------------------------------------------------------------------------------}
function WeekOfMonth(Date: TDate): SmallInt;
	var Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	WeekOfMonth := (Day - 1) div 7 + 1;
end;
{--------------------------------------------------------------------------------------------------}
function DayOfWeek(Date: TDate): TDays;
begin
	DayOfWeek := TDays(Date mod 7);
end;
{--------------------------------------------------------------------------------------------------}
function DayOfYear(Date: TDate): SmallInt;
	var Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	DayOfYear := OrdinalDate[Month, LeapYear(Year)] + Day;
end;
{--------------------------------------------------------------------------------------------------}
function FirstWeekDay: TDays;
begin
	FirstWeekDay := TDays(TimeSettings.FirstWeekDay);
end;
{--------------------------------------------------------------------------------------------------}
function FormatDate(const Format: String; Date: TDate): String;
	var Day, Month, Year: SmallInt;
		Temp: byte = 1;
begin
	FormatDate := '';
	{todo sort this thing up!!!
	%a - Locale's abbreviated weekday name
	%A - Locale's full weekday name
	%b - Locale's abbreviated month name
	%B - Locale's full month name
	%d - Day of the month as a decimal number [1..31]
	%D - Day of the month as a decimal number [01..31]
	%j - Day of the year as a decimal number [1..366]
	%J - Day of the year as a decimal number [001..366]
	%m - Month as a decimal number [1..12]
	%M - Month as a decimal number [01..12]
	%n - A <newline>
	%t - A <tab>
	%u - Weekday as a decimal number [1..7] (1 = Monday)
	%U - Week of the year (Sunday as the first day of the week) as a decimal number [00,53]. All days in a new year preceding the first Sunday shall be considered to be in week 0
	%V - Week of the year (Monday as the first day of the week) as a decimal number [01,53]. If the week containing January 1 has four or more days in the new year, then it shall be considered week 1; otherwise, it shall be the last week of the previous year, and the next week shall be week 1
	%w - Weekday as a decimal number [0..6] (0 = Sunday)
	%W - Week of the year (Monday as the first day of the week) as a decimal number [00,53]. All days in a new year preceding the first Monday shall be considered to be in week 0
	%y - Year within century [00..99]
	%Y - Year with century as a decimal number
	%% - A <percent-sign> character

	%a	 Represents the abbreviated weekday name (for example, Sun) defined by the abday statement.
	%A	 Represents the full weekday name (for example, Sunday) defined by the day statement.
	%b	 Represents the abbreviated month name (for example, Jan) defined by the abmon statement.
	%B	 Represents the full month name (for example, January) defined by the month statement.
	%c	 Represents the time-and-date format defined by the d_t_fmt statement.
	%C	 Represents the century as a decimal number (00 to 99).
	%d	 Represents the day of the month as a decimal number (01 to 31).
	%D	 Represents the date in %m/%d/%y format (for example, 01/31/91).
	%e	 Represents the day of the month as a decimal number (01 to 31). The %e field descriptor uses a two-digit field. If the day of the month is not a two-digit number, the leading digit is filled with a space character.
	%Ec	 Specifies the locale's alternate appropriate date and time representation.
	%EC	 Specifies the name of the base year (period) in the locale's alternate representation.
	%Ex	 Specifies the locale's alternate date representation.
	%EX	 Specifies the locale's alternate time representation.
	%Ey	 Specifies the offset from the %EC (year only) field descriptor in the locale's alternate representation.
	%EY	 Specifies the full alternate year representation.
	%Od	 Specifies the day of the month using the locale's alternate numeric symbols.
	%Oe	 Specifies the day of the month using the locale's alternate numeric symbols.
	%OH	 Specifies the hour (24-hour clock) using the locale's alternate numeric symbols.
	%OI	 Specifies the hour (12-hour clock) using the locale's alternate numeric symbols.
	%Om	 Specifies the month using the locale's alternate numeric symbols.
	%OM	 Specifies the minutes using the locale's alternate numeric symbols.
	%OS	 Specifies the seconds using the locale's alternate numeric symbols.
	%OU	 Specifies the week number of the year (Sunday as the first day of the week) using the locale's alternate numeric symbols.
	%Ow	 Specifies the weekday as a number in the locale's alternate representation (Sunday = 0).
	%OW	 Specifies the week number of the year (Monday as the first day of the week) using the locale's alternate numeric symbols.
	%Oy	 Specifies the year (offset from the %C field descriptor) in alternate representation.
	%h	 Represents the abbreviated month name (for example, Jan) defined by the abmon statement. This field descriptor is a synonym for the %b field descriptor.
	%H	 Represents the 24-hour clock hour as a decimal number (00 to 23).
	%I	 Represents the 12-hour clock hour as a decimal number (01 to 12).
	%j	 Represents the day of the year as a decimal number (001 to 366).
	%m	 Represents the month of the year as a decimal number (01 to 12).
	%M	 Represents the minutes of the hour as a decimal number (00 to 59).
	%n	 Specifies a new-line character.
	%N	 Represents the alternate era name.
	%o	 Represents the alternate era year.
	%p	 Represents the a.m. or p.m. string defined by the am_pm statement.
	%r	 Represents the 12-hour clock time with a.m./p.m. notation as defined by the t_fmt_ampm statement.
	%S	 Represents the seconds of the minute as a decimal number (00 to 59).
	%t	 Specifies a tab character.
	%T	 Represents 24-hour clock time in the format %H:%M:%S (for example, 16:55:15).
	%U	 Represents the week of the year as a decimal number (00 to 53). Sunday, or its equivalent as defined by the day statement, is considered the first day of the week for calculating the value of this field descriptor.
	%w	 Represents the day of the week as a decimal number (0 to 6). Sunday, or its equivalent as defined by the day statement, is considered as 0 for calculating the value of this field descriptor.
	%W	 Represents the week of the year as a decimal number (00 to 53). Monday, or its equivalent as defined by the day statement, is considered the first day of the week for calculating the value of this field descriptor.
	%x	 Represents the date format defined by the d_fmt statement.
	%X	 Represents the time format defined by the t_fmt statement.
	%y	 Represents the year of the century (00 to 99).
	Note:
	When the environment variable XPG_TIME_FMT=ON, %y is the year within the century. When a century is not otherwise specified, values in the range 69-99 refer to years in the twentieth century (1969 to 1999, inclusive); values in the range 00-68 refer to 2000 to 2068, inclusive.
	%Y	 Represents the year as a decimal number (for example, 1989).
	%Z	 Represents the time-zone name, if one can be determined (for example, EST); no characters are displayed if a time zone cannot be determined.
	%%	 Specifies a % (percent sign) character.
	}
	DecodeDate(Date, Day, Month, Year);
	while Temp < Length(Format) do begin
		if Format[Temp] = '%' then begin
			case Format[Temp + 1] of
				'a': FormatDate := FormatDate + TimeSettings.ShortDayName[SmallInt(DayOfWeek(Date))];
				'A': FormatDate := FormatDate + TimeSettings.LongDayName[SmallInt(DayOfWeek(Date))];
				'b': FormatDate := FormatDate + TimeSettings.ShortMonthName[Month];
				'B': FormatDate := FormatDate + TimeSettings.LongMonthName[Month];
				'd': FormatDate := FormatDate + IntToStr(Day);
				'D': FormatDate := FormatDate + LeadingZero(Day);
				'j': FormatDate := FormatDate + IntToStr(DayOfYear(Date));
				'J': FormatDate := FormatDate + LeadingZero(DayOfYear(Date), 2);
				'm': FormatDate := FormatDate + IntToStr(Month);
				'M': FormatDate := FormatDate + LeadingZero(Month);
				'n': FormatDate := FormatDate + #10 + #13;
				't': FormatDate := FormatDate + #9;
				'u': FormatDate := FormatDate + IntToStr(SmallInt(DayOfWeek(Date)) + 1);
				'U': ;
				'V': ;
				'w': ;
				'W': ;
				'y': FormatDate := FormatDate + Copy(IntToStr(Year), 3, 2);
				'Y': FormatDate := FormatDate + IntToStr(Year);
				'%': FormatDate := FormatDate + '%';
			end;
			Inc(Temp, 2);
		end else begin
			FormatDate := FormatDate + Format[Temp];
			Inc(Temp);
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
function Easter(Year: SmallInt): TDate;
	var a, b, c, d, e, f, g, h, i, k, l, m: Word;
begin
	if Year >= 0 then begin
		a := Year mod 19;
		b := Year div 100;
		c := Year mod 100;
		d := b div 4;
		e := b mod 4;
		f := (b + 8) div 25;
		g := (b - f + 1) div 3;
		h := (19 * a + b - d - g + 15) mod 30;
		i := c div 4;
		k := c mod 4;
		l := (32 + 2 * e + 2 * i - h - k) mod 7;
		m := (a + 11 * h + 22 * l) div 451;
		EncodeDate(Easter, ((h + l - 7 * m + 114) mod 31) + 1, (h + l - 7 * m + 114) div 31, Year);
	end;
end;
{--------------------------------------------------------------------------------------------------}
function Holiday(Date: TDate): Boolean; //note temporary implementation of swedish holidays...
	var Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	Holiday := (
	(DayofWeek(Date) = Sunday) or		//SÃ¶ndag
	((Day = 1) and (Month = 1)) or		//NyÃ¥rsdagen
	((Day = 6) and (Month = 1)) or		//Trettondagsafton
	((Day = 1) and (Month = 5)) or		//FÃ¶rsta Maj
	((Day = 6) and (Month = 6)) or		//Nationaldagen
	((Day = 25) and (Month = 12)) or	//Juldagen
	((Day = 26) and (Month = 12)) or	//Annandag Jul
	(Date = Easter(Year) - 2) or		//LÃ¥ngfredagen
	(Date = Easter(Year) + 1) or		//Annandag PÃ¥sk
	(Date = Easter(Year) + 39) or 	//Kristi HimmelfÃ¤rdsdag
	((DayofWeek(Date) = Saturday) and (Month = 6) and (Day >= 20) and (Day <= 26)) or
	(Date = Easter(Year) + 49));		//<-Pingstdagen ^Midsommardagen
end;
{--------------------------------------------------------------------------------------------------}
function DateToUnix(Date: TDate): Int64;
begin
	DateToUnix := Date * 86400 - 2440588;
end;
{--------------------------------------------------------------------------------------------------}
function UnixToDate(Unix: Int64): TDate;
begin
	UnixToDate := Unix div 86400 + 2440588;
end;
{--------------------------------------------------------------------------------------------------}
function DaysBetween(One, Two: TDate): LongInt;
begin
	DaysBetween := One - Two;
end;
{--------------------------------------------------------------------------------------------------}
function CompareDate(One, Two: TDate): ValueRelationship; //todo hitta pÃ¥ nÃ¥got elegantare...
begin
	if One < Two then CompareDate := -1 else if One > Two then CompareDate := 1 else CompareDate := 0;
end;
{--------------------------------------------------------------------------------------------------}
function SameDate(One, Two: TDate): Boolean;
begin
	SameDate := One = Two;
end;
{--------------------------------------------------------------------------------------------------}
function IsWorkDay(Day: TDays): Boolean;
begin
	IsWorkDay := Day in [Monday..Friday];
end;
{--------------------------------------------------------------------------------------------------}
function IsWeekEnd(Day: TDays): Boolean;
begin
	IsWeekEnd := Day in [Saturday..Sunday];
end;
{--------------------------------------------------------------------------------------------------}
procedure BumpDate(var Date: TDate; NumberOfDays, NumberOfMonths, NumberOfYears: SmallInt);
	var Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	Month := Month + NumberOfMonths - 1;
	Year := Year + NumberOfYears + (Month div 12) - Ord(Month < 0);
	Month := (Month + 12000) mod 12 + 1;
	EncodeDate(Date, Day, Month, Year);
	Date := Date + NumberOfDays;
end;
{--------------------------------------------------------------------------------------------------}
procedure DecDay(var Date: TDate; NumberOfDays: Word); overload;
begin
	BumpDate(Date, -NumberOfDays, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecDay(var Date: TDate); overload;
begin
	BumpDate(Date, -1, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecMonth(var Date: TDate; NumberOfMonths: Word); overload;
begin
	BumpDate(Date, 0, -NumberOfMonths, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecMonth(var Date: TDate); overload;
begin
	BumpDate(Date, 0, -1, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecWeek(var Date: TDate; NumberOfWeeks: Word); overload;
begin
	BumpDate(Date, -NumberOfWeeks * Week, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecWeek(var Date: TDate); overload;
begin
	BumpDate(Date, -Week, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecYear(var Date: TDate; NumberOfYears: Word); overload;
begin
	BumpDate(Date, 0, 0, -NumberOfYears);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecYear(var Date: TDate); overload;
begin
	BumpDate(Date, 0, 0, -1);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncDay(var Date: TDate; NumberOfDays: Word); overload;
begin
	BumpDate(Date, NumberOfDays, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncDay(var Date: TDate); overload;
begin
	BumpDate(Date, 1, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncMonth(var Date: TDate; NumberOfMonths: Word); overload;
begin
	BumpDate(Date, 0, NumberOfMonths, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncMonth(var Date: TDate); overload;
begin
	BumpDate(Date, 0, 1, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncWeek(var Date: TDate; NumberOfWeeks: Word); overload;
begin
	BumpDate(Date, NumberOfWeeks * Week, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncWeek(var Date: TDate); overload;
begin
	BumpDate(Date, Week, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncYear(var Date: TDate; NumberOfYears: Word); overload;
begin
	BumpDate(Date, 0, 0, NumberOfYears);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncYear(var Date: TDate); overload;
begin
	BumpDate(Date, 0, 0, 1);
end;
{--------------------------------------------------------------------------------------------------}
function Age(Birth, Reference: TDate): SmallInt; overload;
	var BirthDay, BirthMonth, BirthYear, Day, Month, Year: SmallInt;
begin
	DecodeDate(Birth, BirthDay, BirthMonth, BirthYear);
	DecodeDate(Reference, Day, Month, Year);
	Age := Reference - Birth;
	if (Month < BirthMonth) or ((Month = BirthMonth) and (Day < BirthDay)) then
		Dec(Age);
end;
{--------------------------------------------------------------------------------------------------}
function WithinDays(One, Two: TDate; NumberOfDays: SmallInt): Boolean;
begin
	if NumberOfDays < 0 then
		WithinDays := DaysBetween(One, Two) >= NumberOfDays
	else
		WithinDays := DaysBetween(One, Two) <= NumberOfDays;
end;
{--------------------------------------------------------------------------------------------------}
procedure FirstDayOfMonth(var Date: TDate);
var
	Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	EncodeDate(Date, 1, Month, Year);
end;
