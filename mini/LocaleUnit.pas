{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... August 4, 2011                                                                       }
{ Website....                                                                                      }
{                                                                                                  }
{ Description This library loads locale data from the operating system into separate records for   }
{             use in different programmes.                                                         }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
unit LocaleUnit;

interface
{--------------------------------------------------------------------------------------------------}
uses
	StringUnit;
{--------------------------------------------------------------------------------------------------}
type
	TWeekNameArray = array [0..6] of String;
	TMonthNameArray = array [1..12] of String;

	TNumericFormatSettings = record
		DecimalPoint, ThousandSep: Char;
		Grouping: String;
	end;

	TTimeFormatSettings = record
		ShortDayName, LongDayName: TWeekNameArray;
		ShortMonthName, LongMonthName: TMonthNameArray;
		AMAndPM: String;
		DateTimeFormat: String;	 	//Standard date and time format corresponding to the %c field descriptor. Can contain any combination of characters and field descriptors.
		DateFormat: String;			//Standard date format corresponding to the %x field descriptor. Can contain any combination of characters and field descriptors.
		TimeFormat: String;			//Standard time format corresponding to the %X field descriptor. Can contain any combination of characters and field descriptors.
		TimeFormatAMPM: String;		//Standard 12-hour time format that includes an am_pm value (the %p field descriptor). This statement corresponds to the %r field descriptor. The string can contain any combination of characters and field descriptors.
		Era: String;
		EraYear: String;
		EraDateTimeFormat: String;
		EraDateFormat: String;
		EraTimeFormat: String;
		AlternateDigits: String;
		//TimeEraNumEntries: Byte;
		//TimeEraEntries: Char;
		WeekNumberOfDays: Byte;
		//WeekFirstDay:
		//WeekFirstWeek:
		FirstWeekday: Byte;
		FirstWorkday: Byte;
		CalendarDir: Byte;
		//TimeZone: String;
		DateZoneFormat: String;
	end;

	TMonetaryFormatSettings = record
		IntCurrSymbol: String;
		CurrencySymbol: String;
		MonDecimalPoint: Char;
		MonThousandSep: Char;
		MonGrouping: String;
		PositiveSign: Char;
		NegativeSign: Char;
		IntFracDigits: Byte;
		FracDigits: Byte;
		PosCurrSymPrec: Boolean;	//Specifies an integer value indicating whether the int_curr_symbol or currency_symbol String precedes or follows the value for a nonnegative formatted monetary quantity:
									//	0		Currency symbol follows the monetary quantity.
									//	1		Currency symbol precedes the monetary quantity.
		NegCurrSymPrec: Boolean;

		PosSepBySpace: Byte;		//Specifies an integer value indicating whether the int_curr_symbol or currency_symbol String is separated by a space from a nonnegative formatted monetary quantity:
									//	0		No space separates the currency symbol from the monetary quantity.
									//	1		A space separates the currency symbol from the monetary quantity.
									//	2		A space separates the currency symbol and the positive_sign string, if adjacent.
		NegSepBySpace: Byte;

		PosSignPosition: Byte;		//Specifies an integer value indicating the positioning of the positive_sign string for a nonnegative formatted monetary quantity:

									//	0		A left_parenthesis and right_parenthesis symbol enclose both the monetary quantity and the int_curr_symbol or currency_symbol string.
									//	1		The positive_sign string precedes the quantity and the int_curr_symbol or currency_symbol string.
									//	2		The positive_sign string follows the quantity and the int_curr_symbol or currency_symbol string.
									//	3		The positive_sign string immediately precedes the int_curr_symbol or currency_symbol string.
									//	4		The positive_sign string immediately follows the int_curr_symbol or currency_symbol string.
		NegSignPosition: Byte;
	end;

	TMessagesFromatSettings = record
		YesExpr: String;
		NoExpr: String;
		YesStr: String;
		NoStr: String;
	end;

	TTelephoneFormatSettings = record
		TelIntFormat: String;
		TelDOMFormat: String;
		IntSelect: String;
		IntPrefix: String;
	end;

{	todo implement support for Windows...
	FormatSettings = record
		CurrencyFormat: Byte;
		NegCurrFormat: Byte;
		CurrencySymbol: String;
		CurrencyDecimals: Byte;
		ThousandSeparator: Char;
		DecimalSeparator: Char;
		DateSeparator: Char;
		TimeSeparator: Char;
		ListSeparator: Char;
		ShortDateFormat: String;
		LongDateFormat: String;
		TimeAMString: String;
		TimePMString: String;
		ShortTimeFormat: String;
		LongTimeFormat: String;
		ShortMonthNames: MonthNameArrayType;
		LongMonthNames: MonthNameArrayType;
		ShortDayNames: WeekNameArrayType;
		LongDayNames: WeekNameArrayType;
		TwoDigitYearCenturyWindow: word;
	end;}
{--------------------------------------------------------------------------------------------------}
var
	NumericSettings: TNumericFormatSettings;
	TimeSettings: TTimeFormatSettings;
	MonetarySettings: TMonetaryFormatSettings;
	MessagesSettings: TMessagesFromatSettings;
	TelephoneSettings: TTelephoneFormatSettings;
{--------------------------------------------------------------------------------------------------}
implementation
{--------------------------------------------------------------------------------------------------}
function StrToArray(const Str: String): StringArray;
begin
	StrToArray := StrSplit(Trim(Str, ['"']), [';']);
end;
{--------------------------------------------------------------------------------------------------}
initialization
	//todo Load the data into the records...
	
	NumericSettings.DecimalPoint := ',';
	NumericSettings.ThousandSep := ' ';
	NumericSettings.Grouping := '';

	TimeSettings.ShortDayName[0] := 'Mon';
	TimeSettings.ShortDayName[1] := 'Tue';
	TimeSettings.ShortDayName[2] := 'Wed';
	TimeSettings.ShortDayName[3] := 'Thu';
	TimeSettings.ShortDayName[4] := 'Fri';
	TimeSettings.ShortDayName[5] := 'Sat';
	TimeSettings.ShortDayName[6] := 'Sun';
	
	TimeSettings.LongDayName[0] := 'Monday';
	TimeSettings.LongDayName[1] := 'Tuesday';
	TimeSettings.LongDayName[2] := 'Wednesday';
	TimeSettings.LongDayName[3] := 'Thursday';
	TimeSettings.LongDayName[4] := 'Friday';
	TimeSettings.LongDayName[5] := 'Saturday';
	TimeSettings.LongDayName[6] := 'Sunday';
	
	TimeSettings.ShortMonthName[1] := 'Jan';
	TimeSettings.ShortMonthName[2] := 'Feb';
	TimeSettings.ShortMonthName[3] := 'Mar';
	TimeSettings.ShortMonthName[4] := 'Apr';
	TimeSettings.ShortMonthName[5] := 'May';
	TimeSettings.ShortMonthName[6] := 'Jun';
	TimeSettings.ShortMonthName[7] := 'Jul';
	TimeSettings.ShortMonthName[8] := 'Aug';
	TimeSettings.ShortMonthName[9] := 'Sep';
	TimeSettings.ShortMonthName[10] := 'Oct';
	TimeSettings.ShortMonthName[11] := 'Nov';
	TimeSettings.ShortMonthName[12] := 'Dec';
	
	TimeSettings.LongMonthName[1] := 'January';
	TimeSettings.LongMonthName[2] := 'February';
	TimeSettings.LongMonthName[3] := 'March';
	TimeSettings.LongMonthName[4] := 'April';
	TimeSettings.LongMonthName[5] := 'May';
	TimeSettings.LongMonthName[6] := 'June';
	TimeSettings.LongMonthName[7] := 'July';
	TimeSettings.LongMonthName[8] := 'August';
	TimeSettings.LongMonthName[9] := 'September';
	TimeSettings.LongMonthName[10] := 'October';
	TimeSettings.LongMonthName[11] := 'November';
	TimeSettings.LongMonthName[12] := 'December';
	
	TimeSettings.AMAndPM := 'fm em';
// 	TimeSettings.DateTimeFormat: String;
// 	TimeSettings.DateFormat: String;
// 	TimeSettings.TimeFormat: String;
// 	TimeSettings.TimeFormatAMPM: String;
// 	TimeSettings.Era: String;
// 	TimeSettings.EraYear: String;
// 	TimeSettings.EraDateTimeFormat: String;
// 	TimeSettings.EraDateFormat: String;
// 	TimeSettings.EraTimeFormat: String;
// 	TimeSettings.AlternateDigits: String;
// 	TimeSettings.WeekNumberOfDays := 7;
// 	TimeSettings.FirstWeekday: Byte;
// 	TimeSettings.FirstWorkday: Byte;
// 	TimeSettings.CalendarDir: Byte;
	TimeSettings.DateZoneFormat := '';
	
finalization
	
end.