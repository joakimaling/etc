const
	DaysPerYear: Double = 365.2425;
type
	ValueRelationship = -1..1;
procedure EncodeDate(var Date: TDate; Day, Month, Year: SmallInt);
var
	a, y, m: Word;
begin
	a := (14 - Month) div 12;
	y := Year + 4800 - a;
	m := Month + 12 * a - 3;
	Date := Day + (153 * m + 2) div 5 + 365 * y + y div 4 - y div 100 + y div 400 - 32045;
end;
{--------------------------------------------------------------------------------------------------}
procedure DecodeDate(Date: TDate; var Day, Month, Year: SmallInt);
var
	j, g, dg, c, dc, b, db, a, da, y, m, d: LongInt;
begin
	//j := (Date + 0.5) + 32044; todo lÃ¤gg till dÃ¥ tid implementeras...
	j := Date + 32044; //todo tag bort
	g := j div 146097;
	dg := j mod 146097;
	c := (dg div 36524 + 1) * 3 div 4;
	dc := dg - c * 36524;
	b := dc div 1461;
	db := dc mod 1461;
	a := (db div 365 + 1) * 3 div 4;
	da := db - a * 365;
	y := g * 400 + c * 100 + b * 4 + a;
	m := (da * 5 + 308) div 153 - 2;
	d := da - (m + 4) * 153 div 5 + 122;
	Year := y - 4800 + (m + 2) div 12;
	Month := (m + 2) mod 12 + 1;
	Day := d + 1;
end;
{--------------------------------------------------------------------------------------------------}
function ValidDate(Day, Month, Year: SmallInt): Boolean;
begin
	if (Day < 1) or (Year < -4800) or ((Year = -4800) and (Month < 3)) then
		ValidDate := false
	else
		case Month of
			1, 3..12: ValidDate := Day <= 30 + (Month + Month div 8) mod 2;
			2: ValidDate := Day <= 28 + Ord(LeapYear(Year));
			else ValidDate := false;
		end;
end;
{--------------------------------------------------------------------------------------------------}
function WeekNumber(Date: TDate): SmallInt;
var
	Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	WeekNumber := ((OrdinalDate[Month, LeapYear(Year)] + Day) - SmallInt(DayOfWeek(Date)) + 11) div 7;
end;
{--------------------------------------------------------------------------------------------------}
function WeekOfMonth(Date: TDate): SmallInt;
var
	Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	WeekOfMonth := (Day - 1) div 7 + 1;
end;
{--------------------------------------------------------------------------------------------------}
function DayOfWeek(Date: TDate): TDays;
begin
	DayOfWeek := TDays(Date mod 7);
end;
{--------------------------------------------------------------------------------------------------}
function DayOfYear(Date: TDate): SmallInt;
var
	Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	DayOfYear := OrdinalDate[Month, LeapYear(Year)] + Day;
end;
{--------------------------------------------------------------------------------------------------}
function FirstWeekDay: TDays;
begin
	FirstWeekDay := TDays(TimeSettings.FirstWeekDay);
end;
{--------------------------------------------------------------------------------------------------}
function FormatDate(const Format: String; Date: TDate): String;
var
	Day, Month, Year: SmallInt;
	Temp: byte = 1;
begin
	FormatDate := '';
	{
	%a - Represents the abbreviated weekday name
	%A - Represents the full weekday name
	%b - Represents the abbreviated month name
	%B - Represents the full month name
	%c - Represents the century as a decimal number (0 to 99)
	%C - Represents the century as a decimal number (00 to 99)
	%d - Same as %e
	%D - Represents the day of the month as a decimal number (01 to 31)
	%e - Represents the day of the month as a decimal number (1 to 31)
	%g - Represents a 2-digit year corresponding to the %V week number. Normally only usefull with %V
	%G - Represents a 4-digit year corresponding to the %V week number. Normally only usefull with %V
	%h - Same as %b
	%j - Represents the day of the year as a decimal number (001 to 366)
	%J - Represents the day of the year as a decimal number (1 to 366)
	%m - Represents the month of the year as a decimal number (1 to 12)
	%M - Represents the month of the year as a decimal number (01 to 12)
	%n - A <newline> character
	%N - Represents the era name
	%O - Represents the non-astronomical year (e.g. 0 -> 1, -1 -> 2). Normally only usefull with %N
	%t - A <tab> character
	%u - Represents the day of week as a decimal number (1 to 7) (where 1 = Monday)
	%U - Represents the week number of year, with Sunday as first day of week (00 to 53)
	%V - Represents the week of year, with Monday as the first day of week (01 to 53). If the week containing January 1 has four or more days in the new year, then it shall be considered week 1; otherwise, it shall be the last week of the previous year, and the next week shall be week 1
	%w - Represents the day of week as a decimal number (0 to 6) (where 0 = Sunday)
	%W - Represents the week number of year, with Monday as first day of week (00 to 53)
	%x - Represents the date format defined by the d_fmt statement
	%y - Represents the year within century (00 to 99)
	%Y - Represents the full year as a decimal number
	%% - A <percent-sign> character
	}
	DecodeDate(Date, Day, Month, Year);
	while Temp < Length(Format) do begin
		if Format[Temp] = '%' then begin
			case Format[Temp + 1] of
				'a': FormatDate := FormatDate + TimeSettings.ShortDayName[SmallInt(DayOfWeek(Date))];
				'A': FormatDate := FormatDate + TimeSettings.LongDayName[SmallInt(DayOfWeek(Date))];
				'b', 'h': FormatDate := FormatDate + TimeSettings.ShortMonthName[Month];
				'B': FormatDate := FormatDate + TimeSettings.LongMonthName[Month];
				'c': FormatDate := FormatDate + IntToStr(Year div 100);
				'C': FormatDate := FormatDate + PadLeft(IntToStr(Year div 100), 2, '0');
				'D': FormatDate := FormatDate + PadLeft(IntToStr(Day), 2, '0');
				'e', 'd': FormatDate := FormatDate + IntToStr(Day);
				'g': ;
				'G': ;
				'j': FormatDate := FormatDate + PadLeft(IntToStr(DayOfYear(Date)), 3, '0');
				'J': FormatDate := FormatDate + IntToStr(DayOfYear(Date));
				'm': FormatDate := FormatDate + IntToStr(Month);
				'M': FormatDate := FormatDate + PadLeft(IntToStr(Month), 2, '0');
				'n': FormatDate := FormatDate + #10 + #13;
				'N': ;
				'O': ;
				't': FormatDate := FormatDate + #9;
				'u': FormatDate := FormatDate + IntToStr(SmallInt(DayOfWeek(Date)) + 1);
				'U': ;
				'V': ;
				'w': ;
				'W': ;
				'x': ;
				'y': FormatDate := FormatDate + Copy(IntToStr(Year), 3, 2);
				'Y': FormatDate := FormatDate + IntToStr(Year);
				'%': FormatDate := FormatDate + '%'
				else FormatDate := FormatDate + Format[Temp];
			end;
			Inc(Temp, 2);
		end else begin
			FormatDate := FormatDate + Format[Temp];
			Inc(Temp);
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
function Easter(Year: SmallInt): TDate;
var
	a, b, c, d, e, f, g, h, i, k, l, m: Word;
begin
	if Year >= 0 then begin
		a := Year mod 19;
		b := Year div 100;
		c := Year mod 100;
		d := b div 4;
		e := b mod 4;
		f := (b + 8) div 25;
		g := (b - f + 1) div 3;
		h := (19 * a + b - d - g + 15) mod 30;
		i := c div 4;
		k := c mod 4;
		l := (32 + 2 * e + 2 * i - h - k) mod 7;
		m := (a + 11 * h + 22 * l) div 451;
		EncodeDate(Easter, ((h + l - 7 * m + 114) mod 31) + 1, (h + l - 7 * m + 114) div 31, Year);
	end;
end;
{--------------------------------------------------------------------------------------------------}
function Holiday(Date: TDate): Boolean; //note temporary implementation of swedish holidays...
var
	Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	Holiday := (
	(DayofWeek(Date) = Sunday) or		//Söndag
	((Day = 1) and (Month = 1)) or		//Nyårsdagen
	((Day = 6) and (Month = 1)) or		//Trettondagsafton
	((Day = 1) and (Month = 5)) or		//Första Maj
	((Day = 6) and (Month = 6)) or		//Nationaldagen
	((Day = 25) and (Month = 12)) or	//Juldagen
	((Day = 26) and (Month = 12)) or	//Annandag Jul
	(Date = Easter(Year) - 2) or		//Långfredagen
	(Date = Easter(Year) + 1) or		//Annandag Påsk
	(Date = Easter(Year) + 39) or 	//Kristi Himmelfärdsdag
	((DayofWeek(Date) = Saturday) and (Month = 6) and (Day >= 20) and (Day <= 26)) or
	(Date = Easter(Year) + 49));		//<-Pingstdagen ^Midsommardagen
end;
{--------------------------------------------------------------------------------------------------}
function DateToUnix(Date: TDate): Int64;
begin
	DateToUnix := Date * 86400 - 2440588;
end;
{--------------------------------------------------------------------------------------------------}
function UnixToDate(Unix: Int64): TDate;
begin
	UnixToDate := Unix div 86400 + 2440588;
end;
{--------------------------------------------------------------------------------------------------}
function DateToDos(Date: TDate): LongInt;
var
	Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	DateToDos := ((Year - 1980) shl 25) and (Month shl 21) and (Day shl 16);
end;
{--------------------------------------------------------------------------------------------------}
function DaysBetween(One, Two: TDate): LongInt;
begin
	DaysBetween := One - Two;
end;
{--------------------------------------------------------------------------------------------------}
function CompareDate(One, Two: TDate): ValueRelationship;
begin
	if One < Two then CompareDate := -1 else CompareDate := Ord(One > Two);
end;
{--------------------------------------------------------------------------------------------------}
function SameDate(One, Two: TDate): Boolean;
begin
	SameDate := One = Two;
end;
{--------------------------------------------------------------------------------------------------}
function IsWorkDay(Day: TDays): Boolean;
begin
	IsWorkDay := Day in [Monday..Friday];
end;
{--------------------------------------------------------------------------------------------------}
function IsWeekEnd(Day: TDays): Boolean;
begin
	IsWeekEnd := Day in [Saturday..Sunday];
end;
{--------------------------------------------------------------------------------------------------}
procedure BumpDate(var Date: TDate; NumberOfDays, NumberOfMonths, NumberOfYears: SmallInt);
var
	Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	Month := Month + NumberOfMonths - 1;
	Year := Year + NumberOfYears + (Month div 12) - Ord(Month < 0);
	Month := (Month + 12000) mod 12 + 1;
	EncodeDate(Date, Day, Month, Year);
	Inc(Date, NumberOfDays);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecDay(var Date: TDate; NumberOfDays: Word); overload;
begin
	BumpDate(Date, -NumberOfDays, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecDay(var Date: TDate); overload;
begin
	BumpDate(Date, -1, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecMonth(var Date: TDate; NumberOfMonths: Word); overload;
begin
	BumpDate(Date, 0, -NumberOfMonths, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecMonth(var Date: TDate); overload;
begin
	BumpDate(Date, 0, -1, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecWeek(var Date: TDate; NumberOfWeeks: Word); overload;
begin
	BumpDate(Date, -NumberOfWeeks * Week, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecWeek(var Date: TDate); overload;
begin
	BumpDate(Date, -Week, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecYear(var Date: TDate; NumberOfYears: Word); overload;
begin
	BumpDate(Date, 0, 0, -NumberOfYears);
end;
{--------------------------------------------------------------------------------------------------}
procedure DecYear(var Date: TDate); overload;
begin
	BumpDate(Date, 0, 0, -1);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncDay(var Date: TDate; NumberOfDays: Word); overload;
begin
	BumpDate(Date, NumberOfDays, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncDay(var Date: TDate); overload;
begin
	BumpDate(Date, 1, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncMonth(var Date: TDate; NumberOfMonths: Word); overload;
begin
	BumpDate(Date, 0, NumberOfMonths, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncMonth(var Date: TDate); overload;
begin
	BumpDate(Date, 0, 1, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncWeek(var Date: TDate; NumberOfWeeks: Word); overload;
begin
	BumpDate(Date, NumberOfWeeks * Week, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncWeek(var Date: TDate); overload;
begin
	BumpDate(Date, Week, 0, 0);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncYear(var Date: TDate; NumberOfYears: Word); overload;
begin
	BumpDate(Date, 0, 0, NumberOfYears);
end;
{--------------------------------------------------------------------------------------------------}
procedure IncYear(var Date: TDate); overload;
begin
	BumpDate(Date, 0, 0, 1);
end;
{--------------------------------------------------------------------------------------------------}
function Today: TDate;
var
	Year, Month, Day, Temp: Word;
begin
	GetDate(Year, Month, Day, Temp); //todo byt ut till egen kalkylering, systemoberoende...
	EncodeDate(Today, Day, Month, Year);
end;
{--------------------------------------------------------------------------------------------------}
function Age(Birth, Reference: TDate): SmallInt; overload;
var
	BirthDay, BirthMonth, BirthYear, Day, Month, Year: SmallInt;
begin
	DecodeDate(Birth, BirthDay, BirthMonth, BirthYear);
	DecodeDate(Reference, Day, Month, Year);
	Age := Reference - Birth;
	if (Month < BirthMonth) or ((Month = BirthMonth) and (Day < BirthDay)) then
		Dec(Age);
end;
{--------------------------------------------------------------------------------------------------}
function Age(Birth: TDate): SmallInt; overload;
begin
	Age := Age(Birth, Today);
end;
{--------------------------------------------------------------------------------------------------}
function WithinDays(One, Two: TDate; NumberOfDays: SmallInt): Boolean;
begin
	if NumberOfDays < 0 then
		WithinDays := DaysBetween(One, Two) >= NumberOfDays
	else
		WithinDays := DaysBetween(One, Two) <= NumberOfDays;
end;
{--------------------------------------------------------------------------------------------------}
procedure FirstDayOfMonth(var Date: TDate);
var
	Day, Month, Year: SmallInt;
begin
	DecodeDate(Date, Day, Month, Year);
	EncodeDate(Date, 1, Month, Year);
end;
