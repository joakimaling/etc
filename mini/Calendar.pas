program Calendar;

uses
	ConsoleBuffer,
	ConsoleFrame,
	ConsolePalette,
	ConsoleTextArea,
	DateUtils,
	Keyboard,
	StrUtils,
	SysUtils;

const
	HolidayPalette = $0000;
	OtherPalette = $0000;
	TodayPalette = $0000;

var
	First, Second, Third: PFrame;
	A, B, C: PTextArea;

{ Draws a small calendar, using given frame with a text area, for given date. }
procedure DrawMonth(Frame: PFrame; Moment: TDateTime);
var
	Current, Last: TDateTime;
	Palette: Word;
	Row: Word = 2;
begin
	FrameRename(Frame, FormatDateTime('MMMM YYYY', Moment));
	TextAreaWrite(PTextArea(Frame^.Components[0]), 'Mo Tu We Th Fr Sa Su');
	{
	Current := StartOfTheWeek(StartOfTheMonth(Moment));
	Last := EndOfTheWeek(IncMonth(Current));

	while not IsSameDay(Current, IncDay(Last)) do begin
		Palette := Frame^.Palette^.Enabled;
		if IsToday(Current) then Palette := $3fe7;
		if DayOfTheWeek(Current) = DaySunday then PaletteSetForeground(Palette, $a0);
		if not IsSameMonth(Current, Moment) then PaletteSetForeground(Palette, $f0);;

		TextAreaWrite(
			Frame^.Components[0],
			AddChar(' ', IntToStr(DayOf(Current)), 2)
		);

		if DayOfTheWeek(Current) = 7 then Inc(Row) else
			TextAreaWrite(Frame^.Components[0], ' ');
		Current := IncDay(Current);
	end;
	TextAreaDraw(Frame^.Components[0]);
	TextAreaErase(Frame^.Components[0]);}
end;

begin
	InitKeyboard;
	First := FrameCreate('', 1, 1, 22, 8);
	Second := FrameCreate('', 24, 1, 22, 8);
	Third := FrameCreate('', 47, 1, 22, 8);

	A := TextAreaCreate(0, 0, 20, 6);
	B := TextAreaCreate(0, 0, 20, 6);
	C := TextAreaCreate(0, 0, 20, 6);

	FrameAttach(First, Pointer(A));
	FrameAttach(Second, Pointer(B));
	FrameAttach(Third, Pointer(C));

	repeat
		DrawMonth(First, Today);
		DrawMonth(Second, IncMonth(Today));
		DrawMonth(Third, IncMonth(Today, 2));
		BufferDraw;
	until KeyPressed;

	TextAreaDestroy(A);
	TextAreaDestroy(B);
	TextAreaDestroy(C);

	FrameDestroy(First);
	FrameDestroy(Second);
	FrameDestroy(Third);
	DoneKeyboard;
end.
