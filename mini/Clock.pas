program Clock;

uses
	ConsoleBuffer,
	Keyboard,
	SysUtils;

const
	Font: Array[0..10, 0..6] of Byte = (
		($1e, $33, $33, $33, $33, $33, $1e),
		($0c, $1c, $0c, $0c, $0c, $0c, $3f),
		($1e, $33, $03, $0e, $18, $31, $3f),
		($1e, $33, $03, $0e, $03, $33, $1e),
		($06, $0e, $16, $26, $3f, $06, $0f),
		($3f, $30, $3e, $03, $03, $33, $1e),
		($0e, $18, $30, $3e, $33, $33, $1e),
		($3f, $33, $03, $06, $0c, $0c, $0c),
		($1e, $33, $33, $1e, $33, $33, $1e),
		($1e, $33, $33, $1f, $03, $06, $1c),
		($00, $0c, $0c, $00, $0c, $0c, $00)
	);
	Hue = $10ff;

var
	Bit, X, Y: Byte;
	Chunk, Hour, Minute, Second: Word;
	Tick: Boolean = false;

{ Creates a binary sequence represented as a string of Xs and spaces from given
  hexadecimal number. }
function Dot(Hexadecimal: Byte): String;
var
	Binary: Byte = 64;
begin
	Dot := '';
	repeat
		if Hexadecimal >= Binary then begin
			Dec(Hexadecimal, Binary);
			Dot := Dot + '#';
		end else begin
			Dot := Dot + #32;
		end;
		Binary := Binary div 2;
	until Hexadecimal = 0;
end;

begin
	BufferStart;
	InitKeyboard;
	X := (BufferGetWidth - 64) div 2;
	Y := (BufferGetHeight - 7) div 2;
	repeat
		DecodeTime(Time, Hour, Minute, Second, Chunk);
		for Bit := 0 to 6 do begin
			Chunk := Y + Bit;
			BufferDrawText(X + 0, Chunk, Dot(Font[Hour div 10, Bit]), Hue);
			BufferDrawText(X + 8, Chunk, Dot(Font[Hour mod 10, Bit]), Hue);
			if Tick then BufferDrawText(X + 16, Chunk, Dot(Font[10, Bit]), Hue);
			BufferDrawText(X + 24, Chunk, Dot(Font[Minute div 10, Bit]), Hue);
			BufferDrawText(X + 32, Chunk, Dot(Font[Minute mod 10, Bit]), Hue);
			if Tick then BufferDrawText(X + 40, Chunk, Dot(Font[10, Bit]), Hue);
			BufferDrawText(X + 48, Chunk, Dot(Font[Second div 10, Bit]), Hue);
			BufferDrawText(X + 56, Chunk, Dot(Font[Second mod 10, Bit]), Hue);
		end;
		BufferDrawTextCentre(Y + 10, 'Press any key to quit', Hue);
		Tick := not Tick;
		BufferDraw;
		Sleep(500);
	until KeyPressed;
	DoneKeyboard;
	BufferStop;
end.
