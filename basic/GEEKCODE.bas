
    'GEEK CODE
    'Start: 2011-02-24


    nomainwin

    WindowWidth = 616
    WindowHeight = 510

    statictext #main.header, "Typers of GEEKs", 10, 10, 120, 20
    statictext #main.description, "Geeks come in many flavors. The flavors relate to the vocation (or, if a student, what they are training in) of the particular geek. To start a code, a geek must declare himself or herself to be a geek. To do this, we start the code with a G to denote ", 10, 35, 2016, 20
    checkbox #main.gb, "Geek of Business", [Set_GB], [Reset_GB], 10, 95, 144, 20
    checkbox #main.gc, "Geek of Classics", [Set_GC], [Reset_GC], 10, 120, 144, 20
    checkbox #main.gca, "Geek of Commercial Arts", [Set_GCA], [Reset_GCA], 10, 145, 200, 20
    checkbox #main.gcm, "Geek of Computer Management", [Set_GCM], [Reset_GCM], 10, 170, 232, 20
    checkbox #main.gcs, "Geek of Computer Science", [Set_GCS], [Reset_GCS], 10, 195, 208, 20
    checkbox #main.gcc, "Geek of Communications", [Set_GCC], [Reset_GCC], 10, 220, 192, 20
    checkbox #main.ge, "Geek of Engineering", [Set_GE], [Reset_GE], 10, 245, 168, 20
    checkbox #main.ged, "Geek of Education", [Set_GED], [Reset_GED], 10, 270, 152, 20
    checkbox #main.gfa, "Geek of Fine Arts", [Set_GFA], [Reset_GFA], 10, 295, 152, 20
    checkbox #main.gg, "Geek of Government", [Set_GG], [Reset_GG], 10, 320, 160, 20
    checkbox #main.gh, "Geek of Humanities", [Set_GH], [Reset_GH], 10, 345, 160, 20
    checkbox #main.git, "Geek of Information Technology", [Set_GIT], [Reset_GIT], 10, 370, 256, 20
    checkbox #main.gj, "Geek of Jurisprudence (Law)", [Set_GJ], [Reset_GJ], 10, 395, 232, 20
    checkbox #main.gls, "Geek of Library Science", [Set_GLS], [Reset_GLS], 10, 420, 200, 20
    checkbox #main.gl, "Geek of Literature", [Set_GL], [Reset_GL], 250, 95, 160, 20
    checkbox #main.gmc, "Geek of Mass Communications", [Set_GMC], [Reset_GMC], 250, 120, 232, 20
    checkbox #main.gm, "Geek of Math", [Set_GM], [Reset_GM], 250, 145, 112, 20
    checkbox #main.gmd, "Geek of Medicine", [Set_GMD], [Reset_GMD], 250, 170, 144, 20
    checkbox #main.gmu, "Geek of Music", [Set_GMU], [Reset_GMU], 250, 195, 120, 20
    checkbox #main.gpa, "Geek of Performing Arts", [Set_GPA], [Reset_GPA], 250, 220, 200, 20
    checkbox #main.gp, "Geek of Philosophy", [Set_GP], [Reset_GP], 250, 245, 160, 20
    checkbox #main.gs, "Geek of Science (Physics, Chemistry, Biology etc)", [Set_GS], [Reset_GS], 250, 270, 408, 20
    checkbox #main.gss, "Geek of Social Science (Psychology, Sociology etc)", [Set_GSS], [Reset_GSS], 250, 295, 416, 20
    checkbox #main.gtw, "Geek of Technical Writing", [Set_GTW], [Reset_GTW], 250, 320, 216, 20
    checkbox #main.go, "Geek of Other (your field's not found here)", [Set_GO], [Reset_GO], 250, 345, 360, 20
    checkbox #main.gu, "Geek of 'Undecided'", [Set_GU], [Reset_GU], 250, 370, 168, 20
    checkbox #main.gn, "Geek of no qualifications. A rather miserable existence", [Set_GN], [Reset_GN], 250, 395, 456, 20
    checkbox #main.gat, "Geek of All Trades. GATs can do everything", [Set_GAT], [Reset_GAT], 250, 420, 352, 20
    open "The GEEK CODE" for window_nf as #main
    print #main.description, "!font ms_sans_serif 20"
    print #main, "font ms_sans_serif 0 16"


[main.inputLoop]   'wait here for input event
    wait



[Set_GB]   'Perform action for the checkbox named 'gb'

    'Insert your own code here

    wait


[Reset_GB]   'Perform action for the checkbox named 'gb'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GC]   'Perform action for the checkbox named 'gc'

    'Insert your own code here

    wait


[Reset_GC]   'Perform action for the checkbox named 'gc'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GCA]   'Perform action for the checkbox named 'gca'

    'Insert your own code here

    wait


[Reset_GCA]   'Perform action for the checkbox named 'gca'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GCM]   'Perform action for the checkbox named 'gcm'

    'Insert your own code here

    wait


[Reset_GCM]   'Perform action for the checkbox named 'gcm'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GCS]   'Perform action for the checkbox named 'gcs'

    'Insert your own code here

    wait


[Reset_GCS]   'Perform action for the checkbox named 'gcs'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GCC]   'Perform action for the checkbox named 'gcc'

    'Insert your own code here

    wait


[Reset_GCC]   'Perform action for the checkbox named 'gcc'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GE]   'Perform action for the checkbox named 'ge'

    'Insert your own code here

    wait


[Reset_GE]   'Perform action for the checkbox named 'ge'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GED]   'Perform action for the checkbox named 'ged'

    'Insert your own code here

    wait


[Reset_GED]   'Perform action for the checkbox named 'ged'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GFA]   'Perform action for the checkbox named 'gfa'

    'Insert your own code here

    wait


[Reset_GFA]   'Perform action for the checkbox named 'gfa'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GG]   'Perform action for the checkbox named 'gg'

    'Insert your own code here

    wait


[Reset_GG]   'Perform action for the checkbox named 'gg'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GH]   'Perform action for the checkbox named 'gh'

    'Insert your own code here

    wait


[Reset_GH]   'Perform action for the checkbox named 'gh'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GIT]   'Perform action for the checkbox named 'git'

    'Insert your own code here

    wait


[Reset_GIT]   'Perform action for the checkbox named 'git'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GJ]   'Perform action for the checkbox named 'gj'

    'Insert your own code here

    wait


[Reset_GJ]   'Perform action for the checkbox named 'gj'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GLS]   'Perform action for the checkbox named 'gls'

    'Insert your own code here

    wait


[Reset_GLS]   'Perform action for the checkbox named 'gls'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GL]   'Perform action for the checkbox named 'gl'

    'Insert your own code here

    wait


[Reset_GL]   'Perform action for the checkbox named 'gl'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GMC]   'Perform action for the checkbox named 'gmc'

    'Insert your own code here

    wait


[Reset_GMC]   'Perform action for the checkbox named 'gmc'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GM]   'Perform action for the checkbox named 'gm'

    'Insert your own code here

    wait


[Reset_GM]   'Perform action for the checkbox named 'gm'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GMD]   'Perform action for the checkbox named 'gmd'

    'Insert your own code here

    wait


[Reset_GMD]   'Perform action for the checkbox named 'gmd'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GMU]   'Perform action for the checkbox named 'gmu'

    'Insert your own code here

    wait


[Reset_GMU]   'Perform action for the checkbox named 'gmu'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GPA]   'Perform action for the checkbox named 'gpa'

    'Insert your own code here

    wait


[Reset_GPA]   'Perform action for the checkbox named 'gpa'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GP]   'Perform action for the checkbox named 'gp'

    'Insert your own code here

    wait


[Reset_GP]   'Perform action for the checkbox named 'gp'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GS]   'Perform action for the checkbox named 'gs'

    'Insert your own code here

    wait


[Reset_GS]   'Perform action for the checkbox named 'gs'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GSS]   'Perform action for the checkbox named 'gss'

    'Insert your own code here

    wait


[Reset_GSS]   'Perform action for the checkbox named 'gss'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GTW]   'Perform action for the checkbox named 'gtw'

    'Insert your own code here

    wait


[Reset_GTW]   'Perform action for the checkbox named 'gtw'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GO]   'Perform action for the checkbox named 'go'

    'Insert your own code here

    wait


[Reset_GO]   'Perform action for the checkbox named 'go'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GU]   'Perform action for the checkbox named 'gu'

    'Insert your own code here

    wait


[Reset_GU]   'Perform action for the checkbox named 'gu'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GN]   'Perform action for the checkbox named 'gn'

    'Insert your own code here

    wait


[Reset_GN]   'Perform action for the checkbox named 'gn'

    'Insert your own code here

    goto [main.inputLoop]


[Set_GAT]   'Perform action for the checkbox named 'gat'

    'Insert your own code here

    wait


[Reset_GAT]   'Perform action for the checkbox named 'gat'

    'Insert your own code here

    goto [main.inputLoop]

