'J-Paint made by Shadowy 2008
'

'FIXAS -> sidopanelsfunktionerna
'         fler menyalternativ
'         fotomanipuleringsfunktionerna
'         nya bilder?
'         titelbyte n�r filnamnet �ndras!

  'on error goto [error_occured]

  menu #jp,"File","New",[new_image],"Open",[open_image],"Save",[save_image],"Print",[print_image],|,"Quit",[exitprogram]
  menu #jp,"Edit","Undo",[branchLabel],"Redo",[branchLabel2],|,"Grayscale",[image_grayscale],"Negative",[image_negative]
  menu #jp,"Help","About",[help_me]

  bmpbutton #jp.new,"images\new.bmp",[new_image],UL,5,5
  bmpbutton #jp.open,"images\open.bmp",[open_image],UL,5,30
  bmpbutton #jp.save,"images\save.bmp",[save_image],UL,5,55
  bmpbutton #jp.print,"images\print.bmp",[print_image],UL,5,80
  bmpbutton #jp.help,"images\help.bmp",[help_me],UL,5,105
  bmpbutton #jp.draw,"images\draw.bmp",[draw_image],UL,5,130
  bmpbutton #jp.fill,"images\fill.bmp",[fill_area],UL,5,155
  bmpbutton #jp.circle,"images\circle.bmp",[make_circle],UL,5,180
  bmpbutton #jp.box,"images\box.bmp",[make_box],UL,5,205
  bmpbutton #jp.cut,"images\cut.bmp",[cut_area],UL,30,5
  bmpbutton #jp.copy,"images\copy.bmp",[copy_area],UL,30,30
  bmpbutton #jp.past,"images\paste.bmp",[paste_area],UL,30,55
  bmpbutton #jp.undo,"images\undo.bmp",[undo_draw],UL,30,80
  bmpbutton #jp.redo,"images\redo.bmp",[redo_draw],UL,30,105
  bmpbutton #jp.brush,"images\brush.bmp",[size_big],UL,30,130
  bmpbutton #jp.pick,"images\pick.bmp",[pick_color],UL,30,155
  bmpbutton #jp.rubb,"images\rubber.bmp",[set_rubber],UL,30,180
  bmpbutton #jp.none,"images\none.bmp",[do_nothing],UL,30,205

  graphicbox #jp.palette,60,432,212,32
  graphicbox #jp.color,40,432,17,32
  graphicbox #jp.main,60,5,730,422

  dim info$(0,0)
  NORMAL=0:MAKEBOX=1:MAKECIRCLE=2:PICKCOLOR=3:EARSE=4:FILLAREA=5
  GRAYSCALE=0:NEGATIVE=1
  Color1$="0 0 0":Color2$="255 255 255"
  penState=stateNormal
  imageSaved=1
  xmax=730:ymax=422
  title$="untitled"

  nomainwin
  call makeWindow 800,520,100,10
  open "J-Paint v1.0 - ";title$ for window_nf as #jp
  #jp,"trapclose [exitprogram]"

  #jp.main,"when leftButtonDown [leftStartPainting]"
  #jp.main,"when leftButtonMove [leftContinuePainting]"
  #jp.main,"when leftButtonUp [leftStopPainting]"
  #jp.main,"when rightButtonDown [rightStartPainting]"
  #jp.main,"when rightButtonMove [rightContinuePainting]"
  #jp.main,"when rightButtonUp [rightStopPainting]"
  #jp.palette,"when leftButtonDown [pickColor1]"
  #jp.palette,"when rightButtonDown [pickColor2]"

  call makeColor Color1$,Color2$
  call makePalette
  wait


  sub makePalette
    for i=1 to 14 step 1
      color1$=word$("0 0 0-128 128 128-128 0 0-128 128 0-0 128 0-0 128 128-0 0 128-128 0 128-128 128 64-0 64 64-0 128 255-0 64 128-128 0 255-128 64 0",i,"-")
      color2$=word$("255 255 255-192 192 192-255 0 0-255 255 0-0 255 0-0 255 255-0 0 255-255 0 255-255 255 128-0 255 128-128 255 255-128 128 255-255 0 128-255 128 64",i,"-")
      #jp.palette,"down;backcolor ";color1$;";color ";color1$;";place ";(i-1)*15;" 0;boxfilled ";i*15;" 15"
      #jp.palette,"backcolor ";color2$;";color ";color2$;";place ";(i-1)*15;" 15;boxfilled ";i*15;" 30"
    next i
    #jp.palette,"flush"
  end sub


  sub makeColor color1$,color2$
    #jp.color,"down;backcolor ";color1$;";color ";color1$;";place 0 0;boxfilled 15 15"
    #jp.color,"backcolor ";color2$;";color ";color2$;";place 0 15;boxfilled 15 30;flush"
  end sub


  sub makeWindow w,h,x,y
    WindowWidth=w:WindowHeight=h
    UpperLeftX=x:UpperLeftY=y
  end sub


  sub AlterImage filter
    for x=0 to xmax step 1
      for y=0 to ymax step 1
        select case filter
          case GRAYSCALE
            color$=GetGrayscale$(GetPixelValue$(x,y,"#jp.main"))
          case NEGATIVE
            color$=GetNegative$(GetPixelValue$(x,y,"#jp.main"))
        end select
        #jp.main,"size 1;color ";color$;";set ";x;" ";y
      next y
    next x
  end sub


  function GetGrayscale$(color$)
    red=val(word$(color$,1)):green=val(word$(color$,2)):blue=val(word$(color$,3))
    red=(red+green+blue)/3:green=(red+green+blue)/3:blue=(red+green+blue)/3
    GetGrayscale$=using("###",red)+using("####",green)+using("####",blue)
  end function


  function GetNegative$(color$)
    red=val(word$(color$,1)):green=val(word$(color$,2)):blue=val(word$(color$,3))
    GetGrayscale$=str$(255-red)+" "+str$(255-green)+" "+str$(255-blue)
  end function


  function GetPixelValue$(x,y,handle$)
    #handle$,"getbmp gpv ";x;" ";y;" ";1;" ";1
    bmpsave "gpv","getpvaluetemp.bmp"
    open "getpvaluetemp.bmp" for input as #gpv
    s$=input$(#gpv,lof(#gpv))
    close #gpv
    bpp=asc(mid$(s$,29,1))
    select case bpp
      case 32
        red=asc(mid$(s$,69,1))
        green=asc(mid$(s$,68,1))
        blue=asc(mid$(s$,67,1))
      case 16
        bytes=asc(mid$(s$,67,1))+256*asc(mid$(s$,68,1))
        red=(bytes and 63488)/256
        green=(bytes and 2016)/32*4
        blue=(bytes and 31)*8
    end select
    GetPixelValue$=using("###",red)+using("####",green)+using("####",blue)
    kill "getpvaluetemp.bmp"
    unloadbmp "gpv"
  end function


  function FileExists(fullPath$)
    files PathOnly$(fullPath$),FilenameOnly$(fullPath$),info$()
    FileExists=val(info$(0,0))>0
  end function


  function PathOnly$(fullPath$)
    PathOnly$=fullPath$
    while right$(PathOnly$,1)<>"\" and PathOnly$<>""
      PathOnly$=left$(PathOnly$,len(PathOnly$)-1)
    wend
  end function


  function FilenameOnly$(fullPath$)
    pathLength=len(PathOnly$(fullPath$))
    FilenameOnly$=right$(fullPath$,len(fullPath$)-pathLength)
  end function


'**********************************************************************************************************************************************
sub floodFill x,y,handle$,rgbBound$
  if GetPixelValueFromMem$(x,y)=rgbBound$ then exit sub 'already filled
  'push seed on the stack
  stackTop=1
  fillStack(stackTop,0)=x
  fillStack(stackTop,1)=y
  do while stackTop>0   'until empty
    #handle$,"set 0 20 ;\Pixels:";using("#######",drawPIXELcounter);" Stack:";using("#######",stackTop);"     "
    #handle$,"discard" 'not sure if it influences at all
    scan
    'get a point (pop from stack)
    x3=fillStack(stackTop,0)
    y3=fillStack(stackTop,1)
    stackTop=stackTop-1
    'now, go to the maximum possible - to the left and to the right
    'right. Let's include initial point here as well
    xr=x3
    x=x3
    do while x<width
      if GetPixelValueFromMem$(x,y3)<>rgbBound$ then
        drawPIXELcounter=drawPIXELcounter+1
        GetPixelResponse$(x,y3)=rgbBound$
        x=x+1
      else
        xr=x-1:exit do
      end if
    loop
    if x>=width then xr=width-1
    #handle$,"line ";x3;" ";y3;" ";x;" ";y3
    'left
    xl=x3
    x=x3-1
    do while x>=0
      if GetPixelValueFromMem$(x,y3)<>rgbBound$ then
        drawPIXELcounter=drawPIXELcounter+1
        GetPixelResponse$(x,y3)=rgbBound$
        x=x-1
      else
        xl=x+1:exit do
      end if
    loop
    if x<0 then xl=0
    #handle$,"line ";x3-1;" ";y3;" ";x;" ";y3
    'now, check lower line
    y=y3+1
    if y < height then
      pInFilled=-1
      for x = xr to xl step -1
        if GetPixelValueFromMem$(x,y)<>rgbBound$ then
          if pInFilled then    'we get righthmost not filled point
            pInFilled=0
            'put it on stack
            stackTop=stackTop+1
            fillStack(stackTop,0)=x
            fillStack(stackTop,1)=y
          end if
        else
          pInFilled=-1
        end if
      next
    end if
    'now, same for upper line
    y=y3-1
    if y>=0 then
      pInFilled=-1
      for x=xr to xl step -1
        if GetPixelValueFromMem$(x,y)<>rgbBound$ then
          if pInFilled then    'we get righthmost not filled point
            pInFilled=0
            'put it on stack
            stackTop=stackTop+1
            fillStack(stackTop,0)=x
            fillStack(stackTop,1)=y
          end if
        else
          pInFilled=-1
        end if
      next
    end if
  loop
  #handle$,"set 0 20 ;\Pixels:";using("#######",drawPIXELcounter);" Stack:";using("#######",stackTop);"     "
end sub


function GetPixelValueFromMem$(x,y)
  'this functiom will return pixel from pre-read (full image) bitmap
  'btw bitmap numbered from 0, to (width-1), (height-1). And Y is inverted.
  getPIXELask=getPIXELask+1
  if GetPixelResponse$(x, y)<>"" then GetPixelValueFromMem$=GetPixelResponse$(x,y): exit function
  getPixelCounter = getPixelCounter + 1
  i=height-y-1
  'oneLine$=Mid$(storedBitmap$,i*row+1,row)
  'triplet$=Mid$(oneLine$,x*4,3)
  triplet$=mid$(storedBitmap$,i*row+x*4,3)
  GetPixelValueFromMem$=triplet$
  GetPixelResponse$(x,y)=triplet$
end function


sub InitForGetPixelValueFromMem handle$
  'this thing saves bitmap, reads in bitmap for getPixel
  'global width, height, storedBitmap$
  #handle$,"getbmp gpv 0 0 "; width;" "; height
  'Save in a bmp file
  fileName$="getpvaluetemp.bmp"
  bmpsave "gpv",fileName$
  unloadbmp "gpv"
  'this part copied out of Andy Amaya spriteMaker program
  open fileName$ for Binary as #bmpIn
    'get the length of the file
    lenFile=LOF(#bmpIn)
    'get bmpHeaderInfo
    info$=Input$(#bmpIn,66)
    if Left$(info$,2)<>"BM" and Mid$(info$,29,1)<>Chr$(32) then
      notice "This program works only in Truecolor (millions of colors, JB saves 32-bit windows bitmap)."+Chr$(10)+"Please try again."
      'JB saves as 32 or as 16 bit. Now we deal only with 32
      end
    end if
    bmpWidth=Asc(Mid$(info$,19,1))+Asc(Mid$(info$,20,1))*256
    bmpHeight=Asc(Mid$(info$,23,1))+Asc(Mid$(info$,24,1))*256
    'but they are just what we saved so could ignore them
    'for 32 bit width * 4 ALWAYS divides by 4, so...
    row=bmpWidth*4
    'Set to start of bitmap color triplets
    seek #bmpIn,67
    'load bitmap data into 'storedBitmap$'
    storedBitmap$=Input$(#bmpIn,lenFile-67)
  close #bmpIn
  kill fileName$
  'this also clears mem
  redim GetPixelResponse$(width,height)
end sub
'**********************************************************************************************************************************************


[leftStartPainting]
  imageSaved=0
  select case penState
 '   case stateMakeBox
 '   case stateMakeCircle
 '   case statePickColor
  '  case ERASE
  '  case FILLAREA                        'Detta ska vara f�rgen som ska fyllas INTE kantf�rgen!
     ' FloodFill(MouseX,MouseY,"#jp.main",GetPixelValue$(MouseX,MouseY,"#jp.main"))
    case NORMAL
      #jp.main,"down;place ";MouseX;" ";MouseY
      goto [leftContinuePainting]
  end select
  wait

[leftContinuePainting]
  #jp.main,"color ";Color1$;";goto ";MouseX;" ";MouseY
  wait

[leftStopPainting]
  #jp.main,"up;flush"
  wait


[rightStartPainting]
  imageSaved=0
    select case penState
 '   case stateMakeBox
 '   case stateMakeCircle
 '   case statePickColor
 '   case stateErase
  '  case FILLAREA                        'Detta ska vara f�rgen som ska fyllas INTE kantf�rgen!
     ' FloodFill(MouseX,MouseY,"#jp.main",GetPixelValue$(MouseX,MouseY,"#jp.main"))
    case NORMAL
      #jp.main,"down;place ";MouseX;" ";MouseY
      goto [leftContinuePainting]
  end select
  wait

[rightContinuePainting]
  #jp.main,"color ";Color2$;";goto ";MouseX;" ";MouseY
  wait

[rightStopPainting]
  #jp.main,"up;flush"
  wait


[pickColor1]
  Color1$=GetPixelValue$(MouseX,MouseY,"#jp.palette")
  call makeColor Color1$,Color2$
  wait


[pickColor2]
  Color2$=GetPixelValue$(MouseX,MouseY,"#jp.palette")
  call makeColor Color1$,Color2$
  wait


[new_image]
  if not(imageSaved) then
    confirm "Image not saved! Save?";answer$
    if answer$="yes" then gosub [save_image2]
  end if
  #jp.main,"fill white;flush"
  imageSaved=1
  wait


[open_image]
  filedialog "Open saved image","*.bmp",bmpstring$
  if bmpstring$<>"" then
    if not(imageSaved) then
      confirm "Image not saved! Save?";answer$
      if answer$="yes" then gosub [save_image2]
    end if
    loadbmp "bmptemp",bmpstring$
    #jp.main,"place 0 0;drawbmp bmptemp;flush"
    unloadbmp "bmptemp"
  end if
  wait


[save_image]
  filedialog "Save the image","*.bmp",bmpstring$
  if bmpstring$<>"" then
    if FileExists(bmpstring$) then
      confirm "A file with that name allready exists! Overwrite?";answer$
      if answer$="no" then goto [save_image]
    end if
    #jp.main,"getbmp temp 0 0 ";xmax;" ";ymax
    if right$(bmpstring$,4)=".bmp" then bmpsave "temp",bmpstring$ else bmpsave "temp",bmpstring$+".bmp"
    imageSaved=1
  end if
  wait


[save_image2]
  filedialog "Save the image","*.bmp",bmpstring$
  if bmpstring$<>"" then
    if FileExists(bmpstring$) then
      confirm "A file with that name allready exists! Overwrite?";answer$
      if answer$="no" then goto [save_image2]
    end if
    #jp.main,"getbmp temp 0 0 ";xmax;" ";ymax
    if right$(bmpstring$,4)=".bmp" then bmpsave "temp",bmpstring$ else bmpsave "temp",bmpstring$+".bmp"
    imageSaved=1
  end if
  return


[print_image]
  wait


[help_me]
  notice "J-Paint"+chr$(13)+"J-Paint by Shadowy 2008"+chr$(13)+"Version 1.0"
  wait


[image_grayscale]
  call AlterImage GRAYSCALE
  wait


[image_negative]
  call AlterImage NEGATIVE
  wait


[draw_image]
  wait


[fill_area]
  penState=FILLAREA
  wait


[make_circle]
  penState=MAKECIRCLE
  wait


[make_box]
  penState=MAKEBOX
  wait


[cut_area]
  penState=CUTAREA
  wait


[copy_area]
  wait


[paste_area]
  wait


[undo_draw]
  wait


[redo_draw]
  wait


[size_big]
  penState=NORMAL
  #jp.main,"size 3"
  wait


[pick_color]
  penState=PICKCOLOR
  wait


[set_rubber]
  penState=ERASE
  wait


[do_nothing]
  wait


[exitprogram]
  if not(imageSaved) then
    confirm "Image not saved! Save?";answer$
    if answer$="yes" then gosub [save_image2]
  end if
  close #jp
  end

