'Utility/Program Launcher.  Submitted by JFB.  January, 2007

    nomainwin

    count = 20
    dim array$(count)

    dim info$(1,1)
    files DefaultDir$, "\list.txt", info$(
    if val(info$(0,0)) > 0 then

        open DefaultDir$ + "\list.txt" for input as #1

            do while iOpen < count

                input #1, array$(iOpen)
                iOpen = iOpen + 1

            loop

        close #1

    end if

    WindowWidth = 400
    WindowHeight = 390
    UpperLeftX = Int((DisplayWidth - WindowWidth)/2)
    UpperLeftY = Int((DisplayHeight - WindowHeight)/2)

    groupbox #main.groupbox1, "Utility Up To Run", 14, 20, 365, 110
    groupbox #main.groupbox2, "Utilities List", 14, 143, 365, 185

    textbox #main.textbox, 33, 45, 326, 25
    listbox #main.listbox, array$(), [List], 30, 168, 328, 140

    button #main.button1, "Run", [Run], UL, 33, 85, 215, 25
    button #main.button2, "Skip", [Skip], UL, 257, 85, 100, 25

    menu #main, "Menu", "Add A Utility To List", [Add],_
                        "Remove A Listed Utility", [Remove], |,_
                        "Save Current Utility List", [Save],_
                        "Delete Entire Utility List", [Delete]

    open "Utility Launcher" for window_nf as #main

    print #main, "font ms_sans_serif 0 12"
    print #main.textbox, array$(0)
    print #main.listbox, "singleclickselect"
    print #main.listbox, "selectindex 1"

    wait

[List]

    print #main.listbox, "selection? selected$"

    do while iList < count

        if selected$ = array$(iList) then

            iRun = iList
            iList = 0
            print #main.textbox, array$(iRun)
            exit do

        end if

        iList = iList + 1

    loop

    wait

[Run]

    print #main.button1, "!disable"

    run array$(iRun)

    timer 100, [continue.past]
    wait
    [continue.past]
    timer 0

    iRun = iRun + 1

    if array$(iRun) = "" or iRun >= count then iRun = 0

    print #main.textbox, array$(iRun)
    print #main.listbox, "selectindex "; iRun + 1

    print #main.button1, "!enable"

    wait

[Skip]

    iRun = iRun + 1

    if array$(iRun) = "" or iRun >= count then iRun = 0

    print #main.textbox, array$(iRun)
    print #main.listbox, "selectindex "; iRun + 1

    wait

[Add]

    do while iLoad < count

        if array$(iLoad) = "" then

            filedialog_
            "Navigate To Find The Utility You Want To Add To List",_
            "C:\Program Files\*.exe",fileName$

            if fileName$ = "" then exit do

            array$(iLoad) = fileName$
            print #main.textbox, array$(iLoad)
            print #main.listbox, "reload"
            print #main.listbox, "selectindex "; iLoad + 1
            iRun = iLoad
            exit do

        end if

        iLoad = iLoad + 1

    loop

    if iLoad >= count then notice "Utility List Is Full   "

    wait

[Remove]

    confirm "Do you want to Remove this Utility from List?   "_
            + chr$(13) + array$(iRun) + space$(3);remove$

    if remove$ = "yes" then

        do until array$(iRun) = ""

            array$(iRun) = array$(iRun + 1)
            iRun = iRun + 1

        loop

        iRun = 0

        print #main.textbox, array$(iRun)
        print #main.listbox, "reload"
        print #main.listbox, "selectindex "; iRun + 1

        confirm "Do you want to Save Modified List?   ";modify$

        if modify$ = "yes" then goto [Save]

    end if

    wait

[Save]

    iSave = 0

    open DefaultDir$ + "\list.txt" for output as #1

        do while iSave < count

            print #1, array$(iSave)
            iSave = iSave + 1

        loop

    close #1

    notice "List Should Be Saved   "

    wait

[Delete]

    confirm_
      "Do you really want to Delete the Entire Utility List   ";_
       delete$

    if delete$ = "yes" then

        open DefaultDir$ + "\list.txt" for output as #1

            do while iDelete < count

                array$(iDelete) = ""
                print #1, array$(iDelete)
                iDelete = iDelete + 1

            loop

        close #1

        iDelete = 0

        print #main.textbox, ""
        print #main.listbox, "reload"
        notice "List Should Be Gone   "

    end if

    wait
