
  nomainwin
  max=100:count=0
  dim xPos(max),yPos(max),xSpeed(max),ySpeed(max),Mass(max),Name$(max)


  Vearthpos=150
  Hearthpos=100
  hdist=60
  vdist=40
  vspeed=0
  hspeed=-0.8
  radius=7

  WindowWidth=260:WindowHeight=155
  statictext #main.name,"Name:",5,5,50,20
  statictext #main.mass,"Mass:",5,25,50,20
  statictext #main.xpos,"xPos:",5,45,50,20
  statictext #main.ypos,"yPos:",5,65,50,20
  statictext #main.xspeed,"xSpeed:",5,85,50,20
  statictext #main.yspeed,"ySpeed:",5,105,50,20
  textbox #main.name2,55,2,75,20
  textbox #main.mass2,55,22,75,20
  textbox #main.xpos2,55,42,75,20
  textbox #main.ypos2,55,62,75,20
  textbox #main.xspeed2,55,82,75,20
  textbox #main.yspeed2,55,102,75,20
  button #main.default,"Make planet!",[make_planet],UL,135,2,115,120
  open "Main Input" for dialog as #main
  #main,"trapclose [quit_orbiting]"
  #main.name2,"!setfocus"
  wait


  WindowWidth=900:WindowHeight=600
  open "Planetary Orbits" for graphics_nsb_nf as #orb
  #orb,"trapclose [quit_orbiting]"
  #orb,"home;down"




[make_planet]
  #main.name2,"!contents? ";Name$(count)
  #main.mass2,"!contents? ";mass$:Mass(count)=val(mass$)
  #main.xpos2,"!contents? ";xpos$:xPos(count)=val(xpos$)
  #main.ypos2,"!contents? ";ypos$:yPos(count)=val(ypos$)
  #main.xspeed2,"!contents? ";xspeed$:xSpeed(count)=val(xspeed$)
  #main.yspeed2,"!contents? ";yspeed$:ySpeed(count)=val(yspeed$)
  count=count+1
  #main.name2,""
  #main.mass2,""
  #main.xpos2,""
  #main.ypos2,""
  #main.xspeed2,""
  #main.yspeed2,""
  #main.name2,"!setfocus"
  wait


[quit_orbiting]
  close #main
  'close #orb
  end


  #orb, "place ";Hearthpos;" ";Vearthpos
  #orb, "circle ";radius



[main_loop]
  scan
  distance=SQR(hdist^2 + vdist^2)
  gravity=(radius/distance)^2
  haceleration=(hdist/distance)*gravity
  vaceleration=(vdist/distance)*gravity
  hspeed=hspeed-haceleration
  vspeed=vspeed-vaceleration
  hdist=hdist+hspeed
  vdist=vdist+vspeed
  #orb,"place ";Hearthpos+hdist;" ";Vearthpos+vdist
  #orb,"set ";Hearthpos+hdist;" ";Vearthpos+vdist
  call Pause 10
  if distance<radius then
    close #orb
    end
  end if
  #orb,"flush"
  goto [main_loop]





  sub Pause mil
    t=time$("milliseconds")
    while time$("milliseconds")<t+mil
    wend
  end sub

