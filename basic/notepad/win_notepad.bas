'J-Note made by Shadowy 2008
'v1.0 - 12/21/2008 - F�rsta utg�van!

'FIXAS -> automatiskt radbyte
'         skrivaralternativ
'         redigeramenyn
'         helpfilen

  'on error goto [error_occured]

  loadbmp "bold1","bmp\bold.bmp"
  loadbmp "bold2","bmp\bold2.bmp"
  loadbmp "italic1","bmp\italic.bmp"
  loadbmp "italic2","bmp\italic2.bmp"
  loadbmp "underscore1","bmp\underscore.bmp"
  loadbmp "underscore2","bmp\underscore2.bmp"

  global title$,text$,path$,modifiers$,find$,color,separator$,leadingZero,LB$
  dim info$(0,0),font$(18),size$(25),attr$(8),color$(16),symbol$(223),dates$(10),times$(10),bullets$(3)
  font$(0)="Arial"
  font$(1)="Arial_Black"
  font$(2)="Arial_Narrow"
  font$(3)="Century_Gothic"
  font$(4)="Comic_Sans_MS"
  font$(5)="Courier"
  font$(6)="Courier_New"
  font$(7)="Impact"
  font$(8)="MS_Sans_Serif"
  font$(9)="MS_Serif"
  font$(10)="Papyrus"
  font$(11)="Rockwell"
  font$(12)="Rockwell_Extra_Bold"
  font$(13)="System"
  font$(14)="Tahoma"
  font$(15)="Times_New_Roman"
  font$(16)="Vernada"
  font$(17)="Vivaldi"

  attr$(0)="Normal"
  attr$(1)="Bold"
  attr$(2)="Italic"
  attr$(3)="Underscore"
  attr$(4)="Bold Italic"
  attr$(5)="Bold Underscore"
  attr$(6)="Italic Underscore"
  attr$(7)="Bold Italic Underscore"

  for i=0 to 15 step 1
    temp$=word$("Yellow Brown Red Darkred Pink Darkpink Blue Darkblue Green Darkgreen Cyan Darkcyan White Black Lightgray Darkgray",i+1)
    color$(i)=temp$
  next i
  for i=0 to 24 step 1
    temp$=word$("6 8 9 10 11 12 14 16 18 20 22 24 26 28 30 34 38 40 44 48 52 56 60 66 72",i+1)
    size$(i)=temp$
  next i
  for i=0 to 222 step 1
    symbol$(i)=chr$(i+33)
  next i
  bullets$(0)=chr$(248)
  bullets$(1)=chr$(42)
  bullets$(2)=chr$(187)

  if FileExists(DefaultDir$;"\settings.ini") then
    open DefaultDir$;"\settings.ini" for input as #st
      line input #st,path$ 
      line input #st,autoload$
      line input #st,modifiers$
      input #st,auto$
      input #st,date$
      input #st,time$
      input #st,separator$
      input #st,leadingZero$ 
      input #st,font$
      input #st,size$
      input #st,color$ 
      input #st,bkcolor$
      input #st,confirmOnExit$
      input #st,fullScreen$
      input #st,numerize$
      input #st,bulletize$
    close #st
    autoload=val(auto$):date=val(date$):time=val(time$)
    datetime$=dates$(date);" ";times$(time)
    leadingZero=val(leadingZero$):bkcolor=val(bkcolor$)
    font=val(font$):size=val(size$):color=val(color$)
    confirmOnExit=val(confirmOnExit$):fullScreen=val(fullScreen$)
    numerize=val(numerize$):dottize=val(bulletize$)
  else
    font=15:size=3:color=13:bkcolor=12
    datetime$=dates$(1);" ";times$(1)
    path$=DefaultDir$
    separator$="/"
  end if

  title$="untitled"
  if autoload=2 and autoload$<>"" then
    open autoload$ for input as #al
      text$=input$(#al,lof(#al))
      title$=FilenameOnly$(autoload$)
    close #al
  end if
  if autoload=1 and recentfile$<>"" then

  end if

  LB$=chr$(13)+chr$(10)
  call MakeDateAndTime date$("mm/dd/yyyy"),time$()

[begin_program]

  if fullScreen then WindowWidth=DisplayWidth:WindowHeight=DisplayHeight else WindowWidth=800:WindowHeight=600
  UpperLeftX=int((DisplayWidth-WindowWidth)/2):UpperLeftY=int((DisplayHeight-WindowHeight)/2)

  menu #jn,"&File","&New",[document_new],"&Open...",[document_open],"&Save",[document_save],"Save as...",[document_save_as],|,"Printer options",[printer_options],"&Print...",[document_print],|,"&Quit",[exit_program]
  menu #jn,"Edit"',"Undo",[text_undo],|,"Cut",[text_cut],"Copy",[text_copy],"Paste",[text_paste],"Clear",[text_clear],"Select All",[text_select_all],|,"Find/Replace...",[find_replace],"Find Again",[find_again]
  menu #jn,"&Insert","Date and Time",[insert_date_time_dialog],"Symbol",[insert_symbol],"Text file",[insert_textfile]
  menu #jn,"Forma&t","Automatic line feed",[set_automatic_line_feed],"Font...",[set_font_dialog]
  menu #jn,"T&ools","Count Characters and Lines",[tools_count_text],"File Information",[tools_file_information],"Change Case",[tools_change_case],"Bullets and Numering",[tools_numerize],|,"Settings",[tools_settings]
  menu #jn,"&Help","Help",[help_me],|,"About J-Note",[about_it]

  bmpbutton #jn.new,"bmp\new.bmp",[document_new],UL,1,0
  bmpbutton #jn.open,"bmp\open.bmp",[document_open],UL,24,0
  bmpbutton #jn.save,"bmp\save.bmp",[document_save],UL,47,0
  bmpbutton #jn.print,"bmp\print.bmp",[document_print],UL,70,0
  bmpbutton #jn.cut,"bmp\cut.bmp",[text_cut],UL,98,0
  bmpbutton #jn.copy,"bmp\copy.bmp",[text_copy],UL,121,0
  bmpbutton #jn.paste,"bmp\paste.bmp",[text_paste],UL,144,0
  graphicbox #jn.bold,172,0,24,22
  graphicbox #jn.italic,195,0,24,22
  graphicbox #jn.usc,218,0,24,22
  bmpbutton #jn.color,"bmp\color.bmp",[set_color_dialog],UL,241,0
  bmpbutton #jn.date,"bmp\nums.bmp",[text_numerize],UL,269,0
  bmpbutton #jn.date,"bmp\dots.bmp",[text_doterize],UL,292,0
  bmpbutton #jn.date,"bmp\date.bmp",[insert_date_time],UL,315,0
  bmpbutton #jn.help,"bmp\help2.bmp",[about_it],UL,338,0
  combobox #jn.size,size$(),[set_font_size],371,0,40,25
  combobox #jn.font,font$(),[set_font_face],421,0,130,25
  combobox #jn.color,color$(),[set_font_color],561,0,80,25

  ForegroundColor$=color$(color)
  TexteditorColor$=color$(bkcolor)
  texteditor #jn.text,0,26,WindowWidth-8,WindowHeight-72
  texteditor #jn.hide,0,0,0,0'Hidden texteditor for easier text manipulation

  nomainwin
  open "J-Note v1.0 - ";title$ for window as #jn
  #jn,"trapclose [exit_program]"
  if instr(modifiers$,"Bold") then #jn.bold,"down;drawbmp bold2 -1 -1;flush":bold$=" Bold" else #jn.bold,"down;drawbmp bold1 -1 -1;flush":bold$=""
  if instr(modifiers$,"Italic") then #jn.italic,"down;drawbmp italic2 -1 -1;flush":italic$=" Italic" else #jn.italic,"down;drawbmp italic1 -1 -1;flush":italic$=""
  if instr(modifiers$,"Underscore") then #jn.usc,"down;drawbmp underscore2 -1 -1;flush":underscore$=" Underscore" else #jn.usc,"down;drawbmp underscore1 -1 -1;flush":underscore$=""
  #jn.bold,"when leftButtonDown [text_bold]"
  #jn.italic,"when leftButtonDown [text_italic]"
  #jn.usc,"when leftButtonDown [text_underscore]"
  #jn.text,"!font ";font$(font);" ";size$(size);modifiers$
  #jn.size,"selectindex ";size+1
  #jn.font,"selectindex ";font+1
  #jn.color,"selectindex ";color+1
  #jn.text,"!autoresize"
  #jn.text,text$;
  #jn.text,"!origin 1 1"
  wait

'-------------------------------------------------------------------------------------------------------[ FILE ]-------------------------------

[document_new]
  #jn.text,"!modified? changed$"
  if changed$="true" then gosub [document_save2]
  if title$<>"untitled" then
    call makeTitle "untitled"
    goto [begin_program]
  end if
  #jn.text,"!cls"
  #jn.text,"!setfocus"
  wait


[document_open]
  #jn.text,"!modified? changed$"
  if changed$="true" then gosub [document_save2]
  filedialog "Open text file",path$;"*.txt",temp$
  if temp$<>"" then
    #jn.text,"!cls"
    open temp$ for input as #load
      #jn.text,"!contents #load"
    close #load
    path$=PathOnly$(temp$)
    if title$<>FilenameOnly$(temp$) then
      call makeTitle FilenameOnly$(temp$)
      goto [begin_program]
    end if
  end if
  #jn.text,"!setfocus"
  wait


[document_save]
  if title$="untitled" then goto [document_save_as]
  open path$;title$ for output as #save
    #jn.text,"!contents? text$"
    print #save,text$
  close #save
  wait


[document_save2]
  confirm "The text in the file "+title$+" has been modified!"+chr$(13)+"Do you want to save these changes?";answer$
  'call ConfirmDialog "J-Note","The text in the file "+title$+" has been modified!","Do you want to save these changes?"
  if answer$="yes"then
    [try_again]
    filedialog "Save text file",path$;"*.txt",temp$
    if temp$<>"" then
      if right$(temp$,4)<>".txt" then temp$=temp$+".txt"
      if FileExists(temp$) then
        confirm temp$+" already exists!"+chr$(13)+"Do you really want to overwrite it?";answer$
        if answer$="no" then goto [try_again]
      end if
      #jn.text,"!contents? text$"
      open temp$ for output as #save
        print #save,text$
      close #save
      path$=PathOnly$(temp$)
    end if
  end if
  return


[document_save_as]
  filedialog "Save text file",path$;"*.txt",temp$
  if temp$<>"" then
    if right$(temp$,4)<>".txt" then temp$=temp$+".txt"
    if FileExists(temp$) then
      confirm temp$+" already exists!"+chr$(13)+"Do you really want to overwrite it?";answer$
      if answer$="no" then goto [document_save_as]
    end if
    #jn.text,"!contents? text$"
    open temp$ for output as #save
      print #save,text$
    close #save
    path$=PathOnly$(temp$)
    if title$<>FilenameOnly$(temp$) then
      call makeTitle FilenameOnly$(temp$)
      goto [begin_program]
    end if
  end if
  #jn.text,"!setfocus"
  wait


[printer_options]
  notice "J-Note"+chr$(13)+"This feature is currently not available in this version"+chr$(13)+"of J-Note. Please check online for updates!"
  #jn.text,"!setfocus"
  wait


[document_print]
  #jn.text,"!contents? text$"
  PrinterFont$="font ";font$(font);" ";size$(size);modifiers$
  lprint PrinterFont$;text$
  dump
  #jn.text,"!setfocus"
  wait

'-------------------------------------------------------------------------------------------------------[ EDIT ]-------------------------------

[text_undo]
  wait


[text_cut]
  #jn.text,"!cut"
  wait


[text_copy]
  #jn.text,"!copy"
  wait


[text_paste]
  #jn.text,"!paste"
  wait


[text_clear]
  #jn.text,"!cls"
  wait


[text_select_all]
  #jn.text,"!selectall"
  wait


[find_replace]
  WindowWidth=272:WindowHeight=180
  statictext #fr,"Find:",5,3,100,15
  textbox #fr.find,5,18,257,20
  statictext #fr,"Replace with:",5,40,100,15
  textbox #fr.replace,5,57,257,20
  checkbox #fr.cs,"Case sensitive",[case_sensitive_on],[case_sensitive_off],5,80,100,20
  checkbox #fr.cf,"Confirm",[confirm_on],[confirm_off],5,100,100,20
  groupbox #fr,"Direction",120,80,140,38
  radiobutton #fr.fw,"Forward",[search_forward],[search_backward],125,95,60,20
  radiobutton #fr.bw,"Backward",[search_backward],[search_forward],187,95,70,20
  button #fr.default,"Find",[find_replace_okay],UL,5,125,65,22
  button #fr.replaceall,"Replace All",[find_replace_all],UL,73,125,120,22
  button #fr.cancel,"Cancel",[find_replace_cancel],UL,197,125,65,22
  open "Find/Replace" for dialog_modal as #fr
  #fr,"trapclose [find_replace_cancel]"
  #fr.fw,"set"
  #jn.text,"!selection? temp$"
  if temp$<>"" then #fr.find,temp$ 
  #fr.find,"!setfocus"
  wait


[case_sensitive_on]
  caseSensitive=1
  wait


[case_sensitive_off]
  caseSensitive=0
  wait


[confirm_on]
  confirmThat=1
  wait


[confirm_off]
  confirmThat=0
  wait


[search_forward]
  forward=1
  wait


[search_backward]
  forward=0
  wait


[find_replace_okay]
  #fr.find,"!contents? find$"
  if find$<>"" then

  end if
  wait


[find_replace_all]
  #fr.find,"!contents? find$"
  #fr.replace,"!contents? replace$"
  if find$<>"" then

  end if
  wait


[find_replace_cancel]
  close #fr
  wait


[find_again]
  if find$<>"" then

  end if
  wait

'-------------------------------------------------------------------------------------------------------[ INSERT ]-----------------------------

[insert_date_time_dialog]
  ForegroundColor$="black"
  WindowWidth=308:WindowHeight=273
  statictext #dt.txt1,"Available date formats:",5,5,136,15
  statictext #dt.txt2,"Available time formats:",155,5,136,15
  listbox #dt.list1,dates$(),[insert_date_time_update],0,20,175,150
  listbox #dt.list2,times$(),[insert_date_time_update],175,20,127,150
  groupbox #dt,"Date separators",2,170,95,70
  radiobutton #dt.slash,"Use slashes",[use_slash],[do_nothing],8,185,77,20
  radiobutton #dt.lines,"Use lines",[use_lines],[do_nothing],8,200,77,20
  radiobutton #dt.dots,"Use dots",[use_dots],[do_nothing],8,217,77,20
  checkbox #dt.zero,"Add leading zeroes",[add_leading_zeroes],[non_leading_zeroes],115,174,120,20
  button #dt.default,"Insert",[insert_date_time_okay],UL,238,175,60,22
  groupbox #dt.final,"Preview",100,200,198,40
  statictext #dt.txt,datetime$,107,218,190,20
  open "Date and Time" for dialog_modal as #dt
  #dt,"trapclose [insert_date_time_exit]"
  #dt.txt,"!font courier_new 8"
  #dt.list1,"singleclickselect"
  #dt.list2,"singleclickselect"
  #dt.list1,"selectindex ";date+1
  #dt.list2,"selectindex ";time+1
  if separator$="." then #dt.dots,"set"
  if separator$="-" then #dt.lines,"set"
  if separator$="/" then #dt.slash,"set"
  if leadingZero then #dt.zero,"set"


[insert_date_time_update]
  #dt.list1,"selectionindex? date"
  #dt.list2,"selectionindex? time"
  date=date-1:time=time-1
  datetime$=dates$(date);" ";times$(time)
  #dt.txt,datetime$
  wait


[use_dots]
  separator$="."
  goto [insert_date_time_update_lists]


[use_lines]
  separator$="-"
  goto [insert_date_time_update_lists]


[use_slash]
  separator$="/"
  goto [insert_date_time_update_lists]


[add_leading_zeroes]
  leadingZero=1
  goto [insert_date_time_update_lists]


[non_leading_zeroes]
  leadingZero=0
  goto [insert_date_time_update_lists]


[insert_date_time_update_lists]
  call MakeDateAndTime date$("mm/dd/yyyy"),time$()
  #dt.list1,"reload"
  #dt.list2,"reload"
  #dt.list1,"selectindex ";date+1
  #dt.list2,"selectindex ";time+1
  goto [insert_date_time_update]


[insert_date_time_okay]
  #jn.text,datetime$;
  close #dt
  wait


[insert_date_time_exit]
  close #dt
  wait


[insert_date_time]
  #jn.text,datetime$;
  #jn.text,"!setfocus"
  wait


[insert_symbol]
  ForegroundColor$="black"
  WindowWidth=148:WindowHeight=235
  listbox #is.list,symbol$(),[update_box2],3,3,50,200
  groupbox #is,"Preview",58,3,80,80
  graphicbox #is.box,66,18,65,57
  button #is.default,"Insert",[insert_symbol_okay],UL,58,154,79,22
  button #is.cancel,"Close",[insert_symbol_exit],UL,58,180,79,22
  open "Symbol" for dialog_modal as #is
  #is,"trapclose [insert_symbol_exit]"
  #is.box,"down;fill buttonface;flush"
  #is.list,"singleclickselect"
  #is.list,"selectindex 1"


[update_box2]
  #is.list,"selection? char$"
  #is.box,"fill buttonface;backcolor buttonface;color black;place 10 40"
  #is.box,"font Times_New_Roman Bold 30;|";char$
  #is.box,"flush"
  wait


[insert_symbol_okay]
  #jn.text,char$;
  #is.list,"setfocus"
  wait


[insert_symbol_exit]
  close #is
  wait


[insert_textfile]
  filedialog "Open text file",path$;"*.txt",temp$
  if temp$<>"" then
    open temp$ for input as #tmp
      insert$=input$(#tmp,lof(#tmp))
      #jn.text,insert$
    close #tmp
  end if
  wait

'-------------------------------------------------------------------------------------------------------[ FORMAT ]-----------------------------

[set_automatic_line_feed]
  notice "J-Note"+chr$(13)+"This feature is currently not available in this version"+chr$(13)+"of J-Note. Please check online for updates!"
  #jn.text,"!setfocus"
  wait


[set_font_dialog]
  ForegroundColor$="black"
  WindowWidth=318:WindowHeight=245
  listbox #ft.font,font$(),[update_box],0,0,130,150
  listbox #ft.size,size$(),[update_box],130,0,50,150
  listbox #ft.attr,attr$(),[update_box],180,0,130,150
  button #ft.cancel,"Cancel",[set_font_cancel],UL,257,183,50,20
  button #ft.default,"OK",[set_font_okay],UL,257,158,50,20
  groupbox #ft,"Preview",2,152,115,62
  graphicbox #ft.box,10,168,98,38
  groupbox #ft,"Effects",122,152,130,62
  checkbox #ft.str,"Strikeout",[set_strikeout],[reset_strikeout],130,166,100,20
  combobox #ft.color,color$(),[update_box],130,186,115,22
  open "Font..." for dialog_modal as #ft
  #ft,"trapclose [set_font_cancel]"
  #ft.font,"singleclickselect"
  #ft.size,"singleclickselect"
  #ft.attr,"singleclickselect"
  #ft.attr,"selectindex 1"
  for i=0 to 7 step 1
    if attr$(i)=right$(modifiers$,len(modifiers$)-1) then
      #ft.attr,"selectindex ";i+1
      exit for
    end if
  next i
  #ft.font,"selectindex ";font+1
  #ft.size,"selectindex ";size+1
  #ft.color,"selectindex ";color+1
  if instr(modifiers$,"Strikeout") then #ft.str,"set"
  #ft.box,"fill buttonface;flush"


[update_box]
  #ft.font,"selectionindex? font2"
  #ft.size,"selectionindex? size2"
  #ft.color,"selectionindex? color2"
  #ft.attr,"selection? attr$"
  #ft.box,"fill buttonface;backcolor buttonface;color ";color$(color2-1);";down;place 10 25"
  #ft.box,"font ";font$(font2-1);" ";size$(size2-1);attr$;strikeout$;";|AaBbYyZz"
  #ft.box,"flush"
  wait


[set_strikeout]
  strikeout$=" Strikeout"
  goto [update_box]
  wait


[reset_strikeout]
  strikeout$=""
  goto [update_box]
  wait


[set_font_okay]
  #ft.font,"selectionindex? font"
  #ft.size,"selectionindex? size"
  #ft.color,"selectionindex? color2"
  #ft.attr,"selection? attr$"
  if instr(attr$,"Bold")<>0 then
    #jn.bold,"drawbmp bold2 -1 -1;flush"
    bold$=" Bold"
  else
    #jn.bold,"drawbmp bold1 -1 -1;flush"
    bold$=""
  end if
  if instr(attr$,"Italic")<>0 then
    #jn.italic,"drawbmp italic2 -1 -1;flush"
    italic$=" Italic"
  else
    #jn.italic,"drawbmp italic1 -1 -1;flush"
    italic$=""
  end if
  if instr(attr$,"Underscore")<>0 then
    #jn.usc,"drawbmp underscore2 -1 -1;flush"
    underscore$=" Underscore"
  else
    #jn.usc,"drawbmp underscore1 -1 -1;flush"
    underscore$=""
  end if
  #jn.font,"selectindex ";font
  #jn.size,"selectindex ";size
  #jn.color,"selectindex ";color2
  modifiers$=" ";attr$;strikeout$
  font=font-1:size=size-1
  #jn.text,"!font ";font$(font);" ";size$(size);modifiers$ 
  close #ft
  if color<>color2-1 then
    color=color2-1
    goto [set_font_color]
  end if
  wait


[set_font_cancel]
  close #ft
  wait


[set_color_dialog]
  WindowWidth=248:WindowHeight=75
  graphicbox #cw.c0,0,0,30,22
  graphicbox #cw.c1,0,22,30,22
  graphicbox #cw.c2,30,0,30,22
  graphicbox #cw.c3,30,22,30,22
  graphicbox #cw.c4,60,0,30,22
  graphicbox #cw.c5,60,22,30,22
  graphicbox #cw.c6,90,0,30,22
  graphicbox #cw.c7,90,22,30,22
  graphicbox #cw.c8,120,0,30,22
  graphicbox #cw.c9,120,22,30,22
  graphicbox #cw.ca,150,0,30,22
  graphicbox #cw.cb,150,22,30,22
  graphicbox #cw.cc,180,0,30,22
  graphicbox #cw.cd,180,22,30,22
  graphicbox #cw.ce,210,0,30,22
  graphicbox #cw.cf,210,22,30,22
  open "Font color..." for dialog_modal as #cw
  #cw,"trapclose [set_color_exit]"
  #cw.c0,"fill ";color$(0);";flush;when leftButtonDown [pick_color_0]"
  #cw.c1,"fill ";color$(1);";flush;when leftButtonDown [pick_color_1]"
  #cw.c2,"fill ";color$(2);";flush;when leftButtonDown [pick_color_2]"
  #cw.c3,"fill ";color$(3);";flush;when leftButtonDown [pick_color_3]"
  #cw.c4,"fill ";color$(4);";flush;when leftButtonDown [pick_color_4]"
  #cw.c5,"fill ";color$(5);";flush;when leftButtonDown [pick_color_5]"
  #cw.c6,"fill ";color$(6);";flush;when leftButtonDown [pick_color_6]"
  #cw.c7,"fill ";color$(7);";flush;when leftButtonDown [pick_color_7]"
  #cw.c8,"fill ";color$(8);";flush;when leftButtonDown [pick_color_8]"
  #cw.c9,"fill ";color$(9);";flush;when leftButtonDown [pick_color_9]"
  #cw.ca,"fill ";color$(10);";flush;when leftButtonDown [pick_color_10]"
  #cw.cb,"fill ";color$(11);";flush;when leftButtonDown [pick_color_11]"
  #cw.cc,"fill ";color$(12);";flush;when leftButtonDown [pick_color_12]"
  #cw.cd,"fill ";color$(13);";flush;when leftButtonDown [pick_color_13]"
  #cw.ce,"fill ";color$(14);";flush;when leftButtonDown [pick_color_14]"
  #cw.cf,"fill ";color$(15);";flush;when leftButtonDown [pick_color_15]"
  wait


[pick_color_0]
  close #cw
  if color<>0 then
    #jn.text,"!contents? text$"
    close #jn
    color=0
    goto [begin_program]
  end if
  wait


[pick_color_1]
  close #cw
  if color<>1 then
    #jn.text,"!contents? text$"
    close #jn
    color=1
    goto [begin_program]
  end if
  wait


[pick_color_2]
  close #cw
  if color<>2 then
    #jn.text,"!contents? text$"
    close #jn
    color=2
    goto [begin_program]
  end if
  wait


[pick_color_3]
  close #cw
  if color<>3 then
    #jn.text,"!contents? text$"
    close #jn
    color=3
    goto [begin_program]
  end if
  wait


[pick_color_4]
  close #cw
  if color<>4 then
    #jn.text,"!contents? text$"
    close #jn
    color=4
    goto [begin_program]
  end if
  wait


[pick_color_5]
  close #cw
  if color<>5 then
    #jn.text,"!contents? text$"
    close #jn
    color=5
    goto [begin_program]
  end if
  wait


[pick_color_6]
  close #cw
  if color<>6 then
    #jn.text,"!contents? text$"
    close #jn
    color=6
    goto [begin_program]
  end if
  wait


[pick_color_7]
  close #cw
  if color<>7 then
    #jn.text,"!contents? text$"
    close #jn
    color=7
    goto [begin_program]
  end if
  wait


[pick_color_8]
  close #cw
  if color<>8 then
    #jn.text,"!contents? text$"
    close #jn
    color=8
    goto [begin_program]
  end if
  wait


[pick_color_9]
  close #cw
  if color<>9 then
    #jn.text,"!contents? text$"
    close #jn
    color=9
    goto [begin_program]
  end if
  wait


[pick_color_10]
  close #cw
  if color<>10 then
    #jn.text,"!contents? text$"
    close #jn
    color=10
    goto [begin_program]
  end if
  wait


[pick_color_11]
  close #cw
  if color<>11 then
    #jn.text,"!contents? text$"
    close #jn
    color=11
    goto [begin_program]
  end if
  wait


[pick_color_12]
  close #cw
  if color<>12 then
    #jn.text,"!contents? text$"
    close #jn
    color=12
    goto [begin_program]
  end if
  wait


[pick_color_13]
  close #cw
  if color<>13 then
    #jn.text,"!contents? text$"
    close #jn
    color=13
    goto [begin_program]
  end if
  wait


[pick_color_14]
  close #cw
  if color<>14 then
    #jn.text,"!contents? text$"
    close #jn
    color=14
    goto [begin_program]
  end if
  wait


[pick_color_15]
  close #cw
  if color<>15 then
    #jn.text,"!contents? text$"
    close #jn
    color=15
    goto [begin_program]
  end if
  wait


[set_color_exit]
  close #cw
  wait

'-------------------------------------------------------------------------------------------------------[ TOOLS ]------------------------------

[tools_count_text]
  #jn.text,"!lines lineCount"
  blankLineCount=0
  wordCount=0
  charNum=0
  for i=1 to lineCount step 1
    #jn.text,"!line ";i;" line$"
    charCount=charCount+len(line$)
    wordCount=wordCount+WordCount(line$)
    if line$="" then blankLineCount=blankLineCount+1
  next i
  call InformationDialog "Document Counter",path$;title$,"Total lines:",str$(lineCount),"Blank lines:",str$(blankLineCount),_
  "Words:",str$(wordCount),"Characters:",str$(charCount)
  wait


[tools_file_information]
  if title$<>"untitled" then
    if FileExists(path$;title$) then
      call InformationDialog "File Information",info$(0,2);info$(0,3);info$(1,0),"File name: ",info$(1,0),"File size:",Size$(val(info$(1,1))),_
      "Created on (date): ",word$(info$(1,2),1),"Created at (time): ",word$(info$(1,2),2)
    end if
    'else
      'confirm "The text file is not saved! This is needed for J-Note to retrieve the information."+chr$(13)+"Do you want to save the text file now?";answer$
      'if answer$="yes" then
      '  gosub [document_save2]
      '  goto [tools_file_information]
      'end if
  end if
  wait


[tools_change_case]
  #jn.text,"!selection? selected$"
  if selected$<>"" then
    example$="thiS IS an ExaMPle TEXt"
    ForegroundColor$="black"
    WindowWidth=380:WindowHeight=157
    groupbox #cc,"Select case",5,3,180,120
    groupbox #cc,"Example",188,3,180,45
    groupbox #cc,"Result",188,48,180,45
    radiobutton #cc.upper,"Upper",[tools_change_case_upper],[do_nothing],15,18,100,20
    radiobutton #cc.lower,"Lower",[tools_change_case_lower],[do_nothing],15,38,100,20
    radiobutton #cc.sentence,"Sentence",[tools_change_case_sentence],[do_nothing],15,58,100,20
    radiobutton #cc.title,"Title",[tools_change_case_title],[do_nothing],15,78,100,20
    radiobutton #cc.toggle,"Toggle",[tools_change_case_toggle],[do_nothing],15,98,100,20
    statictext #cc.txt1,example$,198,23,165,20
    statictext #cc.txt2,example$,198,68,165,20
    button #cc.default,"Okay",[tools_change_case_okay],UL,203,98,80,22
    button #cc.cancel,"Cancel",[tools_change_case_cancel],UL,288,98,80,22
    open "Change Case" for dialog_modal as #cc
    #cc,"trapclose [tools_change_case_cancel]"
    #cc.txt1,"!font courier_new 8"
    #cc.txt2,"!font courier_new 8"
  else
    notice "J-Note";chr$(13);"Please select some text before you do this operation!"
  end if
  wait

[tools_change_case_upper]
  #cc.txt2,upper$(example$)
  cc$="uc"
  wait


[tools_change_case_lower]
  #cc.txt2,lower$(example$)
  cc$="lc"
  wait


[tools_change_case_sentence]
  #cc.txt2,SentenceCase$(example$)
  cc$="sc"
  wait


[tools_change_case_title]
  #cc.txt2,TitleCase$(example$)
  cc$="tc"
  wait


[tools_change_case_toggle]
  #cc.txt2,ToggleCase$(example$)
  cc$="tg"
  wait


[tools_change_case_okay]
  select case cc$
    case "uc":selected$=upper$(selected$)
    case "lc":selected$=lower$(selected$)
    case "sc":selected$=SentenceCase$(selected$)
    case "tc":selected$=TitleCase$(selected$)
    case "tg":selected$=ToggleCase$(selected$)
  end select
  call InsertIntoText selected$
  close #cc
  wait


[tools_change_case_cancel]
  close #cc
  wait


[tools_numerize]
  ForegroundColor$="black"
  WindowWidth=380:WindowHeight=157
  groupbox #nr.nums,"Numbered",5,3,180,90
  groupbox #nr.dots,"Bulleted",188,3,180,90
  radiobutton #nr.num1,"Numbers (1, 2, 3...)",[tools_numerize_numbers],[do_nothing],15,20,140,20
  radiobutton #nr.num2,"Letters (A, B, C...)",[tools_numerize_letters],[do_nothing],15,40,140,20
  radiobutton #nr.num3,"Roman numbers (I, II, III...)",[tools_numerize_roman],[do_nothing],15,60,150,20
  radiobutton #nr.dota,"Ball ( ";bullets$(0);" )",[tools_numerize_balls],[do_nothing],198,20,100,20
  radiobutton #nr.dotb,"Star ( ";bullets$(1);" )",[tools_numerize_stars],[do_nothing],198,40,100,20
  radiobutton #nr.dotc,"Arrows ( ";bullets$(2);" )",[tools_numerize_arrows],[do_nothing],198,60,100,20
  button #nr.default,"OK",[tools_numerize_okay],UL,203,98,80,22
  button #nr.cancel,"Cancel",[tools_numerize_cancel],UL,288,98,80,22
  open "Bullets and Numbering" for dialog_modal as #nr
  #nr,"trapclose [tools_numerize_cancel]"
  if numerize=0 then #nr.num1,"set"
  if numerize=1 then #nr.num2,"set"
  if numerize=2 then #nr.num3,"set"
  if dottize=0 then #nr.dota,"set"
  if dottize=1 then #nr.dotb,"set"
  if dottize=2 then #nr.dotc,"set"
  wait


[tools_numerize_numbers]
  tmpNumerize=0
  wait


[tools_numerize_letters]
  tmpNumerize=1
  wait


[tools_numerize_roman]
  tmpNumerize=2
  wait


[tools_numerize_balls]
  tmpDottize=0
  wait


[tools_numerize_stars]
  tmpDottize=1
  wait


[tools_numerize_arrows]
  tmpDottize=2
  wait


[tools_numerize_okay]
  numerize=tmpNumerize
  dottize=tmpDottize
  close #nr
  wait


[tools_numerize_cancel]
  close #nr
  wait


[tools_settings]
  ForegroundColor$="black"
  WindowWidth=287:WindowHeight=243
  groupbox #st,"Start-up options",5,3,270,115
  radiobutton #st.nofile,"Open no file",[settings_nofile],[do_nothing],12,20,80,20
  radiobutton #st.recentfile,"Open most recent file",[settings_recentfile],[do_nothing],12,40,120,20
  radiobutton #st.thisfile,"Open this file:",[settings_thisfile],[do_nothing],12,60,85,20
  textbox #st.filetext,100,60,100,20
  button #st.getfile,"Browse...",[settings_browse],UL,205,60,60,20
  checkbox #st.fullscreen,"Open in full-screen mode",[settings_full_screen],[settings_user_size],12,90,170,20
  groupbox #st,"Other options",5,118,270,66
  checkbox #st.confirm,"Confirm when exiting the program",[settings_confirm],[settings_skip_it],12,135,180,20
  statictext #st.txt,"Background color:",12,157,88,20
  combobox #st.bkcolor,color$(),[settings_bkcolor],100,155,100,20
  button #st.default,"OK",[settings_okay],UL,168,188,50,22
  button #st.cancel,"Cancel",[settings_cancel],UL,225,188,50,22
  open "Settings" for dialog_modal as #st
  #st,"trapclose [settings_cancel]"
  if autoload$<>"" then #st.filetext,FilenameOnly$(autoload$)
  if fullScreen then #st.fullScreen,"set"
  if confirmOnExit then #st.confirm,"set"
  if autoload=0 then #st.nofile,"set":#st.filetext,"!disable":#st.getfile,"!disable"
  if autoload=1 then #st.recentfile,"set":#st.filetext,"!disable":#st.getfile,"!disable"
  if autoload=2 then #st.thisfile,"set":#st.filetext,"!enable":#st.getfile,"!enable"
  #st.bkcolor,"selectindex ";bkcolor+1
  #st.fullscreen,"disable"
  tmpFullScreen=-1
  tmpConfirmOnExit=-1
  wait


[settings_nofile]
  #st.filetext,"!disable"
  #st.getfile,"!disable"
  autoload=0
  wait


[settings_recentfile]
  #st.filetext,"!disable"
  #st.getfile,"!disable"
  autoload=1
  wait


[settings_thisfile]
  #st.filetext,"!enable"
  #st.filetext,"!setfocus"
  #st.getfile,"!enable"
  autoload=2
  wait


[settings_browse]
  filedialog "Open text file for auto load",path$;"*.txt",tempload$
  if tempload$<>"" then
    autoload$=tempload$
    #st.filetext,FilenameOnly$(autoload$)
  else
    #st.nofile,"set"
    #st.filetext,"!disable"
    #st.getfile,"!disable"
    autoload=0
  end if
  wait


[settings_full_screen]
  tmpFullScreen=1
  wait


[settings_user_size]
  tmpFullScreen=0
  wait


[settings_confirm]
  tmpConfirmOnExit=1
  wait


[settings_skip_it]
  tmpConfirmOnExit=0
  wait


[settings_bkcolor]
  #st.bkcolor,"selectionindex? tmpBkColor"
  if color=tmpBkColor-1 then
    confirm "You've choosen the same background color which is set for the font color!"+chr$(13)+"Are sure you want to do this?";answer$
    if answer$="no" then #st.bkcolor,"selectindex ";bkcolor+1
  end if
  wait


[settings_okay]
  if tmpFullScreen<>-1 then fullScreen=tmpFullScreen
  if tmpConfirmOnExit<>-1 then confirmOnExit=tmpConfirmOnExit
  #st.bkcolor,"selectionindex? tmpBkColor"
  close #st
  if bkcolor<>tmpBkColor-1 then
    #jn.text,"!contents? text$"
    bkcolor=tmpBkColor-1
    close #jn
    goto [begin_program]
  end if
  wait


[settings_cancel]
  close #st
  wait

'-------------------------------------------------------------------------------------------------------[ HELP ]-------------------------------

[help_me]
  if FileExists(DefaultDir$;"\help.html") then run "explorer.exe help.html" else notice "Help";chr$(13);"The help file was not found!"
  wait


[about_it]
  notice "J-Note"+chr$(13)+"A Notepad-inspired program coded in Just BASIC by Shadowy in 2008"+chr$(13)+"Visit http://www.cybershadow.com/programs.html for updates"
  wait

'-------------------------------------------------------------------------------------------------------[ MISCELLANEOUS ]----------------------

[text_bold]
  if bold$="" then
    #jn.bold,"drawbmp bold2 -1 -1;flush"
    bold$=" Bold"
  else
    #jn.bold,"drawbmp bold1 -1 -1;flush"
    bold$=""
  end if
  gosub [set_modifiers]
  wait


[text_italic]
  if italic$="" then
    #jn.italic,"drawbmp italic2 -1 -1;flush"
    italic$=" Italic"
  else
    #jn.italic,"drawbmp italic1 -1 -1;flush"
    italic$=""
  end if
  gosub [set_modifiers]
  wait


[text_underscore]
  if underscore$="" then
    #jn.usc,"drawbmp underscore2 -1 -1;flush"
    underscore$=" Underscore"
  else
    #jn.usc,"drawbmp underscore1 -1 -1;flush"
    underscore$=""
  end if
  gosub [set_modifiers]
  wait


[set_modifiers]
  modifiers$=bold$;italic$;underscore$;strikeout$
  #jn.text,"!font ";font$(font);" ";size$(size);modifiers$
  #jn.text,"!setfocus"
  return


[text_numerize]
  #jn.text,"!selection? selected$"
  if selected$<>"" then
    #jn.text,"!cut"
    index=1
    Numbered$=""
    while word$(selected$,index,LB$)<>""
      select case numerize
        case 0:prefix$=str$(index)
        case 1:prefix$=chr$(index+64)
        case 2:prefix$=Romanize$(index)
      end select
      Numbered$=Numbered$+prefix$+". "+word$(selected$,index,LB$)+LB$ 
      index=index+1
    wend
    Numbered$=left$(Numbered$,len(Numbered$)-2)
    call InsertIntoText Numbered$
  end if
  #jn.text,"!setfocus"
  wait


[text_doterize]
  #jn.text,"!selection? selected$"
  if selected$<>"" then
    #jn.text,"!cut"
    index=1
    Dotted$=""
    while word$(selected$,index,LB$)<>""
      Dotted$=Dotted$+bullets$(dottize)+" "+word$(selected$,index,LB$)+LB$ 
      index=index+1
    wend
    Dotted$=left$(Dotted$,len(Dotted$)-2)
    call InsertIntoText Dotted$
  end if
  wait


[set_font_size]
  #jn.size,"selectionindex? size"
  size=size-1
  #jn.text,"!font ";font$(font);" ";size$(size);modifiers$
  #jn.text,"!setfocus"
  wait


[set_font_face]
  #jn.font,"selectionindex? font"
  font=font-1
  #jn.text,"!font ";font$(font);" ";size$(size);modifiers$
  #jn.text,"!setfocus"
  wait


[set_font_color]
  #jn.text,"!contents? text$"
  #jn.color,"selectionindex? temp"
  if bkcolor=temp-1 then confirm "You're about to set the font color to the same value as the background color!"+chr$(13)+"Are sure you want to do this?";answer$
  if answer$="no" then #jn.color,"selectindex ";color+1:wait
  color=temp-1
  close #jn
  goto [begin_program]


[exit_program]
  #jn.text,"!modified? changed$"
  if changed$="true" then gosub [document_save2]
  if confirmOnExit then
    confirm "You're about to exit J-Note!"+chr$(13)+"Are you sure you want to do this?";answer$
    if answer$="no" then wait
  end if
  for i=1 to 6 step 1
    unloadbmp word$("bold1 bold2 italic1 italic2 underscore1 underscore2",i)
  next i
  open DefaultDir$;"\settings.ini" for output as #st
    print #st,path$ 
    print #st,autoload$
    print #st,modifiers$
    print #st,str$(autoload)
    print #st,str$(date)
    print #st,str$(time)
    print #st,separator$
    print #st,str$(leadingZero)
    print #st,str$(font)
    print #st,str$(size)
    print #st,str$(color)
    print #st,str$(bkcolor)
    print #st,str$(confirmOnExit)
    print #st,str$(fullScreen)
    print #st,str$(numerize)
    print #st,str$(dottize)
  close #st
  close #jn
  end


[error_occured]
  notice "Attention!"+chr$(13)+"An error has occured and J-Note is forced to shut down!"+chr$(13)+"Error: "+Err$+" ("+str$(Err)+")"
  end


[do_nothing]
  wait

'-------------------------------------------------------------------------------------------------------[ FUNCTIONS AND SUBROUTINES ]----------

  function FileExists(fullPath$)
    files PathOnly$(fullPath$),FilenameOnly$(fullPath$),info$()
    FileExists=val(info$(0,0))>0
  end function


  function PathOnly$(fullPath$)
    PathOnly$=fullPath$
    while right$(PathOnly$,1)<>"\" and PathOnly$<>""
      PathOnly$=left$(PathOnly$,len(PathOnly$)-1)
    wend
  end function


  function FilenameOnly$(fullPath$)
    pathLength=len(PathOnly$(fullPath$))
    FilenameOnly$=right$(fullPath$,len(fullPath$)-pathLength)
  end function


  function SentenceCase$(txt$)
    SentenceCase$=upper$(left$(txt$,1))+lower$(mid$(txt$,2))
  end function


  function TitleCase$(txt$)
    lineNum=1
    Sentence$=word$(txt$,lineNum,LB$)
    do
      index=1
      temp$=word$(Sentence$,index)
      TitleCase$=TitleCase$+SentenceCase$(temp$)
      index=2
      temp$=word$(Sentence$,index)
      while temp$<>""
        TitleCase$=TitleCase$+" "+SentenceCase$(temp$)
        index=index+1
        temp$=word$(Sentence$,index)
      wend
      lineNum=lineNum+1
      Sentence$=word$(txt$,lineNum,LB$)
      TitleCase$=TitleCase$+LB$
    loop while Sentence$<>""
  end function


  function ToggleCase$(txt$)
    lcmin=Asc("a")
    lcmax=Asc("z")
    ucmin=Asc("A")
    ucmax=Asc("Z")
    dif=ucmax-lcmax
    for i=1 to len(txt$) step 1
      temp$=mid$(txt$,i,1)
      tempASCII=asc(temp$)
      if lcmin<=tempASCII and lcmax>=tempASCII then
        ToggleCase$=ToggleCase$+chr$(tempASCII+dif)
      else
        if ucmin<=tempASCII and ucmax>=tempASCII then
          ToggleCase$=ToggleCase$+chr$(tempASCII-dif)
        else
          ToggleCase$=ToggleCase$+temp$
        end if
      end if
    next i
  end function


  function MonthName$(month)
    MonthName$=word$("January February March April May June July August September October November December",month)
  end function


  function DayName$(day)
    DayName$=word$("Monday Tuesday Wednesday Thursday Friday Saturday Sunday",day)
  end function


  function Zero$(number)
    if number<10 then Zero$="0";str$(number) else Zero$=str$(number)
  end function


  function Size$(size)
    if size>1048576 then Size$=str$(val(using("###.#",size/1048576)));" MB"
    if size>1024 then Size$=str$(val(using("###.#",size/1024)));" kB" else Size$=str$(val(using("###.#",size)));" bytes"
  end function


  function WordCount(string$)
    while word$(string$,WordCount+1)<>""
      WordCount=WordCount+1
    wend
  end function


  function Romanize$(number)
    if number<=3999 then
      number$=str$(number)
      while len(number$)<4
        number$="0";number$
      wend
      Romanize$=word$("M MM MMM",val(mid$(number$,1,1)))
      Romanize$=Romanize$;word$("C CC CCC CD D DC DCC DCCC CM",val(mid$(number$,2,1)))
      Romanize$=Romanize$;word$("X XX XXX XL L LX LXX LXXX XC",val(mid$(number$,3,1)))
      Romanize$=Romanize$;word$("I II III IV V VI VII VIII IX",val(mid$(number$,4,1)))
    end if
  end function


  sub makeTitle titlename$
    #jn.text,"!contents? text$"
    title$=titlename$
    close #jn
  end sub


  sub MakeDateAndTime date$,time$
    year$=right$(date$,4):month=val(date$):date=val(mid$(date$,4,2))
    hour=val(time$):ampm$="am"
    if hour=0 then hour=12
    if hour>11 then ampm$="pm"
    if hour>12 then hour=hour-12
    if leadingZero then
      month$=Zero$(month)
      date$=Zero$(date)
      hour$=Zero$(hour)
    else
      month$=str$(month)
      date$=str$(date)
      hour$=str$(hour)
    end if
    dates$(0)=MonthName$(month);" ";date$;", ";year$
    dates$(1)=date$;" ";MonthName$(month);" ";year$
    dates$(2)=date$;separator$;left$(MonthName$(month),3);separator$;year$
    dates$(3)=year$;separator$;month$;separator$;date$
    dates$(4)=date$;separator$;month$;separator$;year$
    dates$(5)=month$;separator$;date$;separator$;year$
    times$(0)=hour$+mid$(time$,3,3)+ampm$
    times$(1)=hour$+right$(time$,6)+ampm$ 
    times$(2)=str$(val(time$))+mid$(time$,3,3)
    times$(3)=str$(val(time$))+right$(time$,6)
  end sub


  sub ConfirmDialog winTitle$,winText1$,winText2$,BYREF answer$
    loadbmp "warning","bmp\warning.bmp"
    WindowWidth=269:WindowHeight=105
    statictext #win.t1,winText1$,43,7,225,20
    statictext #win.t2,winText2$,43,22,185,20
    graphicbox #win.box,3,5,35,37
    button #win.default,"Yes",[confirmDialogYes],UL,3,50,80,22
    button #win.negative,"No",[confirmDialogNo],UL,90,50,80,22
    button #win.cancel,"Cancel",[confirmDialogCancel],UL,179,50,80,22
    open winTitle$ for dialog_modal as #win
    #win,"trapclose [confirmDialogCancel]"
    #win.box,"down;drawbmp warning 0 0;flush"
    answer$="cancel"
    wait
    [confirmDialogYes]
      answer$="yes"
      goto [confirmDialogCancel]
    [confirmDialogNo]
      answer$="no"
    [confirmDialogCancel]
      unloadbmp "warning"
      close #win
  end sub


  sub InformationDialog winTitle$,winTxt1$,winTxt2$,winTxt3$,winTxt4$,winTxt5$,winTxt6$,winTxt7$,winTxt8$,winTxt9$
    WindowWidth=330:WindowHeight=215
    statictext #win.txt1,winTxt1$,5,5,315,50
    statictext #win.txt2,winTxt2$,15,55,185,20
    statictext #win.txt4,winTxt4$,15,80,225,20
    statictext #win.txt6,winTxt6$,15,105,185,20
    statictext #win.txt8,winTxt8$,15,130,225,20
    statictext #win.txt3,winTxt3$,130,55,185,20
    statictext #win.txt5,winTxt5$,130,80,225,20
    statictext #win.txt7,winTxt7$,130,105,185,20
    statictext #win.txt9,winTxt9$,130,130,225,20
    button #win.default,"Close",[InformationDialogCancel],UL,125,160,80,22
    open winTitle$ for dialog_modal as #win
    #win,"trapclose [InformationDialogCancel]"
    #win,"font Times_New_Roman 10"
    wait
    [InformationDialogCancel]
    close #win
  end sub


  sub InsertIntoText string$
    #jn.hide,"!selectall"
    #jn.hide,"!cls"
    #jn.hide,string$;
    #jn.hide,"!selectall"
    #jn.hide,"!cut"
    #jn.text,"!paste"
  end sub

