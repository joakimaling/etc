' A simple player which uses the capabilities of Just Basic to play midi/wave
' samples
' 2017-06-01
nomainwin

' Set all the variables
counter = 1
isPlaying = 0
listName$ = ".\playlist.txt"
max = 25
title$ = "MWPlayer"
version$ = "0.1.0"

' Make them global and dimension the arrays
global counter, isPlaying, length, listName$, max, song$, title$, version$
dim paths$(max), songs$(max), i$(1, 1)

' Set the window size
WindowWidth = 600
WindowHeight = 425

' Centre the window on the display
UpperLeftX = int((DisplayWidth - WindowWidth) / 2)
UpperLeftY = int((DisplayHeight - WindowHeight) / 2)

' Add the controls to the window
listbox #mp.list, songs$(), mpPlay, 400, 10, 185, 250
statictext #mp.current, "00:00", 10, 280, 35, 12
statictext #mp.total, "00:00", 550, 280, 35, 12
graphicbox #mp.visual 10, 10, 380, 250
graphicbox #mp.bar 10, 270, 575, 10
button #mp.previous, "<<", mpPrevious, UL, 10, 295, 40, 22
button #mp.play, "Play", mpPlay, UL, 55, 295, 55, 22
button #mp.stop, "Stop", mpStop, UL, 115, 295, 55, 22
button #mp.next, ">>", mpNext, UL, 175, 295, 40, 22
button #mp.open, "Open", mpOpen, UL, 430, 295, 45, 22
button #mp.delete, "Delete", mpDelete, UL, 480, 295, 55, 22
button #mp.about, "About", mpAbout, UL, 540, 295, 45, 22

' Launch the window
open title$ + " " + version$ for window_nf as #mp

' Adjust some settings for the window and some of the controls
#mp.delete, "!disable"
#mp.stop, "!disable"
#mp, "font courier_new bold 8"
#mp, "trapclose mpQuit"
call mpClear

' Load the initial playlist
call mpLoad listName$
wait

' Plays the selected song in the playlist
sub mpPlay h$
    call mpStop h$
    #mp.list, "selectionIndex? song"
    song$ = paths$(song)
    if song$ = "" then
        notice "No song selected!"
        exit sub
    end if
    select case right$(song$, 4)
        case ".mid", "midi"
            playmidi song$, length
            #mp.total, getTimeString$(length)
        case ".wav"
            playwave song$
        case else
            notice "Format not supported"
            exit sub
    end select
    call togglePlayState
    timer 100, updateTime
    scan
    wait
end sub

' Stops the currently playing song
sub mpStop h$
    timer 0
    if isPlaying then
        #mp.list, "selectionIndex? song"
        select case right$(paths$(song), 4)
            case ".mid", "midi"
                stopmidi
            case ".wav"
                playwave ""
        end select
        call mpClear
        #mp.current, "00:00"
        call togglePlayState
    end if
end sub

' Skips to the previous song in the playlist
'sub mpPrevious h$
'    call mpStop h$
'    #mp.list, "selectionIndex? selected"
'    if selected <> 0 then
'        selected = selected - 1
'        if selected < 1 then
'            selected = counter - 1
'        end if
'        #mp.list, "selectionIndex "; selected
'        call mpPlay h$
'    end if
'end sub

' Skips to the next song in the playlist
'sub mpNext h$
'    call mpStop h$
'    #mp.list, "selectionIndex? selected"
'    if selected <> 0 then
'        selected = selected + 1
'        if selected > counter - 1 then
'            selected = 1
'        end if
'        #mp.list, "selectionIndex "; selected
'        call mpPlay h$
'    end if
'end sub

' Opens a single song and puts it into the playlist
sub mpOpen h$
    if counter < max then
        filedialog "Open", "*.wav;*.mid;*.midi", song$
        if song$ <> "" then
            paths$(counter) = song$
            songs$(counter) = getFileName$(paths$(counter))
            counter = counter + 1
            #mp.list, "reload"
            call mpSave listName$
        end if
    end if
end sub

' Deletes a song from the playlist
sub mpDelete h$
    #mp.list, "selectionIndex? selected"
    if selected = 0 then
        notice "No song selected!"
        exit sub
    end if
    while selected <= counter - 1
        songs$(selected) = songs$(selected + 1)
        selected = selected + 1
    wend
    counter = counter - 1
    #mp.list, "reload"
    call mpSave listName$
end sub

' Load the contents of the list file into the playlist
sub mpLoad fileName$
    if fileExists(fileName$) then
        open fileName$ for input as #ip
            while eof(#ip) = 0 and counter < max
                line input #ip, paths$(counter)
                songs$(counter) = getFileName$(paths$(counter))
                counter = counter + 1
            wend
        close #ip
        #mp.list, "reload"
    end if
end sub

' Save the contents of the playlist into the list file
sub mpSave fileName$
    open fileName$ for output as #op
        for i = 1 to counter - 1
            print #op, paths$(i)
        next
    close #op
end sub

' Clears the visual viewport and progress bar
sub mpClear
    #mp.bar "fill buttonface; flush"
    #mp.visual "fill black; flush"
end sub

' Displays a small box with information about the application
sub mpAbout h$
    notice "About " + title$ + chr$(13) + title$ + " " + version$ + chr$(13) + _
    "Created by Little Tiger"
end sub

' Stops the music and quits the application
sub mpQuit h$
    call mpStop h$
    close #mp
    end
end sub

' Toggles the play/stop buttons
sub togglePlayState
    if isPlaying then
        #mp.play, "!enable"
        #mp.stop, "!disable"
        isPlaying = 0
    else
        #mp.play, "!disable"
        #mp.stop, "!enable"
        isPlaying = 1
    end if
end sub

'
sub updateTime
    'call generateFractal
    select case right$(song$, 4)
        case ".mid", "midi"
            if midipos() <> length then
                call updateBar midipos() / length, "green"
                '#mp.current, getTime(midipos(), "midi")
            else
                isEnded = 1
            end if
        'case ".wav"
            'if wavetime <> val(getTime$(song$)) then
                'updateBar (wavetime / val(getTime$(song$))) * 100, "blue"
                'wavetime = wavetime + 0.1
                '#mp.current, getTime(wavetime, "wave")
            'else
                'isEnded = 1
            'end if
    end select
    'if isEnded then
        'call mpStop h$
    'end if
end sub

' Fills the progress bar according to given percentage and colour
sub updateBar percent, colour$
    bar = percent * 565
    #mp.bar, "down; color "; colour$; "; backcolor "; colour$
    #mp.bar, "place 0 0; boxfilled "; bar; " 10"
end sub

' Generates a cool fractal pattern in the visual box
'sub generateFractal
'    dim col$(12)
'    for c = 0 to 12
'        col$(c) = str$(int(rnd(0) * 256)) + " " + str$(int(rnd(0) * 256)) + " " + str$(int(rnd(0) * 256))
'    next
'    a = rnd(0)
'    b = 0.9998
'    c = 2 - 2 * a
'    dots = 12000
'    x = j = 0
'    y = rnd(0) * 12 + 0.1
'    for i = 0 to dots
'        scan
'        z = x
'        x = b * y + j
'        j = a * x + c * (x ^ 2) / (1 + x ^ 2)
'        y = j - z
'        xp = (x * 20) + midx
'        yp = (y * 20) + midy
'        #mp.visual, "color "; col$(i / 100)
'        #mp.visual, "set "; xp; " "; yp
'    next
'end sub

' Checks if a given file exists by trying to open it.
function fileExists(fileName$)
    files "", fileName$, i$()
    fileExists = val(i$(0, 0)) <> 0
end function

'
function getTime$(song$)
    'open song$ for input as #ip
        'data$ = input$(#ip, 40)
    'close #ip
    'size = toInt(mid$(data$, 5, 4))
    'data = instr(data$, "fmt") + 16
    'bps = toInt(mid$(data$, data, 4))
    'if right$(song$, 4) = ".wav" then
        'time = int(size / bps)
    'else
        'time = int((size / bps) * 10)
    'end if
    'getTime$ = getTimeString$(time)
end function

' Generates a time string in the format mm:ss from an integer value representing
' elapsed seconds
function getTimeString$(time)
    time$ = str$(time)
    while len(time$) < 4
        time$ = "0" + time$
    wend
    getTimeString$ = left$(time$, 2) + ":" + right$(time$, 2)
end function

'
function toInt(string$)
    for i = 0 to 3
        number = asc(mid$(string$, i + 1, 1))
        toInt = toInt + number * 256 ^ i
    next
end function

' Returns the file name from its full path
' TODO: rework the code!
function getFileName$(fullPath$)
    firstCut = 1

    for i = len(fullPath$) to 1 step -1
        if mid$(fullPath$, i, 1) = "\" then
            firstCut = i + 1
            exit for
        end if
    next

    getFileName$ = mid$(fullPath$, firstCut, len(fullPath$) - firstCut + 1)
    getFileName$ = mid$(getFileName$, 1, instr(getFileName$, ".") - 1)
end function
