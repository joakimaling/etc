
    nomainwin

    WindowWidth = 550
    WindowHeight = 410

    textbox #calc.textbox1, 5, 5, 300, 25
    button #calc.button9, "9", [button9], UL, 65, 35, 25, 25
    button #calc.button8, "8", [button8], UL, 35, 35, 25, 25
    button #calc.button7, "7", [button7], UL, 5, 35, 25, 25
    button #calc.button6, "6", [button6], UL, 65, 65, 25, 25
    button #calc.button5, "5", [button5], UL, 35, 65, 25, 25
    button #calc.button4, "4", [button4], UL, 5, 65, 25, 25
    button #calc.button3, "3", [button3], UL, 65, 95, 25, 25
    button #calc.button2, "2", [button2], UL, 35, 95, 25, 25
    button #calc.button1, "1", [button1], UL, 5, 95, 25, 25
    button #calc.button0, "0", [button0], UL, 5, 125, 25, 25
    button #calc.buttonAdd, "+", [buttonAdd], UL, 95, 125, 25, 25
    button #calc.buttonSubtract, "-", [buttonSubtract], UL, 95, 95, 25, 25
    button #calc.buttonMultiply, "*", [buttonMultiply], UL, 95, 65, 25, 25
    button #calc.buttonDivide, "/", [buttonDivide], UL, 95, 35, 25, 25
    button #calc.buttonPoint, ".", [buttonPoint], UL, 35, 125, 25, 25
    button #calc.buttonEquals, "=", [buttonEquals], UL, 65, 125, 25, 25
    open "Calculator" for window_nf as #calc
    print #calc, "font ms_sans_serif 0 16"


[calc.inputLoop]   'wait here for input event

    display$ = "0"
    gosub [displayEntry]
    overwrite = 1
    wait


[button9]   'Perform action for the button named 'button9'

    digit$ = "9"
    gosub [displayEntry]
    wait


[button8]   'Perform action for the button named 'button8'

    digit$ = "8"
    gosub [displayEntry]
    wait


[button7]   'Perform action for the button named 'button7'

    digit$ = "7"
    gosub [displayEntry]
    wait


[button6]   'Perform action for the button named 'button6'

    digit$ = "6"
    gosub [displayEntry]
    wait


[button5]   'Perform action for the button named 'button5'

    digit$ = "5"
    gosub [displayEntry]
    wait


[button4]   'Perform action for the button named 'button4'

    digit$ = "4"
    gosub [displayEntry]
    wait


[button3]   'Perform action for the button named 'button3'

    digit$ = "3"
    gosub [displayEntry]
    wait


[button2]   'Perform action for the button named 'button2'

    digit$ = "2"
    gosub [displayEntry]
    wait

[button1]   'Perform action for the button named 'button2'

    digit$ = "1"
    gosub [displayEntry]
    wait


[button0]   'Perform action for the button named 'button1'

    digit$ = "0"
    gosub [displayEntry]
    wait


[buttonAdd]   'Perform action for the button named 'buttonAdd'

    op$ = "+"
    gosub [resolvePending]
    wait


[buttonSubtract]   'Perform action for the button named 'buttonSub'

    op$ = "-"
    gosub [resolvePending]
    wait


[buttonMultiply]   'Perform action for the button named 'buttonMul'

    op$ = "*"
    gosub [resolvePending]
    wait


[buttonDivide]   'Perform action for the button named 'buttonDiv'

    op$ = "/"
    gosub [resolvePending]
    wait


[buttonPoint]   'Perform action for the button named 'buttonDecimal'

    'only allow one decimal point per entry
    if instr(display$, ".") > 0 then wait

    display$ = display$ + "."
    gosub [displayEntry]
    wait


[buttonEquals]   'Perform action for the button named 'buttonEqual'
    'Insert your own code here
    wait

[resolvePending]

    if pendingOp$ = "" then pending$ = display$
        
    if pendingOp$ = "+" then display$ = str$(val(pending$) + val(display$))
    if pendingOp$ = "-" then display$ = str$(val(pending$) - val(display$))
    if pendingOp$ = "*" then display$ = str$(val(pending$) * val(display$))
    if pendingOp$ = "/" then display$ = str$(val(pending$) / val(display$))

    pendingOp$ = op$
    overwrite = 1
    
    gosub [displayEntry]

    return

[displayEntry]

    if overwrite = 1 or display$ = "0" then
        display$ = digit$
        overwrite = 0
    else
        display$ = display$ + digit$
    end if

    print #calc.textbox1, display$
    return
