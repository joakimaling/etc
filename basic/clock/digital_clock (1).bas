'J-Clock made by Shadowy 2008
'v1.0 - 12/19/2008 - F�rsta utg�van!

'FIXAS -> F�rdr�jning d� f�nstret hamnar bakom andra f�nster och vid minimering
'         Minneshantering??
'         chime fastnar!

  global bkcolor$,fgcolor$
  dim info$(0,0),chime$(4),t$(4)'Inneh�ller tidens fyra delar
  chime$(1)="Never"
  chime$(2)="Every hour"
  chime$(3)="Every 30 minutes"
  chime$(4)="Every 15 minutes"
  WindowWidth=220:WindowHeight=105:bkcolor$="0 0 128":fgcolor$="128 255 255"
  for i=1 to 13 step 1
    temp$=word$("t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 tx ca ci",i)
    loadbmp temp$,"bmp\";temp$;".bmp"
  next i
  if fileexists("\time.txt") then
    open DefaultDir$;"\time.txt" for input as #load
      line input #load,tempstring$
      line input #load,alarm$
    close #load
  end if
  call setVariables tempstring$,alarm$
  nomainwin
  open "J-Clock v1.0" for graphics_nsb_nf as #dc
  #dc,"fill black;flush;trapclose [exit_clock]"
  #dc,"setfocus;when characterInput [keyboard]"
  #dc,"when rightButtonDown [mouse]"
  gosub [make_pictures]
  timer 500,[update_time]
  wait


[update_time]
  minutes=int(time$("seconds")/60)
  if c$="ci" then c$="ca" else c$="ci"
  if chime>0 and not(chimed) then gosub [check_chime]
  if alarmset then gosub [check_alarm]
  gosub [split_time]
  call drawClock t$(1),t$(2),c$,t$(3),t$(4),ampm$,days$,a$
  wait


'Delar upp tidsstr�ngen i mindre best�ndsdelar
[split_time]
  hours=int(minutes/60)
  minutes=minutes mod 60
  if not(mode24) then hours=english(hours) else ampm$="xx"
  time$=leading$("x",hours)+leading$("0",minutes)
  for i=1 to 4 step 1
    t$(i)="t"+mid$(time$,i,1)
  next i
  return


'Skapar bilderna till dagarna och AM/PM
[make_pictures]
  #dc,"font courier_new bold 10"
  for day=0 to 7 step 1'Dag 7 �r en bild d�r allt �r sl�ckt
    for i=0 to 6 step 1
      day$=word$("MON TUE WED THU FRI SAT SUN",i+1)
      if i=day then color$=fgcolor$ else color$=bkcolor$
      #dc,"backcolor 0 0 0;color ";color$;";place ";i*30+5;" 12;|";day$
      #dc,"getbmp d";day;" 1 1 235 15"
    next i
  next day
  #dc,"fill 0 0 0;flush;font arial bold 10"
  #dc,"backcolor 0 0 0;color ";fgcolor$;";place 5 15;|pm"
  #dc,"backcolor 0 0 0;color ";bkcolor$;";place 5 30;|am"
  #dc,"getbmp pm 1 1 27 40"
  #dc,"backcolor 0 0 0;color ";bkcolor$;";place 5 15;|pm"
  #dc,"backcolor 0 0 0;color ";fgcolor$;";place 5 30;|am"
  #dc,"getbmp am 1 1 27 40"
  #dc,"backcolor 0 0 0;color ";bkcolor$;";place 5 15;|pm"
  #dc,"backcolor 0 0 0;color ";bkcolor$;";place 5 30;|am"
  #dc,"getbmp xx 1 1 27 40"
  #dc,"fill 0 0 0;flush;font arial bold 6"
  #dc,"backcolor 0 0 0;color ";fgcolor$;";place 5 15;|alarm"
  #dc,"getbmp aa 1 1 27 20"
  #dc,"backcolor 0 0 0;color ";bkcolor$;";place 5 15;|alarm"
  #dc,"getbmp ai 1 1 27 20"
  return


'Kontrollerar om tiden st�mmer �verense med chime och ger signal
'beroende p� vilka inst�llningar som satts
[check_chime]
  if not(chimed) then
    time=minutes mod 60
    if chime=EVERY15MIN and (time=15 or time=45) then playwave "snd\chime.wav",async
    if chime>=EVERY30MIN and time=30 then playwave "snd\chime.wav",async
    if chime>=EVERYHOUR and time=0 then playwave "snd\chime.wav",async
    chimed=1
  end if
  return


'Kontrollerar om tiden st�mmer med alarmtiden och ett alarm ljuder
[check_alarm]
  if alarm=minutes then playwave "snd\alarm.wav",loop
  return


'Utf�r tangentkommandon
[keyboard]
  select case asc(Inkey$)
    case 32
      playwave ""
      if alarmset then
        alarmset=0
        a$="ai"
      end if
  end select
  wait


'�ppnar inst�llningsdialogen
[mouse]
  WindowWidth=262:WindowHeight=125
  groupbox #st,"State settings",5,5,100,90
  checkbox #st.h24,"24 hours",[use_24hours],[use_12hours],10,22,65,20
  checkbox #st.alarm,"Alarm set",[alarm_set],[alarm_off],10,42,65,20
  checkbox #st.days,"Days on",[show_days],[hide_days],10,62,65,20
  groupbox #st,"Alarm time",110,5,140,45
  textbox #st.hrs,120,22,22,20
  textbox #st.min,160,22,22,20
  statictext #st.txt,":",150,24,10,20
  statictext #st.txt,"HH : MM",195,25,50,20
  groupbox #st,"Chime times",110,50,140,45
  combobox #st.chime,chime$(),[set_chime],120,65,120,20
  open "Settings" for window_nf as #st
  #st,"trapclose [exit_subwin]"
  #st.hrs,leading$("0",int(alarm/60))
  #st.min,leading$("0",alarm mod 60)
  #st.chime,"selectindex ";chime+1
  if mode24 then #st.h24,"set"
  if showdays then #st.days,"set"
  if not(alarmset) then
    #st.hrs,"!disable"
    #st.min,"!disable"
    #st.txt,"!disable"
  else
    #st.alarm,"set"
  end if
  winopen=1
  wait


'St�nger f�nstret f�r inst�llningarna
[exit_subwin]
  #st.hrs,"!contents? hour$"
  #st.min,"!contents? minutes$"
  hrs=val(hour$)
  min=val(minutes$)
  if hrs<0 then hrs=0
  if min<0 then min=0
  if hrs>23 then hrs=23
  if min>59 then min=59
  alarm=hrs*60+min
  winopen=0
  close #st
  wait


'St�nger av ljudet, sparar inst�llningarna och avslutar
[exit_clock]
  timer 0
  playwave ""
  open DefaultDir$;"\time.txt" for output as #save
    print #save,alarmset;mode24;showdays;chime
    print #save,alarm
  close #save
  for i=1 to 13 step 1
    temp$=word$("t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 tx ca ci",i)
    unloadbmp temp$
  next i
  if winopen then close #st
  close #dc
  end


'Ritar ut hela klockan p� sk�rmen
  sub drawClock a$,b$,c$,d$,e$,f$,g$,h$
    #dc,"drawbmp ";a$;" 0 0"
    #dc,"drawbmp ";b$;" 41 0"
    #dc,"drawbmp ";c$;" 82 0"
    #dc,"drawbmp ";d$;" 103 0"
    #dc,"drawbmp ";e$;" 144 0"
    #dc,"drawbmp ";f$;" 185 0"
    #dc,"drawbmp ";h$;" 185 44"
    #dc,"drawbmp ";g$;" 0 64"
   ' #dc,"flush"
  end sub


'S�tter textstr�ngarna till globala variabler
sub setVariables tempstring$,alarm$ 
  global alarm,alarmset,mode24,showdays,chime,ampm$,days$,a$
  alarm=val(alarm$)
  alarmset=val(mid$(tempstring$,1,1))
  mode24=val(mid$(tempstring$,2,1))
  showdays=val(mid$(tempstring$,3,1))
  chime=val(mid$(tempstring$,4,1))
  if showdays then days$="d"+str$((date$("days") mod 7)+1) else days$="d7"
  if alarmset then a$="aa" else a$="ai"
  EVERYHOUR=1:EVERY30MIN=2:EVERY15MIN=3:c$="ci"
end sub


'S�tter ut ett mellanrum eller nolla framf�r tiden om den �r < 10
  function leading$(x$,time)
    if time<10 then leading$=x$+str$(time) else leading$=str$(time)
  end function


'G�r om timmarna fr�n 24 till 12 format
  function english(time)
    ampm$="am"
    if time=0 then time=12
    if time>11 then ampm$="pm"
    if time>12 then time=time-12
    english=time
  end function


'Returnerar sant om filen existerar
function fileexists(file$)
  files DefaultDir$,file$,info$()
  fileexists=val(info$(0,0))
end function


[alarm_set]
  #st.hrs,"!enable"
  #st.min,"!enable"
  #st.txt,"!enable"
  alarmset=1
  a$="aa"
  wait


[alarm_off]
  #st.hrs,"!disable"
  #st.min,"!disable"
  #st.txt,"!disable"
  alarmset=0
  a$="ai"
  wait


[use_24hours]
  mode24=1
  wait


[use_12hours]
  mode24=0
  wait


[show_days]
  days$="d"+str$((date$("days") mod 7)+1)
  showdays=1
  wait


[hide_days]
  days$="d7"
  showdays=0
  wait


[set_chime]
  #st.chime,"selectionindex? temp"
  chime=temp-1
  wait
