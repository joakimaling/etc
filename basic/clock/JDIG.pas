{ 07-04-07 17:34 �ling Programmes! }

unit JDIG;

interface

  const mid=0;rtl=1;ltr=2;

 procedure graphcheck;
 function dig14bin(letter:char):string;
 procedure dig14digit(seg:string;x,y,c,size:integer);
 procedure dig14(seg:string;dir,size:integer);
 function dig7bin(letter:char):string;
 procedure dig7digit(seg:string;x,y,c,size:integer);
 procedure dig7(seg:string;dir,size:integer);
 procedure colon(size:integer);

implementation

uses Graph,Crt;

procedure graphcheck;
  var mode,driver:smallint; {integer/smallint}
  begin
    driver:=Detect;
    InitGraph(driver,mode,'');
    if GraphResult<>GrOk then begin
      GotoXY(20,15);
      Write('Error in ',GraphErrorMsg(GraphResult));
      Halt
    end
  end;

function dig14bin(letter:Char):string;
  var bin:string[14];
  begin
    case letter of
      'a','A':bin:='11111000110000';
          'B':bin:='10101111010000';
          'b':bin:='01011100110000';
          'C':bin:='11010100000000';
          'c':bin:='00010100110000';
          'D':bin:='10101111000000';
          'd':bin:='00111100110000';
      'e','E':bin:='11010100100000';
      'f','F':bin:='11010000100000';
      'g','G':bin:='11011100010000';
          'H':bin:='01111000110000';
          'h':bin:='01011000110000';
          'I':bin:='10000111000000';
          'i':bin:='00000101110000';
      'j','J':bin:='00101100000010';
      'k','K':bin:='01010000100101';
          'L':bin:='01010100000000';
          'l':bin:='00000011000000';
          'M':bin:='01111000001100';
          'm':bin:='00011001110000';
          'N':bin:='01111000001001';
          'n':bin:='00011000110000';
          'O':bin:='11111100000000';
          'o':bin:='00011100110000';
      'p','P':bin:='11110000110000';
          'Q':bin:='11111100000001';
          'q':bin:='00011100110001';
          'R':bin:='11110000110001';
          'r':bin:='00010000110000';
      's','S':bin:='11001100110000';
          'T':bin:='11100011000000';
          't':bin:='00000011110000';
          'U':bin:='01111100000000';
          'u':bin:='00011100000000';
      'v','V':bin:='01010000000110';
          'W':bin:='01111000000011';
          'w':bin:='00011101000000';
      'x','X':bin:='00000000001111';
      'y','Y':bin:='00000001001100';
      'z','Z':bin:='10000100000110';
      '�','�':bin:='10011100110000';
{byt ">c�}'"':bin:='00010100110100';
{byt !>c^}'!':bin:='00010100111100';
          '0':bin:='11111100000110';
          '1':bin:='00101000000100';
          '2':bin:='10110100110000';
          '3':bin:='10101100010000';
          '4':bin:='01101000110000';
          '5':bin:='11001100110000';
          '6':bin:='11011100110000';
          '7':bin:='10000000000110';
          '8':bin:='11111100110000';
          '9':bin:='11101100110000';
      '(','<':bin:='00000000000101';
      ')','>':bin:='00000000001010';
         '''':bin:='00000010000000';
          '+':bin:='00000011110000';
          '-':bin:='00000000110000';
          '*':bin:='00000011111111';
          '/':bin:='00000000000110';
          '\':bin:='00000000001001';
          '%':bin:='01001000000110';
          '$':bin:='11001111110000';
          '?':bin:='11100001010000';
          '_':bin:='00000100000000'
        else  bin:='00000000000000'
    end;
    dig14bin:=bin
  end;

procedure dig14digit(seg:string;x,y,c,size:integer);
  type lights=array[1..14] of integer;
  var number:byte;
       diod:lights;
  begin
    for number:=1 to Length(seg) do begin
      if seg[number]='1' then
        diod[number]:=c+8   {  light colour, active }
      else
        diod[number]:=c     { dark colour, inactive }
    end;
{ 1: upper line }
    SetColor(diod[1]);
    Line(x-2*size+1,y-3*size,x+2*size-1,y-3*size);
{ 2: upper left line }
    SetColor(diod[2]);
    Line(x-2*size,y+1,x-2*size,y-3*size);
{ 3: upper right line }
    SetColor(diod[3]);
    Line(x+2*size,y+1,x+2*size,y-3*size);
{ 4: lower left line }
    SetColor(diod[4]);
    Line(x-2*size,y+1,x-2*size,y+3*size);
{ 5: lower right line }
    SetColor(diod[5]);
    Line(x+2*size,y,x+2*size,y+3*size);
{ 6: lower line }
    SetColor(diod[6]);
    Line(x-2*size+1,y+3*size,x+2*size-1,y+3*size);
{ 7: upper middle line }
    SetColor(diod[7]);
    Line(x,y-1,x,y-3*size+1);
{ 8: lower middle line }
    SetColor(diod[8]);
    Line(x,y+1,x,y+3*size-1);
{ 9: left middle line }
    SetColor(diod[9]);
    Line(x,y,x-2*size+1,y);
{ 10: right middle line }
    SetColor(diod[10]);
    Line(x,y,x+2*size-1,y);
{ 11: upper left cross }
    SetColor(diod[11]);
    Line(x-1,y-1,x-2*size+1,y-3*size+1);
{ 12: upper right cross }
    SetColor(diod[12]);
    Line(x+1,y-1,x+2*size-1,y-3*size+1);
{ 13: lower left cross }
    SetColor(diod[13]);
    Line(x-1,y+1,x-2*size+1,y+3*size-1);
{ 14: lower right cross }
    SetColor(diod[14]);
    Line(x+1,y+1,x+2*size-1,y+3*size-1)
  end;

procedure dig14(seg:string;dir,size:integer);
  var x,y,i,c:integer;
  begin
    c:=GetColor;
    if c=yellow then c:=brown;
    if c=white then c:=7;
    y:=GetY;
    x:=GetX;
    case dir of
      ltr:begin
            for i:=1 to Length(seg) do begin
              dig14digit(dig14bin(seg[i]),x,y,c,size);
              x:=x+7*size
            end
          end;
      mid:begin
            x:=x-((Length(seg)*7*size)div 2);
            for i:=1 to Length(seg) do begin
              dig14digit(dig14bin(seg[i]),x,y,c,size);
              x:=x+7*size
            end
          end;
      rtl:begin
            for i:=Length(seg) downto 1 do begin
              dig14digit(dig14bin(seg[i]),x,y,c,size);
              x:=x-7*size
            end
          end
    end
  end;

function dig7bin(letter:char):string;
  var bin:string[7];
  begin
    case letter of
      'a','A':bin:='1111101';   {  1  }
      'b','B':bin:='0101111';   { 2 3 }
          'c':bin:='0001011';   {  7  }
          'C':bin:='1101010';   { 4 5 }
      'd','D':bin:='0011111';   {  6  }
      'e','E':bin:='1101011';
      'f','F':bin:='1101001';
          'h':bin:='0101101';
          'H':bin:='0111101';
      'j','J':bin:='0011110';
      'l','L':bin:='0101010';
      'n','N':bin:='0001101';
          'o':bin:='0001111';
          'O':bin:='1111110';
      'p','P':bin:='1111001';
      'r','R':bin:='0001001';
      's','S':bin:='1100111';
      't','T':bin:='0101011';
          'u':bin:='0001110';
          'U':bin:='0111110';
      'y','Y':bin:='0110111';
      'z','Z':bin:='1011011';
      '�','�':bin:='1001111';
          '0':bin:='1111110';
          '1':bin:='0010100';
          '2':bin:='1011011';
          '3':bin:='1010111';
          '4':bin:='0110101';
          '5':bin:='1100111';
          '6':bin:='1101111';
          '7':bin:='1010100';
          '8':bin:='1111111';
          '9':bin:='1110111';
          '-':bin:='0000001';
          '_':bin:='0000010'
        else  bin:='0000000'
    end;
    dig7bin:=bin
  end;

procedure dig7digit(seg:string;x,y,c,size:integer);
  type lights=array[1..7] of integer;
  var number:byte;
       diod:lights;
  begin
    for number:=1 to Length(seg) do begin
      if seg[number]='1' then
        diod[number]:=c+8   {  light colour, active }
      else
        diod[number]:=c     { dark colour, inactive }
    end;
{ 1: upper line }
    SetColor(diod[1]);
    Line(x-2*size+1,y-3*size,x+2*size-1,y-3*size);
{ 2: upper left line }
    SetColor(diod[2]);
    Line(x-2*size,y+1,x-2*size,y-3*size);
{ 3: upper right line }
    SetColor(diod[3]);
    Line(x+2*size,y+1,x+2*size,y-3*size);
{ 4: lower left line }
    SetColor(diod[4]);
    Line(x-2*size,y+1,x-2*size,y+3*size);
{ 5: lower right line }
    SetColor(diod[5]);
    Line(x+2*size,y,x+2*size,y+3*size);
{ 6: lower line }
    SetColor(diod[6]);
    Line(x-2*size+1,y+3*size,x+2*size-1,y+3*size);
{ 7: middle line }
    SetColor(diod[7]);
    Line(x-2*size+1,y,x+2*size-1,y)
  end;

procedure dig7(seg:string;dir,size:integer);
  var x,y,i,c:integer;
  begin
    c:=GetColor;
    if c=yellow then c:=brown;
    if c=white then c:=7;
    y:=GetY;
    x:=GetX;
    case dir of
      ltr:begin
            for i:=1 to Length(seg) do begin
              dig7digit(dig7bin(seg[i]),x,y,c,size);
              x:=x+7*size
            end
          end;
      mid:begin
            x:=x-((Length(seg)*7*size)div 2);
            for i:=1 to Length(seg) do begin
              dig7digit(dig7bin(seg[i]),x,y,c,size);
              x:=x+7*size
            end
          end;
      rtl:begin
            for i:=Length(seg) downto 1 do begin
              dig7digit(dig7bin(seg[i]),x,y,c,size);
              x:=x-7*size
            end
          end
    end
  end;

procedure colon(size:integer);
  var x,y:integer;
  begin
    x:=GetX;
    y:=GetY;
    Circle(x,y-1*size,size);
    Circle(x,y+1*size,size)
  end;

end.

