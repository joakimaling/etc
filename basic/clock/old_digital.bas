  'Digital Clock made by Shadowy
  'Last update: 2/4/2007 11:37 pm
  'v1.17 - 2/01/2007 - Fixed bug, Now pressing snooze wont move altime$ just postpone the signal (ringtime$) ten min.
  'v1.16 - 1/30/2007 - Gave alarm function a memory
  'v1.15 - 1/29/2007 - Date function added
  'v1.14 - 1/25/2007 - Snooze function added, more code shrinking
  'v1.13 - 1/24/2007 - Alarm function with sound added
  'v1.03 - 1/21/2007 - Added seconds to 24hrs clock, code shrinked
  'v1.02 - 1/20/2007 - Sized the digits to fit properly, got rid of the graphicbox that wasen't needed
  'v1.01 - 1/19/2007 - Added 12/24hrs function
  'v1.00 - 1/19/2007 - First release, simple 24hrs clock
  '----Controls using the clock----
  'Shift - Switches between 12/24hrs mode
  'Ctrl  - Displays current date
  'Space - Snooze (the alarm will stop for ten min)
  'Alt   - Stop the alarm (if not reseted, the alarm will go on next time it strucks the set alarm time)
  'Q     - Set the alarm
  'W     - Reset the alarm
  'E     - Display the alarm time
  ver$="v1.17"
  for i=0 to 9
    loadbmp "c";i,"clock\c";i;".bmp"
  next i
  for i=0 to 9
    loadbmp "s";i,"clock\s";i;".bmp"
  next i
  loadbmp "co","clock\colon.bmp"
  loadbmp "sp","clock\space.bmp"
  loadbmp "sl","clock\slash.bmp"
  loadbmp "al","clock\alarm.bmp"
  loadbmp "am","clock\am.bmp"
  loadbmp "pm","clock\pm.bmp"
  loadbmp "sw","clock\sw.bmp"
  open DefaultDir$;"\time.txt" for input as #i
  line input #i,altime$
  close #i
  if altime$="" then altime$="00:00"
  ringtime$=altime$
  nomainwin
  WindowWidth=244
  WindowHeight=93
  UpperLeftX=int((DisplayWidth-WindowWidth)/2)
  UpperLeftY=int((DisplayHeight-WindowHeight)/2)
  open "Digital Clock ";ver$ for graphics_nf_nsb as #clock
  print #clock,"trapclose [quit];fill black;flush"
  print #clock,"setfocus;when characterInput [temp]"
  al$="sp"
  eng=1
  timer 500,[make]
  wait
[make]
  time$=time$()
  if cs$="co" then cs$="sp" else cs$="co"
  m1$=mid$(time$,4,1)
  m2$=mid$(time$,5,1)
  s1$=mid$(time$,7,1)
  s2$=mid$(time$,8,1)
  if eng then
    h1$=mid$(eng$(time$),1,1)
    h2$=mid$(eng$(time$),2,1)
    end$=right$(eng$(time$),2)
    call draw al$,h1$,h2$,cs$,m1$,m2$,end$,""
  else
    h1$=mid$(time$,1,1)
    h2$=mid$(time$,2,1)
    call draw al$,h1$,h2$,cs$,m1$,m2$,"s";s1$,s2$
  end if
  gosub [check]
  wait
[quit]
  timer 0
  playwave ""
  close #clock
  end
[temp]
  select case asc(Inkey$)
    case 4                            '(Shift) Switch between 12/24hrs mode
      if eng then eng=0 else eng=1
    case 113                          '(q) Set the alarm time
      prompt "Input alarm time (hh:mm)";altime$
      open "time.txt" for output as #o
      print #o,altime$
      close #o
      ringtime$=altime$
      alset=1
      al$="al"
    case 119                          '(w) Reset the alarm
      alset=0
      al$="sp"
    case 101                          '(e) Display the alarm time for a while
      c$=mid$(altime$,4,1)
      d$=mid$(altime$,5,1)
      if eng then
        a$=mid$(eng$(altime$),1,1)
        b$=mid$(eng$(altime$),2,1)
        g$=right$(eng$(altime$),2)
        call draw al$,a$,b$,"co",c$,d$,g$,""
      else
        a$=mid$(altime$,1,1)
        b$=mid$(altime$,2,1)
        call draw al$,a$,b$,"co",c$,d$,"s0","0"
      end if
      call delay 3000
    case 32                           '(Space) Snooze the alarm
      playwave ""
      h=val(ringtime$)
      m=val(mid$(ringtime$,4,2))
      m=m+10
      if m>=60 then
        m=m-60
        h=h+1
      end if
      if h>=24 then h=0
      ringtime$=zero$(h)+":"+zero$(m)
    case 16                           '(Alt) Stop alarm
      playwave ""
      ringtime$=altime$
    case 8                            '(Ctrl) Display date for a while
      date$=date$("mm/dd/yy")
      a$=mid$(date$,1,1)
      b$=mid$(date$,2,1)
      c$=mid$(date$,4,1)
      d$=mid$(date$,5,1)
      e$=mid$(date$,7,1)
      f$=mid$(date$,8,1)
      if eng then
        call draw al$,a$,b$,"sl",c$,d$,"s"+e$,f$
      else
        call draw al$,c$,d$,"sl",a$,b$,"s"+e$,f$
      end if
      call delay 3000
  end select
  goto [make]
[check]
  if alset then
    if ringtime$+":00"=time$ then playwave "clock\alarm.wav",loop
  end if
  return

sub draw a$,b$,c$,d$,e$,f$,g$,h$
  print #clock,"drawbmp ";a$;" 0 0"
  print #clock,"drawbmp c";b$;" 20 0"
  print #clock,"drawbmp c";c$;" 59 0"
  print #clock,"drawbmp ";d$;" 98 0"
  print #clock,"drawbmp c";e$;" 118 0"
  print #clock,"drawbmp c";f$;" 157 0"
  print #clock,"drawbmp ";g$;" 196 0"
  if h$<>"" then print #clock,"drawbmp s";h$;" 216 0"
end sub

sub delay ms
  for i=1 to ms*120:next i '120 loops per ms. Depends on system features. Works good on my computer (2.1GHz CPU)
end sub

'sub web
'  run "explorer.exe ";chr$(34);"http://www.cyber.se"
'end sub

function eng$(hrs$)
  end$="am"
  hrs=val(hrs$)
  if hrs>11 then end$="pm"
  if hrs>12 then hrs=hrs-12
  if hrs=0 then hrs=12
  eng$=zero$(hrs)+end$
end function

function zero$(i)
  if i<10 then i$="0"+str$(i) else i$=str$(i)
  zero$=i$ 
end function

















