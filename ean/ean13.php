<?php

class EAN13 {
	private const FONT = './arial.ttf';
	private const HEIGHT = 78;
	private const WIDTH = 115;

	private $codes = [
		'1110010', '1100110', '1101100', '1000010', '1011100', '1001110',
		'1010000', '1000100', '1001000', '1110100'
	];

	private $groups = [
		'LLLLLL', 'LLGLGG', 'LLGGLG', 'LLGGGL', 'LGLLGG', 'LGGLLG', 'LGGGLL',
		'LGLGLG', 'LGLGGL', 'LGGLGL'
	];

	private $x = 11;
	private $image;

	/**
	 * Creates an image and stores it in the local variable.
	 */
	public function __construct() {
		$this->image = imagecreatetruecolor(self::WIDTH, self::HEIGHT);
	}

	/**
	 * Destroys the image, freeing up memory.
	 */
	public function __destruct() {
		imagedestroy($this->image);
	}

	/**
	 * Helper function which draws a line of given length at given position.
	 *
	 * @param number $x      Position on the X-axis
	 * @param number $length Length in pixels
	 */
	private function drawLine($x, $length) {
		imageline($this->image, $x, 0, $x, $length, 0x000000);
	}

	/**
	 * Helper function which draws given text at given coordinates.
	 *
	 * @param number $x     Position on the X-axis
	 * @param number $y     Position on the Y-axis
	 * @param string $digit Digit to be drawn
	 */
	private function drawText($x, $y, $digit) {
		imagettftext($this->image, 8, 0, $x, $y, 0x000000, self::FONT, $digit);
	}

	/**
	 * Produces a marker of given sequence.
	 *
	 * @param string $code Code indicating the style of the marker
	 */
	private function putMarker($code) {
		foreach (str_split($code) as $bit) {
			$bit == '1' && $this->drawLine($this->x, self::HEIGHT - 7);
			$this->x++;
		}
	}

	/**
	 * Draws the barcode to the image. Produces a PNG with given number as name.
	 *
	 * @param string $number Number, not longer than 13 digits.
	 */
	public function draw($number) {
		$digit = (int) $number[0];
		$group = $this->groups[$digit];
		$w = 0xFFFFFF;

		imagefilledrectangle($this->image, 0, 0, self::WIDTH, self::HEIGHT, $w);

		// Draw the first control digit
		if ($digit > 0) {
			$this->drawText($this->x - 7, self::HEIGHT - 1, $digit);
		}

		$this->putMarker('101');

		foreach (str_split(substr($number, 1)) as $key => $value) {
			$select = $key < 6 ? $group[$key] : 'R';
			$code = $this->codes[(int) $value];

			if ($select == 'G') {
				$code = strrev($code);
			}

			// Draw the central marker
			if ($key == 6) {
				$this->putMarker('01010');
			}

			$this->drawText($this->x + ($key < 6), self::HEIGHT - 1, $value);

			foreach (str_split($code) as $bit) {
				if ($select == 'L') {
					$bit = $bit === '0' ? '1' : '0';
				}

				$bit == '1' && $this->drawLine($this->x, self::HEIGHT - 12);
				$this->x++;
			}
		}

		$this->putMarker('101');

		// Produce the image
		imagepng($this->image, $number.'.png');
	}
}

header('Content-Type: image/png');
(new EAN13())->draw(substr(sprintf('%013d', $argc > 1 ? $argv[1] : 0), 0, 13));
