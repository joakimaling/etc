<?php

require 'kawaii_item.class.php';

class Kawaii {

	/***/
	protected $connection = null;

	/***/
	protected $type = '';

	/***/
	protected $select = null;

	/***/
	protected $from = null;

	/***/
	protected $where = null;

	/***/
	protected $limit = null;

	/***/
	public function __construct($hostname, $username, $password, $database = '', $charset = 'utf8', $port = 3306) {
		$this->connection = new mysqli($hostname, $username, $password, $database, $port);
		$this->connection->set_charset($charset);
	}

	/**
	 * Closes the connection properly
	 */
	public function __destruct() {
		$this->connection->close();
	}

	/**
	 * Prints the query string. Used internally as well as for debugging
	 * purposes.
	 *
	 * @return string
	 */
	public function __toString() {
		$query = '';

		switch($this->type) {
			case 'select':
				$query .= (string) $this->select;
				$query .= (string) $this->from;

				if($this->where) {
					$query .= (string) $this->where;
				}

				if($this->limit) {
					$query .= (string) $this->limit;
				}
		}

		return $query;
	}

	/**
	 * Clears the currently stored query making room for making a new one.
	 */
	protected function clear() {
		$this->type = '';
		$this->select = null;
		$this->from = null;
		$this->where = null;
		$this->limit = null;
	}

	/***/
	protected function escape($string) {
		return $this->connection->real_escape_string($string);
	}

	/***/
	protected function run() {
		return $this->connection->query($this->escape($this));
	}

	/***/
	public function get() {
		$result = $this->run();
		$rows = [];

		$this->clear();

		if($result) {
			while($row = mysqli_fetch_object($result)) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	/**
	 * Returns the resulting row indicated by $row.
	 *
	 * @param  integer $row The row number to return
	 * @return object
	 */
	public function one($row = 0) {
		return $this->get()[$row];
	}

	/**
	 * Produces (and stores internally) the SELECT portion of the query.
	 *
	 * @param  mixed   $columns The columns in the form of a string or array
	 * @return Kawaii
	 */
	public function select($columns = '*') {
		$this->type = 'select';

		if(is_null($this->select)) {
			$this->select = new KawaiiItem('SELECT', $columns);
		}
		else {
			$this->select->add($columns);
		}

		return $this;
	}

	/**
	 * Produces (and stores internally) the FROM portion of the query.
	 *
	 * @param  mixed   $table Name of a table or a sub query
	 * @param  string  $alias An alias if $table is a sub query
	 * @return Kawaii
	 */
	public function from($table, $alias = '') {

		if(is_null($this->from)) {
			if($table instanceof $this) {

				if(empty($alias)) {
					throw new Exception('Sub-queries must have an alias');
				}

				$table = '(' . (string) $table . ') AS ' . $alias;
			}

			$this->from = new KawaiiItem('FROM', $table);
		}
		else {
			$this->from->add($table);
		}

		return $this;
	}

	/***/
	public function where($conditions, $glue = 'AND') {
		if(is_array($conditions)) {
			foreach($conditions as $key => &$value) {
				$value = $key . ' = ' . $value;
			}

			$conditions = implode(' ' . $glue . ' ', $conditions);
		}

		if(is_null($this->where)) {
			$this->where = new KawaiiItem('WHERE', $conditions, ' ' . $glue . ' ');
		}
		else {
			$this->where->add($conditions);
		}

		return $this;
	}

	/***/
	public function whereOr($conditions) {
		return $this->where($conditions, 'OR');
	}

	/***/
	public function whereIn($column, array $values, $glue = 'AND') {
		return $this->where($column . ' IN (' . implode(', ', $values) . ')', $glue);
	}

	/**
	 * Produces (and stores internally) the LIMIT portion of the query.
	 *
	 * @param  integer $limit  How many rows to limit the result
	 * @param  integer $offset From which row to start counting
	 * @return Kawaii
	 */
	public function limit($limit, $offset = 0) {
		$this->limit = new KawaiiItem('LIMIT', $offset);
		$this->limit->add($limit);

		return $this;
	}
}
