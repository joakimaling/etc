<?php

/***/
class KawaiiItem {

	/***/
	protected $type = '';

	/***/
	protected $items = [];

	/***/
	protected $glue = '';

	/***/
	public function __construct($type, $items, $glue = ', ') {
		$this->type = $type;
		$this->add($items);
		$this->glue = $glue;
	}

	/***/
	public function __toString() {
		return $this->type . ' ' . implode($this->glue, $this->items) . ' ';
	}

	/***/
	public function add($items) {
		$items = is_array($items) ? $items : array($items);
		$this->items = array_merge($this->items, $items);
	}

	/***/
	protected function wrap($items) {
		foreach($items as $key => &$item) {
			if(!is_numeric($item)) {
				$item = $this->enclose($item);
			}
		}

		return $items;
	}

	/***/
	protected function encloseOperators($item) {
		foreach(['=', '!=', '<>', '<=', '>=', '<', '>', 'AS', 'AND', 'OR'] as $operator) {
			$item = str_replace(' ' . $operator . ' ', '` ' . $operator . ' `', $item);
		}

		return $item;
	}

	/***/
	protected function enclose($item) {
		return '`' . str_replace('.', '`.`', $item) . '`';
	}

	/***/
	protected function quote($text) {
		return is_string($text) ? '\'' . $text . '\'' : $text;
	}
}
