<?php
/**
 * Kawaii Framework
 *
 * An open source application development framework. Designed to simplify repeated tasks
 * in creating web applications.
 *
 * @package   Kawaii
 * @author    Joakim Åling <joakim.aling@gmail.com>
 * @copyright [description]
 * @license   [url] [description]
 * @link      (target, link)
 * @since     0.0.1
 */

/**
 * APPLICATION ENVIRONMENT
 *
 * This constant is used to load different configurations depending on the current
 * environment. This, among other things, influences the error handling.
 * Default usage is:
 *
 *	development
 *	testing
 *	production
 */
define('ENVIRONMENT', 'development');

/**
 * APPLICATION DATABASE
 *
 * Depending on the location of this set-up, different databases may be used.
 * Default usage is:
 *
 *	local
 *	live
 */
define('DATABASE', 'local');

/**
 * ERROR HANDLING
 *
 * Depending on the environment different levels of error handling is used. During
 * development all errors will be displayed while at other times they will be hidden and
 * some of them logged.
 */
switch(ENVIRONMENT) {
	case 'development':
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
		break;

	case 'production':
	case 'testing':
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPECATED & ~E_STRICT);
		ini_set('display_errors', '0');
		break;

	default:

		break;
}

/**
 * SYSTEM PATH
 *
 * The path to the folder containing the "system" files.
 */
$system_path = 'system';

/**
 * APPLICATION PATH
 *
 * The folder where the front controller functions are residing in.
 */
$application_path = 'application';

/**
 * PATH CONSTANTS
 *
 */
define('BASEPATH', $system_path);

define('MAINPATH', $application_path . '/');

/**
 * LOAD THE BOOTSTRAP FILE
 *
 * This file will bootstrap the entire application and run it.
 */
require_once BASEPATH . 'core/start.php';

?>