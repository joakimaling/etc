# What is Kawaii?

Kawaii is a lightweight application development framework, written in PHP, designed to ease and speed up development of projects on the web. It is designed firstly for my own needs, but I have decided to share it with the world as it is.

## Requirements

- PHP 5.3
- MySQL 5.5

## Licence

This code is released under the ...

## Coding conventions

I always use UK English (in order to be consistent) for naming of variables, constants, functions, classes and in comments. User strings may be in any language.

### Constants
Written in all capitals and no underscore in between words. Use one word, if possible.

### Variables
Written using one word where possible. If it contains more than one word then camel-casing is used.

### Class names
Camel-casing with initial letter capitalised

### Comments
follow the same indentation as the statement below it. Comment must not be longer than 90 characters.
Tags such as TODO, FIXME, DEPRECATED will be used throughout the comments to indicate code to be added, fixed or removed.