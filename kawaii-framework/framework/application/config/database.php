<?php
/**
 * DATABASE CONFIGURATION
 *
 *
 */
return array(
	'local' => array(
		'hostname' => 'localhost',
		'username' => 'root',
		'password' => '',
		'database' => 'kawaii',
		'charset' => 'utf8',
		'collate' => 'utf8_default_ci',
		'port' => '3306'
	),
	'live' => array(
		'hostname' => 'localhost',
		'username' => 'root',
		'password' => '',
		'database' => 'kawaii',
		'charset' => 'utf8',
		'collate' => 'utf8_default_ci',
		'port' => '3306'
	)
);

?>