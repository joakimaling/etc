<?php
/**
 * CONFIGURATION SETTINGS FOR DEVELOPMENT
 */
defined(BASEPATH) or die('No direct script access allowed');

/**
 * DEFAULT LANGUAGE
 */
$config['language'] = 'en_GB';
$config['charset'] = 'UTF-8';

setlocale(LC_ALL, $config['language'] . '.' . $config['charset']);

?>