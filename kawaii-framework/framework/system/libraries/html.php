<?php
/**
 *
 */
defined(BASEPATH) or die('No direct script access allowed');

class Html {

	/**
	 * [nav description]
	 *
	 * @param  array  $items      A nested array of title-url-pairs
	 * @param  string $active     The name of the active (selected) value
	 * @param  array  $attributes Attributes for the top UL tag
	 * @return string             A nested navbar
	 */
	public function nav($items, $active, $attributes = array()) {
		$result = '';

		foreach($items as $key => $value) {
			if(is_array($value)) {
				$class = ($selected == $value or in_array_r($selected, $value)) ? array('class' => 'selected') : '';
				$result .= $this->tag('li', '', nav($value, $current));
			else {
				$class = ($selected == $value) ? array('class' => 'active') : '';
				$result .= $this->tag('li', $class, $this->l($value, $key)));
			}

		}

		return $this->tag('nav', $attributes, $result);
	}

	/**
	 * [ul description]
	 *
	 * @param  array  $items      [description]
	 * @param  array  $attributes [description]
	 * @return string             [description]
	 */
	public function ul($items, $attributes = array()) {
		return do_list('ul', $items, $attributes);
	}

	/**
	 * [ol description]
	 *
	 * @param  array  $items      [description]
	 * @param  array  $attributes [description]
	 * @return string             [description]
	 */
	public function ol($items, $attributes = array()) {
		return do_list('ol', $items, $attributes);
	}

	/**
	 * Produces a multi-level list.
	 *
	 * @param  string $type       The type of tag being used (UL or OL)
	 * @param  array  $items      A multi-dimensional array with the items of the list
	 * @param  array  $attributes Optional attributes
	 * @return string             The generated list
	 */
	protected function do_list($type, $items, $attributes = array()) {
		$result = '';

		foreach($items as $item) {
			if(is_array($item)) {
				$result .= $this->tag('li', '', do_list($type, $item));
			}
			else {
				$result .= $this->tag('li', '', $item);
			}
		}

		return $this->tag($type, $attributes, $result);
	}

	/**
	 * Produces a link tag with optional name and attributes. If a title is not provided
	 * the link receives the URL as the title.
	 *
	 * @param  string $url        The location link
	 * @param  string $name      The title of the link tag
	 * @param  array  $attributes Optional attributes
	 * @return string             The generated link tag
	 */
	public function l($url, $name = '', $attributes = array()) {
		!empty($name) or $name = $url;
		$attributes['href'] = $url;

		return $this->tag('a', $attributes, $name);
	}


	/**
	 * Produces a HTML tag with provided attributes and optionally a value. The value
	 * will be put in the attributes list of a void tag overwriting any value attribute
	 * otherwise it will be put in between the opening and closing tags.
	 *
	 * @param  string $name       The name of the tag
	 * @param  array  $attributes An array with the tag attributes
	 * @param  string $value      The value, this can be a nested tag clause also
	 * @return string             The generated tag
	 */
	public function tag($name, $attributes = array(), $value = '') {

		if(!in_array($name, array('area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'link', 'meta', 'param', 'source'))) {
			return '<' . $name . attributes($attributes) . '>' . $value . '</' . $name . '>';
		}

		empty($value) or $attributes['value'] = $value;

		return '<' . $name . attributes($attributes) . ' />';
	}

	/**
	 * Produces a formatted string with attributes from an array.
	 *
	 * @param  array  $attributes The attributes in the form of an array
	 * @return string             The formatted string with attributes
	 */
	protected function attributes($attributes = array()) {

		if(is_array($attributes)) {

			$result = '';
			foreach($attributes as $key => $value) {
				$result .= ' ' . (is_numeric($key) ? $value : $key . '="' . $value . '"');
			}
			return $result;
		}

		return $attributes;
	}

	/**
	 * [clean description]
	 *
	 * @param  string $value [description]
	 * @return string        [description]
	 */
	public function clean($value) {
		return htmlentities($value);
	}
}

?>