<?php
/**
 *
 */
defined(BASEPATH) or die('No direct script access allowed');

class Log {

	/**
	 * [write description]
	 * @param  string $message [description]
	 * @param  string $type    [description]
	 * @return [type]          [description]
	 */
	public function write($message, $type = 'error') {

		$file = MAINPATH . '/logs/kawaii.log';

		if($fp = fopen($file, FOPEN_WRITE_CREATE)) {

			$message = date($this->format) . ' ' . $type . ' ' . $message;

			flock($fp, LOCK_EX);
			fwrite($fp, $message);
			flock($fp, LOCK_UN);
			fclose($fp);

			return true;
		}

		return false;
	}
}

?>