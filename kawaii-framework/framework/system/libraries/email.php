<?php
/**
 *
 */
defined(BASEPATH) or die('No direct script access allowed');

class Email {

	/**
	 * [$headers description]
	 * @var array
	 */
	protected $headers = array();

	/**
	 * [$recipients description]
	 * @var array
	 */
	protected $recipients = array();

	/**
	 * [$subject description]
	 * @var string
	 */
	protected $subject = '';

	/**
	 * [$body description]
	 * @var string
	 */
	protected $body = '';

	/**
	 * [__construct description]
	 */
	public function __construct() {

	}

	/**
	 * Sets the sender of the mail.
	 * @param string $from The email of the sender
	 * @param string $name The name of the sender
	 */
	public function from($from, $name = '') {
		$this->setHeader('From', $name . '<' . $from . '>');
	}

	/**
	 * Sets the recipient(s) of the mail
	 * @param mixed $to Can either be a string or an array of one or more email addresses
	 */
	public function to($to) {
		if(!is_array($to)) {
			$this->recipients = array_map('trim', explode(',', $to));
		}
	}

	/**
	 * Sets the subject portion of the mail
	 * @param string $subject The subject
	 */
	public function subject($subject) {
		$this->setHeader('Subject', $subject);
		$this->subject = $subject;
	}

	/**
	 * Sets the message/body portion of the mail
	 * @param string $body The message
	 */
	public function message($body) {
		$this->body = rtrim(str_replace("\r", '', $body));
	}

	/**
	 * Sends the mail.
	 * @return boolean Returns true if the mail was successfully sent
	 */
	public function send() {
		if(is_array($this->recipients)) {
			$this->to = implode(', ', $this->to);
		}
		return mail($this->recipients, $this->subject, $this->body, $this->headers);
	}

	/**
	 * Adds a header to the mail. If the header already exists it gets overwritten.
	 * @param string $header The name of the header
	 * @param string $value  The value of the header
	 */
	protected function setHeader($header, $value) {
		$this->headers[$header] = str_replace(array("\n", "\r"), '', $value);
	}
}

?>