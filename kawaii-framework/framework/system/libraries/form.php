<?php
/**
 *
 */
defined(BASEPATH) or die('No direct script access allowed');

class Form {

	/**
	 * [$fields description]
	 * @var array
	 */
	protected $fields = array();

	/**
	 * [$attributes description]
	 * @var array
	 */
	protected $attributes = array();

	/**
	 * [__construct description]
	 * @param string $name       [description]
	 * @param string $action     [description]
	 * @param string $method     [description]
	 * @param array  $attributes [description]
	 */
	public function __construct($name, $action, $method = 'post', $attributes = array()) {

		$attributes['name'] = $name;

		$attributes['action'] = Uri::full($action);

		$attributes['method'] = in_array($method, array('get', 'post') ? $method : 'post';

		$this->attributes = $attributes;
	}

	/**
	 * [__toString description]
	 * @return string [description]
	 */
	public function __toString() {

		$fields = '';

		foreach($this->fileds as $field) {

			$name = getValue($field, 'tag')

			switch($name) {

				case 'select':
					$inner = '';
					foreach($field['options'] as $option) {
						$inner .= Html::tag('option', $option);
					}
					unset($field['options']);
					$fields .= Html::tag($name, $field, $inner);
					break;

				case 'textarea':
					$fields .= Html::tag($name, $field, Html::clean($this->getValue($field, 'value')));
					break;

				case 'input':
					$fields .= Html::tag($name, $field);
					break;
			}
		}
		return Html::tag('form', $this->attributes, $fields);
	}

	/***/
	public function addSelect($name, $options = array(), $selected = null, $attributes = array()) {

		foreach($options as $option) {
			if($option['value'] == $selected) {
				$option['selected'] = 'selected';
				break;
			}
		}

		$attributes['options'] = $options;
		$attributes['name'] = $name;
		$attributes['tag'] = 'select';

		$this->fields[] = $attributes;
	}

	/**
	 * [addSubmit description]
	 * @param string $value      [description]
	 * @param array  $attributes [description]
	 */
	public function addSubmit($value = 'Submit', $attributes = array()) {
		$this->addInput('submit', 'submit', $attributes, $value);
	}

	/**
	 * [addReset description]
	 * @param string $value      [description]
	 * @param array  $attributes [description]
	 */
	public function addReset($value = 'Reset', $attributes = array()) {
		$this->addInput('reset', 'reset', $attributes, $value);
	}

	/**
	 * [addInput description]
	 * @param string $name       [description]
	 * @param string $type       [description]
	 * @param array  $attributes [description]
	 * @param string $value      [description]
	 */
	public function addInput($name, $type = 'text', $attributes = array(), $value = '') {

		empty($value) or $attributes['value'] = $value;

		$attributes['name'] = $this->attributes['name'] . '-' . $name;
		$attributes['type'] = $type;
		$attributes['tag'] = 'input';

		$this->fileds[] = $attributes;
	}

	/**
	 * [getValue description]
	 * @param  [type] $array [description]
	 * @param  [type] $value [description]
	 * @return [type]        [description]
	 */
	protected function getValue($array, $value) {
		if(isset($array[$value])) {
			$item = $array[$value];
			unset($array[$value]);
			return $item;
		}
		return null;
	}
}

?>