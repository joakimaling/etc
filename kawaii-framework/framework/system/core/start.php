<?php
/**
 *
 */
defined(BASEPATH) or die('No direct script access allowed');

/**
 *
 */
define('VERSION', '0.0.1');

/**
 * GLOBAL FUNCTIONS
 *
 * Loads all the global functions commonly used throughout the application.
 */
require BASEPATH . 'core/common.php';

/**
 * RUN THE APPLICATION
 */
require MAINPATH . 'core/run.php';

?>