<?php
/**
 *
 */
defined(BASEPATH) or die('No direct script access allowed');

/**
 * [getConfig description]
 * @return array [description]
 */
function getConfig() {
	return MAINPATH . 'config/' . ENVIRONMENT . '.php';
}


function in_array_r($needle, $haystack, $strict = false) {
    foreach($haystack as $item) {
        if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

/**
 * Generates an extremely unique id number.
 * @return string [description]
 */
function uniq_id() {
	return strtoupper(md5(uniqid(rand(), true)));
}

?>