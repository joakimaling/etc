<?php
/**
 *
 */
defined(BASEPATH) or die('No direct script access allowed');

use PDO;
use PDOException;

class Connector {

	/**
	 * [$pdo description]
	 * @var [type]
	 */
	protected $pdo;

	/**
	 * [__construct description]
	 * @param array $config [description]
	 */
	public function __construct($config) {
		try {
			extract($config);
			$dns = 'mysql:dbname=' . $database . ';host=' . $hostname . ';port=' . $port . ';charset=' . $charset;
			$this->pdo = new PDO($dns, $username, $password);
		}
		catch(PDOException $exception) {
			die($exception->getMessage());
		}
	}

	/**
	 * [query description]
	 * @param  string $sql [description]
	 * @return [type]      [description]
	 */
	public function query($sql) {
		return $this->pdo->prepare($sql);
	}
}

?>