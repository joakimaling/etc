<?php
/**
 *
 */
defined('BASEPATH') or die('No direct script access allowed');

class Query {

	/***/
	protected $connection = null;

	/**
	 * [$lwrap description]
	 * @var string
	 */
	protected $lwrap = '`';

	/**
	 * [$rwrap description]
	 * @var string
	 */
	protected $rwrap = '`';

	/**
	 * [$select description]
	 * @var [type]
	 */
	protected $select = null;

	/**
	 * [$distinct description]
	 * @var boolean
	 */
	protected $distinct = false;

	/**
	 * [$from description]
	 * @var [type]
	 */
	protected $from = null;

	/**
	 * [$join description]
	 * @var [type]
	 */
	protected $join = null;

	/**
	 * [$where description]
	 * @var [type]
	 */
	protected $where = null;

	/**
	 * [$groupBy description]
	 * @var [type]
	 */
	protected $groupBy = null;

	/**
	 * [$having description]
	 * @var [type]
	 */
	protected $having = null;

	/**
	 * [$orderBy description]
	 * @var [type]
	 */
	protected $orderBy = null;

	/**
	 * [$limit description]
	 * @var [type]
	 */
	protected $limit = null;

	/**
	 * [$insert description]
	 * @var [type]
	 */
	protected $insert = null;

	/**
	 * [$update description]
	 * @var [type]
	 */
	protected $update = null;

	/**
	 * [$delete description]
	 * @var [type]
	 */
	protected $delete = null;

	/**
	 * [$columns description]
	 * @var [type]
	 */
	protected $columns = null;

	/**
	 * [$values description]
	 * @var [type]
	 */
	protected $values = null;

	/**
	 * [$set description]
	 * @var [type]
	 */
	protected $set = null;

	/**
	 * [__construct description]
	 * @param object $connection [description]
	 */
	public function __construct($connection) {
		$this->connection = $connection;
	}

	/**
	 * [__toString description]
	 * @return string [description]
	 */
	public function __toString() {

		$query = '';

		switch($this->type) {
			case 'select':
				$query .= (string) $this->select;
				$query .= (string) $this->from;

				if($this->where) {
					$query .= (string) $this->where;
				}

				if($this->groupBy) {
					$query .= (string) $this->groupBy;
				}

				if($this->having) {
					$query .= (string) $this->having;
				}

				if($this->orderBy) {
					$query .= (string) $this->orderBy;
				}

				break;

			case 'insert':
				$query .= (string) $this->insert;

				if($this->set) {
					$query .= (string) $this->set;
				}
				else {
					if($this->columns) {
						$query .= (string) $this->columns;
					}

					$query .= (string) $this->values;
				}

				break;

			case 'update':
				$query .= (string) $this->update;
				$query .= (string) $this->set;

				if($this->where) {
					$query .= (string) $this->where;
				}

				break;

			case 'delete':
				$query .= (string) $this->delete;
				$query .= (string) $this->from;

				if($this->where) {
					$query .= (string) $this->where;
				}

				break;
		}

		if($this->limit) {
			$query .= (string) $this->limit;
		}

		return $query;
	}

	/**
	 * [select description]
	 * @param  array  $columns [description]
	 * @return [type]          [description]
	 */
	public function select($columns = array('*')) {

		$this->type = 'select';

		if(is_null($this->select)) {
			$this->select = new QueryItem('SELECT', $columns);
		}
		else {
			$this->select->add($columns);
		}

		return $this;
	}

	/***/
	public function distinct() {

		return $this;
	}

	/**
	 * [from description]
	 * @param  mixed  $columns [description]
	 * @param  string $alias   [description]
	 * @return [type]          [description]
	 */
	public function from($columns, $alias = null) {
		if(is_null($this->from)) {
			if($tables instanceof $this) {
				$tables = '(' . (string) $tables . ') AS ' . $alias;
			}
			$this->from = new QueryItem('FROM', $tables);
		}
		else {
			$this->from->add($columns);
		}
		return $this;
	}

	/**
	 * [where description]
	 * @param  string $conditions [description]
	 * @param  string $glue       [description]
	 * @return [type]             [description]
	 */
	public function where($conditions, $glue = 'AND') {
		if(is_null($this->where)) {
			$this->where = new QueryItem('WHERE', $conditions, ' ' . $glue . ' ');
		}
		else {
			$this->where->add($conditions);
		}
		return $this;
	}

	/***/
	public function join() {

	}

	/**
	 * [groupBy description]
	 * @param  mixed $columns [description]
	 * @return [type]          [description]
	 */
	public function groupBy($columns) {
		if(is_null($this->groupBy)) {
			$this->groupBy = new QueryItem('GROUP BY', $columns);
		}
		else {
			$this->groupBy->add($columns);
		}
		return $this;
	}

	/**
	 * [having description]
	 * @param  string $conditions [description]
	 * @param  string $glue       [description]
	 * @return [type]             [description]
	 */
	public function having($conditions, $glue = 'AND') {
		if(is_null($this->having)) {
			$this->having = new QueryItem('HAVING', $conditions);
		}
		else {
			$this->having->add($conditions);
		}
		return $this;
	}

	/***/
	public function orderBy() {

		return $this;
	}

	/**
	 * [limit description]
	 * @param  int $rows   [description]
	 * @param  int $offset [description]
	 * @return [type]         [description]
	 */
	public function limit($rows, $offset = null) {
		$limit = is_null($offset) ? array($rows) : array($offset, $rows);
		$this->limit = new QueryItem('LIMIT', $limit);
		return $this;
	}

	/**
	 * [insert description]
	 * @param  string $table [description]
	 * @return [type]        [description]
	 */
	public function insert($table) {
		$this->type = 'insert';
		$this->insert = new QueryItem('INSERT INTO', $table);

		return $this;
	}

	/**
	 * [columns description]
	 * @param  [type] $columns [description]
	 * @return [type]          [description]
	 */
	public function columns($columns) {
		if(is_null($this->columns)) {
			$this->columns = new QueryItem('()', $columns);
		}
		else {
			$this->columns->add($columns);
		}

		return $this;
	}

	/**
	 * [values description]
	 * @param  [type] $values [description]
	 * @return [type]         [description]
	 */
	public function values($values) {
		if(is_null($this->values)) {
			$this->values = new QueryItem('()', $values, '), (');
		}
		else {
			$this->columns->add($values);
		}

		return $this;
	}

	/**
	 * [update description]
	 * @param  string $table [description]
	 * @return [type]        [description]
	 */
	public function update($table) {
		$this->type = 'update';
		$this->update = new QueryItem('UPDATE', $table);

		return $this;
	}

	/**
	 * [delete description]
	 * @param  string $table [description]
	 * @return [type]        [description]
	 */
	public function delete($table) {
		$this->type = 'delete';
		$this->delete = new QueryItem('DELETE', null);

		empty($table) or $this->from($table);

		return $this;
	}

	/**
	 * [get description]
	 * @return [type] [description]
	 */
	public function get() {
		$statement = $this->connection->query($this);
		return $statement->fetchAll();
	}
}

?>