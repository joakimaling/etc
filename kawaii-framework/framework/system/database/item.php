<?php
/**
 *
 */
defined(BASEPAT) or die('No direct script access allowed');

class QueryItem {

	/**
	 * [$type description]
	 * @var string
	 */
	protected $type = null;

	/**
	 * [$items description]
	 * @var array
	 */
	protected $items = array();

	/**
	 * [$glue description]
	 * @var string
	 */
	protected $glue = null;

	/**
	 * [__construct description]
	 * @param string $type    [description]
	 * @param array  $columns [description]
	 * @param string $glue    [description]
	 */
	public function __construct($type, $columns, $glue = ', ') {
		$this->type = $type;
		$this->add($items);
		$this->glue = $glue;
	}

	/**
	 * [__toString description]
	 * @return string [description]
	 */
	public function __toString() {
		return PHP_EOL . $this->type . ' ' . implode($this->glue, $this->items);
	}

	/**
	 * [add description]
	 * @param mixed $items [description]
	 */
	public function add($items) {
		if(is_array($items)) {
			$this->items = array_merge($this->items, $items);
		}
		else {
			$this->items = array_merge($this->items, array($items));
		}
	}

	/***/
	protected function wrap() {

	}
}

?>