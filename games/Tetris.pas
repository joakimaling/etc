program Tetris;

const
	Height = 18;
	Width = 12;
	Tetromino: Array[0..6] of String = (
		'..X...X...X...X.',
		'..x..xx..x......',
		'.X...xx...x.....',
		'.....XX..XX.....',
		'..X..XX...X.....',
		'.....XX...X...X,',
		'.....XX..X...X..'
	);


var
	Field: Array of Char;
	IsGameOver: Boolean;
	Score: Word;

	Index: Word;

{}
procedure DrawField;
begin
	Write(#27, '[0;0f');
	for Index := 0 to High(Field) do begin
		Write(Field[Index]);
		if Index mod Width = Width - 1 then WriteLn;
	end;
end;

{}
function Rotate(X, Y, R: Byte): Byte;
begin
	case R mod 4 of
		0: Rotate := Y * 4 + X;
		1: Rotate := 12 + Y - (X * 4);
		2: Rotate := 15 - (Y * 4) - X;
		3: Rotate := 3 - Y + (X * 4);
	end;
end;

begin
	Write(#27, '[?25l');
	SetLength(Field, Height * Width);

	for Index := 0 to High(Field) do begin
		Field[Index] := #32;
	end;

	IsGameOver := false;

	repeat
		DrawField;


	until IsGameOver;
end.
