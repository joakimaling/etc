{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator....  Joakim Åling, Sweden                                                                }
{ Date.......  April 27, 2013                                                                      }
{ Website....                                                                                      }
{                                                                                                  }
{ Description  A game for two where the goal is to sink the opponent's fleet of ships. Begin with  } 
{              placing each players ships on a map and then take turns targeting locations on the  }
{              map. During this phase the ships are hidden. If s ship is hit a mark will be left   }
{              on the map hinting the location of a ship.                                          }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....  2013-05-01 release of the first version                                             }
{              2013-05-02 added information system, fixed some small bugs                          }
{                                                                                                  }
{ Information  Implement an AI so there's an option to play against the computer. Network play.    }
{              save progress, scoring system                                                       }
{--------------------------------------------------------------------------------------------------}
program BattleShipsGame;

uses
	Crt, GameUtils, SysUtils;
{--------------------------------------------------------------------------------------------------}
const
	ShipShapes: array[0..4, 0..3, 0..4] of String = (
		(('  0  ', '  00 ', '  00 ', '   0 ',	'     '), ('     ', ' 000 ', '000  ', '     ', '     '), ('  0  ', '  00 ', '  00 ', '   0 ',	'     '), ('     ', ' 000 ', '000  ', '     ', '     ')),
		(('  1  ', '  1  ', '  1  ', '  1  ', '  1  '), ('    1', '   1 ', '  1  ', ' 1   ', '1    '), ('     ', '     ', '11111', '     ', '     '), ('1    ', ' 1   ', '  1  ', '   1 ', '    1')),
		(('     ', '  2  ', ' 222 ', '     ', '     '), ('     ', '  2  ', '  22 ', '  2  ', '     '), ('     ', '     ', ' 222 ', '  2  ', '     '), ('     ', '  2  ', ' 22  ', '  2  ', '     ')),
		(('     ', '  3  ', '  3  ', '  3  ', '     '), ('     ', '   3 ', '  3  ', ' 3   ', '     '), ('     ', '     ', ' 333 ', '     ', '     '), ('     ', ' 3   ', '  3  ', '   3 ', '     ')),
		(('     ', '  4  ', '  4  ', '     ', '     '), ('     ', '   4 ', '  4  ', '     ', '     '), ('     ', '  44 ', '     ', '     ', '     '), ('     ', ' 4   ', '  4  ', '     ', '     '))
	);
	ShipNames: array[0..4] of String = (
		'Aircraft carrier',
		'Battleship      ',
		'Submarine       ',
		'Cruiser         ',
		'Destroyer       '
	);
	MaxShips = 5;
	MaxPlayers = 2;
	Width = 38;
	Height = 14;
	
	BoardY = 1;
	BoardX = 40;

	Target = '?';
	Corsair = 'X';
	Miss = 'M';
	Hit = 'H';
	Water = '.';
	Haul = '#';
{--------------------------------------------------------------------------------------------------}
type
	TBoard = array[0..Width - 1, 0..Height - 1] of Char;
	TShips = array[0..MaxShips - 1] of Byte;
{--------------------------------------------------------------------------------------------------}
var
	Player: array[0..MaxPlayers - 1] of record
		Board: TBoard;
		Ships: TShips;	
	end;
	CurrentPlayer: Byte;
{--------------------------------------------------------------------------------------------------}
procedure InitGame;
var
	X, Y: Byte;
begin
	for X := 0 to Width - 1 do begin
		for Y := 0 to Height - 1 do begin
			Player[0].Board[X, Y] := Water;
			Player[1].Board[X, Y] := Water;
		end;
	end;
	for X := 0 to MaxShips - 1 do begin
			Player[0].Ships[X] := 6 - X;
			Player[1].Ships[X] := 6 - X;
	end;
	CurrentPlayer := 0;
	MessageBox(3, Height + 9, 80, 3, $07, $03);
end;
{--------------------------------------------------------------------------------------------------}
procedure GameMenu;
var
	MenuItems: TMenuItem;
 	UserFile: Text;
 	Row: String;
begin
	ClrScr;
 	Assign(UserFile, 'ship.txt');
	{$I-}
 	Reset(UserFile);
	{$I+}
 	if IOResult = 0 then begin
 		while not Eof(UserFile) do begin
 			ReadLn(UserFile, Row);
 			WriteLn(Row);
 		end;
 	end;
 	Close(UserFile);	
	Write(#27, '[?25l');
	GotoXY(12, 3);
	Write('WELCOME TO BATTLESHIPS!');
	GotoXY(15, 4);
	Write('v0.2 (2013-05-01)');
	SetLength(MenuItems, 2);
	MenuItems[0] := 'Start Game';
	MenuItems[1] := '   Quit   ';
	case DrawMenu(17, 6, MenuItems, $07, $70) of
		0:
			begin
				ClrScr;
				Exit;
			end;
		1:
			Halt;
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure DrawBoard(SX, SY: Byte; Board: TBoard; Hide: Boolean);
var
	X, Y: Byte;
	Symbol: Char;
begin
	TextColor($07);
	DrawFrame(SX, SY, Width + 1, Height + 1);
	for Y := 0 to Height - 1 do begin
		for X := 0 to Width - 1 do begin
			GotoXY(SX + X + 1, SY + Y + 1);
			Symbol := Board[X, Y];
			case Board[X, Y] of
				'0'..'4': 
					if Hide then begin
						TextColor($01);
						Symbol := Water;
					end else begin
						TextColor($0f);
						Symbol := Haul;
					end;
				'A'..'E', Target:
					begin
						TextColor($06);
						Symbol := Target;
					end;
				Hit:
					if Hide then TextColor($02) else TextColor($04);
				Miss:
					if Hide then TextColor($04) else TextColor($02);	
				else
					TextColor($01);
			end;
			Write(Symbol);
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure PlaceShips(var Board: TBoard; Ships: TShips; Player: Byte);
var
	Data: array[0..MaxShips - 1] of record
		X, Y, Flip: Byte;
	end;
	Finished: Boolean = false;
	Ship, X, Y: Byte;
	Shape: String;
	Key: Char;
begin
	WriteHighlighted(3, Height + 4, 'Use the [%arrow keys%] to move the ships, [%space%] to rotate them, [%return%]', $07, $03);
	WriteHighlighted(3, Height + 5, 'to select the next ship and, when you are done, hit [%escape%].', $07, $03);
	Ship := 0;
	Data[0].Flip := 0;
	Data[0].X := 0;
	Data[0].Y := 0;
	Data[1].Flip := 0;
	Data[1].X := Width - 5;
	Data[1].Y := 0;
	Data[2].Flip := 0;
	Data[2].X := 0;
	Data[2].Y := Height - 5;
	Data[3].Flip := 0;
	Data[3].X := Width - 5;
	Data[3].Y := Height - 5;
	Data[4].Flip := 0;
	Data[4].X := 10;
	Data[4].Y := 5;
	ShowMessage('Player %' + IntToStr(Player + 1) + '%: place your %' + ShipNames[Ship] + '%');
	repeat
		for Y := 0 to 4 do begin
			Shape := ShipShapes[Ship, Data[Ship].Flip, Y];
			for X := 0 to 4 do begin
				if Shape[X + 1] <> ' ' then Board[Data[Ship].X + X, Data[Ship].Y + Y] := Shape[X + 1];
			end;
		end;
		DrawBoard((80 - (Width + 2)) div 2, BoardY, Board, false);
		Key := ReadKey;
		if Key in [#72, #75, #77, #80, #32] then begin
			for Y := Data[Ship].Y to Data[Ship].Y + 4 do begin
				for X := Data[Ship].X to Data[Ship].X + 4 do begin
					Board[X, Y] := Water;
				end;
			end;
		end;
		case Key of
			#72:
				if Data[Ship].Y = 0 then Data[Ship].Y := Height - 5 else Dec(Data[Ship].Y);
			#75:
				if Data[Ship].X = 0 then Data[Ship].X := Width - 5 else Dec(Data[Ship].X);
			#77:
				if Data[Ship].X = Width - 5 then Data[Ship].X := 0 else Inc(Data[Ship].X);
			#80:
				if Data[Ship].Y = Height - 5 then Data[Ship].Y := 0 else Inc(Data[Ship].Y);
			#32:
				Data[Ship].Flip := (Data[Ship].Flip + 1) mod 4;
			#13:
				begin
					Ship := (Ship + 1) mod 5;
					ShowMessage('Player %' + IntToStr(Player + 1) + '%: place your %' + ShipNames[Ship] + '%');
				end;
			#27:
				Finished := true;
		end;
	until Finished;
end;
{--------------------------------------------------------------------------------------------------}
procedure PlaceTargets(var Board: TBoard; Shots: Byte);
var
	Finished: Boolean = false;
	X: Byte = Width div 2;
	Y: Byte = Height div 2;
begin
	WriteHighlighted(3, Height + 4, 'Use the [%arrow keys%] to move the corsair, [%space%] target an area, ', $07, $03);
	WriteHighlighted(3, Height + 5, 'when you have placed all targets press [%return%] to fire upon them.', $07, $03);	
	ShowMessage('You have %' + IntToStr(Shots) + '% targets left to place');
	repeat
		DrawBoard(BoardX * CurrentPlayer + 1, BoardY, Board, true);
		GotoXY(BoardX * CurrentPlayer + X + 2, BoardY + Y + 1);
		TextColor($07);
		Write(Corsair);
		case ReadKey of
				#72:
					if Y = 0 then Y := Height - 1 else Dec(Y);
				#75:
					if X = 0 then X := Width - 1 else Dec(X);
				#77:
					if X = Width - 1 then X := 0 else Inc(X);
				#80:
					if Y = Height - 1 then Y := 0 else Inc(Y);
				#32:
					if Shots > 0 then begin
						case Board[X, Y] of
							'0'..'4', Water:
								begin
									Board[X, Y] := Chr(Ord(Board[X, Y]) + 17);
									Dec(Shots);
									ShowMessage('You have %' + IntToStr(Shots) + '% targets left to place');
								end;
							'A'..'E', Target, Hit, Miss:
								ShowMessage('The area [%' + IntToStr(X) + '%,%' + IntToStr(Y)  + '%] has already been targeted');
						end;
					end;
				#8:
					case Board[X, Y] of
						'A'..'E', Target:
							begin
								Board[X, Y] := Chr(Ord(Board[X, Y]) - 17);
								Inc(Shots);
							end;
						Hit, Miss:
							ShowMessage('The targeted area [%' + IntToStr(X) + '%,%' + IntToStr(Y) + '%] cannot be removed'); 
					end;
				#13:
					Finished := Shots = 0;
				#27:
					Halt;
			end;
		until Finished;
end;
{--------------------------------------------------------------------------------------------------}
procedure AttackTargets(var Board: TBoard; var Ships: TShips);
var
	X, Y: Byte;
begin
	for Y := 0 to Height - 1 do begin
		for X := 0 to Width - 1 do begin
			if Board[X, Y] in ['A'..'E', Target] then begin
				if Board[X, Y] in ['A'..'E'] then begin
					Dec(Ships[Ord(Board[X, Y]) - 65]);
					Board[X, Y] := Hit;
					ShowMessage('Firing at [%' + IntToStr(X) + '%,%' + IntToStr(Y) + '%]. A ship was hit!');
				end else if Board[X, Y] = Target then begin
					Board[X, Y] := Miss;
					ShowMessage('Firing at [%' + IntToStr(X) + '%,%' + IntToStr(Y) + '%]. No target hit');
				end;
				DrawBoard(BoardX * CurrentPlayer + 1, BoardY, Board, true);
				Sleep(400);
			end;
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
function GameOver(Ships: TShips): Boolean;
var
	Index: Byte;
begin
	GameOver := true;
	for Index := 0 to MaxShips - 1 do begin
		if Ships[Index] <> 0 then begin
			GameOver := false;
			Exit;
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
function NextPlayer: Byte;
begin
	NextPlayer := (CurrentPlayer + 1) mod MaxPlayers;
end;
{--------------------------------------------------------------------------------------------------}
function AvailableShots(Ships: TShips): Byte;
var
	Index: Byte;
begin
	AvailableShots := 0;
	for Index := 0 to MaxShips - 1 do begin
		Inc(AvailableShots, 2 * Ships[Index]);
	end;
end;
{--------------------------------------------------------------------------------------------------}
begin
	InitGame;
 	GameMenu;
	PlaceShips(Player[NextPlayer].Board, Player[CurrentPlayer].Ships, CurrentPlayer);
	PlaceShips(Player[CurrentPlayer].Board, Player[NextPlayer].Ships, NextPlayer);
	repeat
		ShowMessage('Player %' + IntToStr(CurrentPlayer + 1) + '%: get ready! Press <%Enter%> to continue');
		ReadLn;
		DrawBoard(BoardX * NextPlayer + 1, BoardY, Player[NextPlayer].Board, false);
 		PlaceTargets(Player[CurrentPlayer].Board, AvailableShots(Player[CurrentPlayer].Ships));
 		AttackTargets(Player[CurrentPlayer].Board, Player[NextPlayer].Ships);
		CurrentPlayer := NextPlayer;
	until GameOver(Player[NextPlayer].Ships);	
end.
