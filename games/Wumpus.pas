program Wumpus;

uses
	Crt;

const
	TotalBats = 5;
	TotalGold = 5;
	TotalPits = 5;

type
	ContentTypes = (Empty, Bats, Gold, Monster, Pit);
	PRoom = ^TRoom;
	TRoom = record
		Content: ContentTypes;
		Left, Middle, Right: PRoom;
	end;

var
	CurrentRoom: PRoom;
	Golds, Key, Number: Byte;
	InKey: Char;
	IsEnd: Boolean;
	Rooms: Array[0..19] of PRoom;

{ Bind the rooms together. }
procedure BindRooms;
begin
	Rooms[0]^.Left := Rooms[1];
	Rooms[0]^.Middle := Rooms[4];
	Rooms[0]^.Right := Rooms[10];
	Rooms[1]^.Left := Rooms[0];
	Rooms[1]^.Middle := Rooms[2];
	Rooms[1]^.Right := Rooms[12];
	Rooms[2]^.Left := Rooms[1];
	Rooms[2]^.Middle := Rooms[3];
	Rooms[2]^.Right := Rooms[16];
	Rooms[3]^.Left := Rooms[2];
	Rooms[3]^.Middle := Rooms[4];
	Rooms[3]^.Right := Rooms[19];
	Rooms[4]^.Left := Rooms[0];
	Rooms[4]^.Middle := Rooms[3];
	Rooms[4]^.Right := Rooms[14];
	Rooms[5]^.Left := Rooms[6];
	Rooms[5]^.Middle := Rooms[9];
	Rooms[5]^.Right := Rooms[13];
	Rooms[6]^.Left := Rooms[5];
	Rooms[6]^.Middle := Rooms[7];
	Rooms[6]^.Right := Rooms[17];
	Rooms[7]^.Left := Rooms[6];
	Rooms[7]^.Middle := Rooms[8];
	Rooms[7]^.Right := Rooms[18];
	Rooms[8]^.Left := Rooms[7];
	Rooms[8]^.Middle := Rooms[9];
	Rooms[8]^.Right := Rooms[14];
	Rooms[9]^.Left := Rooms[5];
	Rooms[9]^.Middle := Rooms[8];
	Rooms[9]^.Right := Rooms[11];
	Rooms[10]^.Left := Rooms[0];
	Rooms[10]^.Middle := Rooms[11];
	Rooms[10]^.Right := Rooms[13];
	Rooms[11]^.Left := Rooms[9];
	Rooms[11]^.Middle := Rooms[10];
	Rooms[11]^.Right := Rooms[12];
	Rooms[12]^.Left := Rooms[1];
	Rooms[12]^.Middle := Rooms[11];
	Rooms[12]^.Right := Rooms[14];
	Rooms[13]^.Left := Rooms[5];
	Rooms[13]^.Middle := Rooms[10];
	Rooms[13]^.Right := Rooms[14];
	Rooms[14]^.Left := Rooms[8];
	Rooms[14]^.Middle := Rooms[12];
	Rooms[14]^.Right := Rooms[16];
	Rooms[15]^.Left := Rooms[4];
	Rooms[15]^.Middle := Rooms[13];
	Rooms[15]^.Right := Rooms[17];
	Rooms[16]^.Left := Rooms[2];
	Rooms[16]^.Middle:= Rooms[14];
	Rooms[16]^.Right := Rooms[18];
	Rooms[17]^.Left := Rooms[6];
	Rooms[17]^.Middle := Rooms[14];
	Rooms[17]^.Right := Rooms[19];
	Rooms[18]^.Left := Rooms[7];
	Rooms[18]^.Middle := Rooms[16];
	Rooms[18]^.Right := Rooms[19];
	Rooms[19]^.Left := Rooms[3];
	Rooms[19]^.Middle := Rooms[17];
	Rooms[19]^.Right := Rooms[18];
end;

{ Print information about the current room. }
procedure ExamineRoom;
begin
	case CurrentRoom^.Content of
		Bats: WriteLn('You are teleported to a randomly selected room.');
		//Writeln('Uh oh. You were relocated by bats.');
		Gold: begin
			CurrentRoom^.Content := Empty;
			Inc(Golds);
			if Golds < TotalGold then WriteLn('You found some gold!') else begin
				WriteLn('You found all the gold!');
				IsEnd := true;
			end;
		end;
		Monster: begin
			WriteLn('Oh, no! You stumbled upon the Wumpus itself. ');
			//Writeln('Oh no! The Wumpus ate you up!');
			IsEnd := true;
		end;
		Pit: begin
			WriteLn('You fall to a certain death.');
			//Writeln('Ah! You fell into a bottomless pit!');
			IsEnd := true;
		end;
	end;
	with CurrentRoom^ do begin
		if (Left^.Content = Bats) or (Middle^.Content = Bats) or (Right^.Content = Bats) then begin
			WriteLn('You can hear some noise coming from somewhere!');
		end;

		if (Left^.Content = Monster) or (Middle^.Content = Monster) or (Right^.Content = Monster) then begin
			WriteLn('You can smell a horrendous stentch!');
		end;

		if (Left^.Content = Pit) or (Middle^.Content = Pit) or (Right^.Content = Pit) then begin
			WriteLn('You can feel a cool breeze in the room!');
		end;
	end;
end;

{ Reset the state of the game and replace the contents of the rooms. }
procedure ResetGame;
begin
	{ Clear all rooms of content. }
	for Key := 0 to 19 do begin
		Rooms[Key]^.Content := Empty;
	end;

	{ Place the Wumpus. }
	Rooms[Random(20)]^.Content := Monster;

	{ Place the Bats. }
	for Key := 1 to TotalBats do begin
		repeat
			Number := Random(20);
		until Rooms[Number]^.Content = Empty;
		Rooms[Number]^.Content := Bats;
	end;

	{ Place the Gold. }
	for Key := 1 to TotalGold do begin
		repeat
			Number := Random(20);
		until Rooms[Number]^.Content = Empty;
		Rooms[Number]^.Content := Gold;
	end;

	{ Place the Pits. }
	for Key := 1 to TotalPits do begin
		repeat
			Number := Random(20);
		until Rooms[Number]^.Content = Empty;
		Rooms[Number]^.Content := Pit;
	end;

	{ Place the Player. }
	repeat
		Number := Random(20);
	until Rooms[Number]^.Content = Empty;
	CurrentRoom := Rooms[Number];

	IsEnd := false;
	Golds := 0;
end;

{ Checks if the direction in which an arrow is shot hits the Wumpus. }
procedure ShootArrow(Direction: Char);
var
	IsHit: Boolean = false;
begin
	case Direction of
		'L', 'l': IsHit := CurrentRoom^.Left^.Content = Monster;
		'M', 'm': IsHit := CurrentRoom^.Middle^.Content = Monster;
		'R', 'r': IsHit := CurrentRoom^.Right^.Content = Monster;
	end;
	if IsHit then begin
		WriteLn('You killed the Wumpus!');
		IsEnd := true;
	end else begin
		WriteLn('You missed! The Wumpus was startled and it moved...');
		// Magic Code!
	end;
end;

{ Print the keyboard options. }
procedure ShowHelp;
begin
	WriteLn('+--------------------+');
	WriteLn('| A, a: Shoot Arrows |');
	WriteLn('| L, l: Go to Left   |');
	WriteLn('| M, m: Go to Middle |');
	WriteLn('| R, R: Go to Right  |');
	WriteLn('| Esc: Exit the Game |');
	WriteLn('+--------------------+');
end;

begin
	Randomize;
	ResetGame;
	BindRooms;
	ShowHelp;
	repeat
		ExamineRoom;
		Write('> ');
		ReadLn(InKey);
		case InKey of
			'A', 'a': begin
				Write('Direction > ');
				ReadLn(InKey);
				case InKey of
					'L', 'l', 'M', 'm', 'R', 'r': ShootArrow(InKey);
					else WriteLn('Not possible...');
				end;
			end;
			'L', 'l': CurrentRoom := CurrentRoom^.Left;
			'M', 'm': CurrentRoom := CurrentRoom^.Middle;
			'R', 'r': CurrentRoom := CurrentRoom^.Right;
			#27: Halt;
			else ShowHelp;
		end;
	until IsEnd;
end.
