#ifndef BALL_H
#define BALL_H

#include <allegro.h>
#include "paddle.h"

class Ball{
public:
	Ball();
	~Ball();

	void init();
	bool update(Paddle &p1, Paddle &p2);
	void render(BITMAP *dest);

	double x, y, r, dx, dy;
	SAMPLE *sfx;
};

#endif