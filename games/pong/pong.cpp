#include <allegro.h>
#include "ball.h"
#include "score.h"

#define LEFT 0
#define RIGHT 1

int main(){
	allegro_init();
	install_keyboard();
	set_color_depth(8);
	set_gfx_mode(GFX_AUTODETECT_WINDOWED, 800, 600, 0, 0);

	Ball ball;
	Paddle p1, p2;
	Score s1, s2;

	BITMAP *buffer = create_bitmap(SCREEN_W, SCREEN_H);

	ball.init();
	p1.init(LEFT);
	p2.init(RIGHT);
	s1.init();
	s2.init();

	while(!key[KEY_ESC])
	{
		if(key[KEY_LSHIFT])
			p1.y -= (p1.y < 0)? 0: 1;
		else if(key[KEY_LCONTROL])
			p1.y += (p1.y > SCREEN_H - p1.h)? 0: 1;

		if(key[KEY_RSHIFT])
			p2.y -= (p2.y < 0)? 0: 1;
		else if(key[KEY_RCONTROL])
			p2.y += (p2.y > SCREEN_H - p2.h)? 0: 1;

		if(s1.update(p1.score) || s2.update(p2.score))
			exit(1);

		if(ball.update(p1, p2))
			ball.init();

		ball.render(buffer);

		p1.render(buffer);
		p2.render(buffer);

		s1.render(buffer, SCREEN_W * .25, 20);
		s2.render(buffer, SCREEN_W * .75, 20);

		vline(buffer, SCREEN_W / 2, 0, SCREEN_H, makecol(255, 255, 255));

		blit(buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
		clear_bitmap(buffer);
	}
	destroy_bitmap(buffer);
	return EXIT_SUCCESS;
}
END_OF_MAIN();