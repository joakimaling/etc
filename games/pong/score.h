#ifndef SCORE_H
#define SCORE_H

#include <allegro.h>
#include <string>

class Score{
public:
	Score();
	~Score();

	void init();
	bool update(int score);
	void render(BITMAP *dest, double x, double y);

	double w, h;
	int p1, p2; //Part 1 and 2 of the score
	BITMAP *digit[10];
};

#endif