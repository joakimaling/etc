#ifndef PADDLE_H
#define PADDLE_H

#include <allegro.h>

class Paddle{
public:
	Paddle();
	~Paddle();

	void init(bool aside);
	//void update();
	void render(BITMAP *dest);

	double x, y, w, h;
	int score;
};

#endif