#include "ball.h"
//==================================================================================================
Ball::Ball()
{

}
//==================================================================================================
Ball::~Ball()
{

}
//==================================================================================================
void Ball::init()
{
	x = SCREEN_W / 2;
	y = SCREEN_H / 2;
	r = 5;
	dx = 1;
	dy = -1;
}
//==================================================================================================
bool Ball::update(Paddle &p1, Paddle &p2)
{
	static double p1_last_x = 0, p2_last_x = 0; // we use this so the ball can't go trough the paddle

	// update ball position with speed x,y
	x += dx;
	y += dy;

	// bounce on field bounds
	if(x < r)
	{
		p2.score++;// 		x = r;
		return true;// 		dx = -dx;
	}

	if(x > SCREEN_W - r)
	{
		p1.score++;// 		x = SCREEN_W - r;
		return true;// 		dx = -dx;
	}

	if(y < r)
	{
		y = r;
		dy = -dy;
	}

	if(y > SCREEN_H - r)
	{
		y = SCREEN_H - r;
		dy = -dy;
	}

	if(dx < 0) //if going left
	{
		if(y + r > p1.y && y < p1.y + p1.h && (x + r > p1.x || x + r > p1_last_x) && (x < p1.x + p1.w || x < p1_last_x + p1.w))
 		dx = -dx;

	}
	else //if going right
	{
		if(y + r > p2.y && y < p2.y + p2.h && (x + r > p2.x || x + r > p2_last_x) && (x < p2.x + p2.w || x < p2_last_x + p2.w))
		dx = -dx;
	}

	p1_last_x = p1.x;
	p2_last_x = p2.x;

	return false;
}
//==================================================================================================
void Ball::render(BITMAP *dest)
{
	circlefill(dest, x, y, r, makecol(255, 255, 255));
}
//==================================================================================================