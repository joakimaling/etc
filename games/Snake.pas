program Snake;

uses
	Crt, SysUtils;

const
	BaseLength = 4;
	BodyGlyph: Array[Boolean] of Char = ('o', '+');
	HeadGlyph: Array[Boolean] of Char = ('@', 'X');
	Height = 25;
	Width = 50;

type
	Point = record
		X, Y: Byte;
	end;

var
	Body: Array of Point;
	Food: Point;
	IsDead, IsQuit: Boolean;
	Key, Score: Word;
	Orientation: (East, North, South, West);
	World: Array of Char;

{ Clears the world; filling it with full stops. }
procedure ClearWorld;
begin
	for Key := 0 to High(World) do begin
		World[Key] := '.';
	end;
end;

{ Draws the world and everything in it. }
procedure DrawWorld;
begin
	for Key := 1 to High(Body) do begin
		World[Body[Key].X + Body[Key].Y * Width] := BodyGlyph[IsDead];
	end;

	World[Body[0].X + Body[0].Y * Width] := HeadGlyph[IsDead];
	World[Food.X + Food.Y * Width] := '%';

	Write(#27, '[0;0fScore: ', Score);
	Write(#27, '[2;0f');
	for Key := 0 to High(World) do begin
		Write(World[Key]);
		if Key mod Width = Width - 1 then WriteLn;
	end;
end;

{ Increases the length of the snake's body by one. }
procedure GrowSnake;
begin
	SetLength(Body, 1 + Length(Body));
	Body[High(Body)].X := Body[High(Body) - 1].X;
	Body[High(Body)].Y := Body[High(Body) - 1].Y;
end;

{ Moves the snake one step in the current direction. }
procedure MoveSnake;
begin
	for Key := High(Body) downto 1 do begin
		Body[Key].X := Body[Key - 1].X;
		Body[Key].Y := Body[Key - 1].Y;
	end;

	case Orientation of
		East: if Body[0].X < Width - 1 then Inc(Body[0].X) else Body[0].X := 0;
		North: if Body[0].Y > 0 then Dec(Body[0].Y) else Body[0].Y := Height - 1;
		South: if Body[0].Y < Height - 1 then Inc(Body[0].Y) else Body[0].Y := 0;
		West: if Body[0].X > 0 then Dec(Body[0].X) else Body[0].X := Width - 1;
	end;
end;

{ Creates food at an un-occupied location. }
procedure SpawnFood;
begin
	repeat
		Food.X := Random(Width);
		Food.Y := Random(Height);
	until World[Food.X + Food.Y * Width] = '.';
end;

{ (Re)Initialises the game. }
procedure ResetGame;
begin
	SetLength(Body, BaseLength);
	for Key := 0 to BaseLength - 1 do begin
		Body[Key].X := Key + (Width div 2);
		Body[Key].Y := Height div 2;
	end;

	Orientation := West;
	IsDead := false;
	Score := 0;

	ClearWorld;
	SpawnFood;
end;

begin
	SetLength(World, Height * Width);
	Randomize;
	ClrScr;
	repeat
		ResetGame;
		repeat
			{ Collision - Check if the snake's on top of food, in which case re-
			  spawn the food and increase the length of the snake }
			if (Body[0].X = Food.X) and (Body[0].Y = Food.Y) then begin
				Inc(Score);
				SpawnFood;
				GrowSnake;
			end;

			{ Collision - Check if the snake hits a wall, in which case it dies
			  and it's game over }
			IsDead := '#' = World[Body[0].X + Body[0].Y * Width];

			{ Collision - Check if the snake hits itself, in which case it dies
			  and it's game over }
			for Key := 1 to High(Body) do begin
				IsDead := (Body[0].X = Body[Key].X) and (Body[0].Y = Body[Key].Y);
				if IsDead then Break;
			end;

			DrawWorld;
			Sleep(40);
			MoveSnake;

			ClearWorld;
			while KeyPressed do begin
				case ReadKey of
					#27: Halt;
					#72: if Orientation <> South then Orientation := North;
					#75: if Orientation <> East then Orientation := West;
					#77: if Orientation <> West then Orientation := East;
					#80: if Orientation <> North then Orientation := South;
				end;
			end;
		until IsDead;
		Write(#27, '[', 1 + Height div 2 + 1, ';', (Width - 42) div 2 + 1, 'f');
		Write('THE SNAKE DIED - PRESS SPACE TO PLAY AGAIN');
		IsQuit := ReadKey <> #32;
	until IsQuit;
end.
