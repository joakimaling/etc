{******************************************************************************}
{* Skapare: Shadowy     En simpel MS-R�j (MineSweeper) klon som spelas i kon- *}
{* Datum  : 2011-05-18  sollmilj� d�r r�jkommando ges i form av koordinater   *}
{******************************************************************************}
program MineSweeper;

uses
	Crt;

const
	max_mines = 15; x1_border = 0; x2_border = 9; y1_border = 0; y2_border = 9;
	CRLF = #10 + #13;

type
	square = record
		mine, hidden: boolean;
		number: char;
	end;

var
	field: array[x1_border..x2_border, y1_border..y2_border] of square;
	x, y, a, b: byte;
	won_lost: boolean;

procedure Print(output: char; x, y, colour: byte); //F�renklad utskrift...
begin
	TextAttr := colour;
	GotoXY(x * 2 + 1, y * 2 + 1);
	Write(output);
	TextAttr := $07;
end;


procedure Initialize; //S�tter f�ltets ing�ngsv�rden...
	var count: byte = 0;
begin
	won_lost := false;
	Randomize;
	for y := y1_border to y2_border do begin
		for x := x1_border to x2_border do begin
			if (count < max_mines) and (random(2) = 1) then begin //TODO: L�gg till % per rad...
				field[x, y].mine := true;
				Inc(count);
			end
			else
				field[x, y].mine := false;
			field[x, y].hidden := true;
		end;
	end;
end;

procedure Draw; //Ritar hela minf�ltet varje g�ng...
begin
	ClrScr;
	WriteLn('  0 1 2 3 4 5 6 7 8 9');
	for y := y1_border to y2_border do begin
		Print(Chr(y + 48), 0, y + 1, $07);
		for x := x1_border to x2_border do
			if field[x, y].hidden then
				Print('#', x + 1, y + 1, $03)
			else
				case field[x, y].number of
					'1'..'8': Print(field[x, y].number, x + 1, y + 1, $01);
					'0': Print(#32, x + 1, y + 1, $00);
				end;
	end;
end;

procedure Reveal; //Visar alla minor vid vinst eller f�rlust...
begin
	for y := y1_border to y2_border do
		for x := x1_border to x2_border do
			if field[x, y].mine then
				Print('*', x + 1, y + 1, $0c); //Blinkande ljusr�d f�rg...
end;

function SweepAt(x, y: byte): byte; //R�jer utpekat + kringliggande omr�de tills minor eller en �nda st�ts p�...
begin
	if (x in [x1_border..x2_border]) and (y in [y1_border..y2_border]) then begin
		if ((not field[x, y].mine) or (field[x, y].hidden)) then begin
			field[x, y].hidden := false;
			field[x, y].number := Chr(SweepAt(x - 1, y - 1) + SweepAt(x, y - 1) + SweepAt(x + 1, y - 1) + SweepAt(x + 1, y) + SweepAt(x + 1, y + 1) + SweepAt(x, y + 1) + SweepAt(x - 1, y + 1) + SweepAt(x - 1, y) + 48);
		end;
		SweepAt := Ord(field[x, y].mine);
	end;
end;

begin
	Initialize;
	repeat
		Draw;	Reveal;
		Print('>', 0, y2_border + 2, $07);
		ReadLn(a, b);
		if field[a, b].mine then won_lost := true else SweepAt(a, b);
	until won_lost;
	Reveal;
	ReadLn;
end.