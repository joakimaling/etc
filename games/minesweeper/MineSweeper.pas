{**
 * Project started: 2011-05-18
 *}
program MineSweeper;

const
	MaxHeight = 12;
	MaxWidth = 30;

type
	TCell = record
		HasMine: boolean;
		IsHidden: boolean;
		Value: byte;
	end;

var
	Grid: array[1..MaxWidth, 1..MaxHeight] of TCell;

procedure CountMines;
var
	X, Y, XOffset, YOffset: ShortInt;
begin
	for Y := 1 to MaxHeight do begin
		for X := 1 to MaxWidth do begin
			for YOffset := -1 to 1 do begin
				for XOffset := -1 to 1 do begin
					if (X + XOffset < 1) or (X + XOffset > MaxWidth) then Continue;
					if (Y + YOffset < 1) or (Y + YOffset > MaxHeight) then Continue;
					if Grid[X + XOffset, Y + YOffset].HasMine then Inc(Grid[X, Y].Value);
				end;
			end;
		end;
	end;
end;

procedure Init;
var
	X, Y: byte;
begin
	Randomize;
	for Y := 1 to MaxHeight do begin
		for X := 1 to MaxWidth do begin
			Grid[X, Y].HasMine := Random * 10 < 0.5; 
			Grid[X, Y].IsHidden := false;
		end;
	end;
end;

procedure Draw;
var
		X, Y: byte;
begin
	for Y := 1 to MaxHeight do begin
		for X := 1 to MaxWidth do begin
			if not Grid[X, Y].IsHidden then begin
				if Grid[X, Y].HasMine then begin
					Write('*');
				end else begin
					Write(Grid[X, Y].Value);
				end;
			end else begin
				Write(' ');			
			end;
		end;
		WriteLn;
	end;
end;

begin
	Init;
	CountMines;
	Draw;
end.
