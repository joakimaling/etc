{******************************************************************************}
{* Skapare: Shadowy     En simpel MS-Röj (MineSweeper) klon som spelas i kon- *}
{* Datum  : 2011-05-18  sollmiljö där röjkommando ges i form av koordinater   *}
{******************************************************************************}
program MineSweeper;

uses
	Crt;

const
	max_mines = 95; x1_border = 0; x2_border = 9; y1_border = 0; y2_border = 9;
	CRLF = #10 + #13;

type
	square = record
		mine, hidden, checked: boolean;
		number: char;
	end;

var
	field: array[x1_border..x2_border, y1_border..y2_border] of square;
	x, y, a, b: byte;
	won_lost: boolean;

procedure Print(output: char; x, y, colour: byte); //Förenklad utskrift...
begin
	TextAttr := colour;
	GotoXY(x, y);
	Write(output);
	TextAttr := lightgray;
end;


procedure Initialize; //Sätter fältets ingångsvärden...
	var count: byte = 0;
begin
	won_lost := false;
	Randomize;
	for y := y1_border to y2_border do begin
		for x := x1_border to x2_border do begin
			if (count < max_mines) and (random(2) = 1) then begin //TODO: Lägg till % per rad...
				field[x, y].mine := true;
				Inc(count);
			end
			else
				field[x, y].mine := false;
			field[x, y].hidden := true;
			field[x, y].checked := false;
		end;
	end;
end;

procedure Draw; //Ritar hela minfältet varje gång...
begin
	ClrScr;
	for y := y1_border to y2_border do begin
		for x := x1_border to x2_border do begin
			if field[x, y].hidden then
				Print('#', x, y, $03)
			else if field[x, y].checked then
				Print('c', x, y, $0f)
			else
				case field[x, y].number of
					'1'..'8': Print(field[x, y].number, x, y, $01); //TODO: ny färg för varje siffra
					'0': Print(#32, x, y, $00);
				end;
		end;
	end;
end;

procedure Reveal; //Visar alla minor vid vinst eller förlust...
begin
	for y := y1_border to y2_border do
		for x := x1_border to x2_border do
			if field[x, y].mine then
				Print('*', x, y, $0c); //Blinkande ljusröd färg...
end;

function SweepAt(x, y: byte): byte;
begin
	Draw;
	Reveal;
	ReadLn;
	if (x in [x1_border..x2_border]) and (y in [y1_border..y2_border]) then begin
		if ((not field[x, y].mine) or (not field[x, y].hidden)) and (not field[x, y].checked) then begin
			field[x, y].hidden := false;
			field[x, y].checked := true;
			field[x, y].number := Chr(SweepAt(x - 1, y - 1) + SweepAt(x, y - 1) + SweepAt(x + 1, y - 1) + SweepAt(x + 1, y) + SweepAt(x + 1, y + 1) + SweepAt(x, y + 1) + SweepAt(x - 1, y + 1) + SweepAt(x - 1, y) + 48);
		end;
		SweepAt := Ord(field[x, y].mine);
	end;
end;

begin
	Initialize;
	Draw;
	Reveal;
	ReadLn(a, b);
	SweepAt(a, b);
	ReadLn;
end.