{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator....  Joakim Åling, Sweden                                                                }
{ Date.......  May 18, 2011                                                                        }
{ Website....                                                                                      }
{                                                                                                  }
{ Description  A "clone" of the MS Mine Sweeper game for the console.                              }
{                                                                                                  }
{                                                                                                  }
{ Usage......  Sweep at a location by entering its coordinates. Sweep until only the mines are     }
{              left hidden.                                                                        }
{                                                                                                  }
{ History....  2012-04-22 Rewrote source code.                                                     }
{                                                                                                  }
{ Information  Lots left to be done. Sweeping should only reveal numbers closest to the sweep      }
{              point. Scoring, colouration, board design.                                          }
{--------------------------------------------------------------------------------------------------}
program MineSweeperGame;

uses
	Crt;
{--------------------------------------------------------------------------------------------------}
const
	MaxMines = 15;
	Mine = 'M';
	MinX = 0;
	MinY = 0;
	MaxX = 9;
	MaxY = 9;
{--------------------------------------------------------------------------------------------------}
type
	TBoard = array[MinX..MaxX, MinY..MaxY] of record
		Hidden: Boolean;
		Number: Char;		
	end;
{--------------------------------------------------------------------------------------------------}
var
	Board: TBoard;
{--------------------------------------------------------------------------------------------------}
function CountMines(X, Y: Byte): Byte;
begin
	CountMines := 0;
	Inc(CountMines, Byte((Y <> MinY) and (Board[X, Y - 1].Number = Mine)));
	Inc(CountMines, Byte((X <> MaxX) and (Y <> MinY) and (Board[X + 1, Y - 1].Number = Mine)));
	Inc(CountMines, Byte((X <> MaxX) and (Board[X + 1, Y].Number = Mine)));
	Inc(CountMines, Byte((X <> MaxX) and (Y <> MaxY) and (Board[X + 1, Y + 1].Number = Mine)));
	Inc(CountMines, Byte((Y <> 9) and (Board[X, Y + 1].Number = Mine)));
	Inc(CountMines, Byte((X <> MinX) and (Y <> MaxY) and (Board[X - 1, Y + 1].Number = Mine)));
	Inc(CountMines, Byte((X <> MinX) and (Board[X - 1, Y].Number = Mine)));
	Inc(CountMines, Byte((X <> MinX) and (Y <> MinY) and (Board[X - 1, Y - 1].Number = Mine)));
end;
{--------------------------------------------------------------------------------------------------}
procedure InitGame;
var
	Index, X, Y: Byte;
begin
	for Index := 1 to MaxMines do begin
		repeat
			X := Random(10);
			Y := Random(10);
		until Board[X, Y].Number <> Mine;
		Board[X, Y].Number := Mine;
	end;
	for X := MinX to MaxX do begin
		for Y := MinY to MaxY do begin
			if Board[X, Y].Number <> Mine then Board[X, Y].Number := Chr(48 + CountMines(X, Y));
			if Board[X, Y].Number = '0' then Board[X, Y].Number := #32;
			Board[X, Y].Hidden := true;
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure DrawBoard;
var
	X, Y: Byte;
begin
	ClrScr;
	Write('  ');
	TextAttr := $08;
	for X := MinX to MaxX do Write(' ', X);
	WriteLn;
	for X := MinX to MaxX do begin
		TextAttr := $08;
		Write(' ', X);
		for Y := MinY to MaxY do begin
			if Board[X, Y].Hidden then begin
				TextAttr := $03;
				Write(' #');
			end else begin
				case Board[X, Y].Number of
					'1'..'8':
						TextAttr := $02;
					Mine:
						TextAttr := $04;
				end;
				Write(' ', Board[X, Y].Number);
			end;
		end;
		WriteLn;
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure SweepAt(X, Y: Integer);
begin
	if (Board[X, Y].Number <> Mine) and (Board[X, Y].Hidden) and (X in [MinX..MaxX]) and (Y in [MinY..MaxY]) then begin
		Board[X, Y].Hidden := false;
		SweepAt(X + 1, Y - 1);
		SweepAt(X + 1, Y);
		SweepAt(X + 1, Y + 1);
		SweepAt(X, Y + 1);
		SweepAt(X - 1, Y + 1);
		SweepAt(X - 1, Y);
		SweepAt(X - 1, Y - 1);
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure RevealMines;
var
	X, Y: Integer;
begin
	for X := MinX to MaxX do begin
		for Y := MinY to MaxY do begin
			Board[X, Y].Hidden := not (Board[X, Y].Number = Mine);
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
begin
	Randomize;
	InitGame;
	DrawBoard;
	ReadLn;
	SweepAt(5, 4);
	DrawBoard;
	ReadLn;
end.